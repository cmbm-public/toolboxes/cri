# Collective Relational Inference (CRI)

Our proposed **Collective Relational Inference** model learns the heterogeneous pairwise interactions from the trajectories of interacting particles.  

This repository includes the implementation of the three proposed methods in our paper: CRI, Evolving-CRI and Var-CRI.

Authors: Zhichao Han, Olga Fink and David S. Kammer. 



### Requirements
Python dependencies:  
- Python>=3.9.0  
- Pytorch>=1.10.0  
- Numpy>=1.22.4   

We assume GPU is available. We conduct the experiments on a Linux server with RTX 3090, cudatoolkit=11.3.1.  

We thank the authors of [NRI](https://arxiv.org/abs/1802.04687), which we consider as one baseline in our work, for releasing their code. Our implementation is developed based on their [repository](https://github.com/ethanfetaya/NRI).  


### Dataset
The data used in the publication can be downloaded from the [ETH Research Collection](https://doi.org/10.3929/ethz-b-000610139).  

The default path of input data assumes the following directory structure:

```
dataset/
└───sim_springs/
│   └─── K_2/
│   |    └─── particle_5/
│   |    |    └─── 30000_train_5000_valid_5000_test_BalancedTypes_2/
|   |    |         |  loc_train_springs5_30000.npy
|   |    |         |  ...
|   |    |         
│   |    └─── particle_10/
│   |    |    └─── 50000_train_10000_valid_10000_test_BalancedTypes/
|   |    |         |  loc_train_springs10_50000.npy
|   |    |         |  ...
|   |              
│   └─── K_4/
│   |    └─── particle_5/
│   |    |    └─── 30000_train_5000_valid_5000_test_BalancedTypes/
|   |    |         |  loc_train_springs5_30000.npy
|   |    |         |  ...
│   
└───sim_charge/
│   └─── K_2/
│   |    └─── particle_5/
│   |    |    └─── 10000_train_2000_valid_2000_test_diffMass
|   |    |         |  loc_train_charged5_10000.npy
|   |    |         |  ...
|
└───crystallization/
│   |   LJ_GNN_input_N100_500000_freq50_train7000_val1500_test1500.npz
│   |   ...
```

### (Demo) Example: train CRI on Charge N5K2 using 500 simulations as training data, and test the trained model

```bash
cd codes_for_fixed_topology/
# Using 500 simulations as training data
# The best trained model will be saved in trained_CRI_chargeDiffMass_N5/ folder
# The input data are saved in ../dataset/sim_charge/K_2/particle_5/10000_train_2000_valid_2000_test_diffMass/ by default
CUDA_VISIBLE_DEVICES=0 python train_CRI.py --input-dir ../dataset/sim_charge/K_2/particle_5/10000_train_2000_valid_2000_test_diffMass/ --save-folder trained_CRI_chargeDiffMass_N5 --num-atoms 5 --suffix "_charged5" --edge_types 2 --cmpt-posterior exact --used-time-steps 100 --train-examples 500 --valid-examples 1000 --test-examples 1000 --batch-size 8 --var 0.05 --all_train_exp 10000 --all_valid_exp 2000 --all_test_exp 2000 --seed 123

# Test the best trained model. Results will be saved to trained_CRI_chargeDiffMass_N5/evaluation.txt
CUDA_VISIBLE_DEVICES=0 python test_CRI.py --trained_model_path trained_CRI_chargeDiffMass_N5/ --input-dir ../dataset/sim_charge/K_2/particle_5/10000_train_2000_valid_2000_test_diffMass/ --num-atoms 5 --suffix "_charged5" --edge_types 2 --cmpt-posterior exact --used-time-steps 100 --train-examples 500 --valid-examples 1000 --test-examples 1000 --batch-size 8 --var 0.05 --all_train_exp 10000 --all_valid_exp 2000 --all_test_exp 2000 --test_generalization FALSE > trained_CRI_chargeDiffMass_N5/evaluation.txt
```


### To reproduce the experiments in the paper
Check the bash files in `codes_for_fixed_topology/` and `codes_for_evolving_topology/`    


### License
MIT License
