from __future__ import division
from __future__ import print_function

import time
import argparse
import pickle
import os
import datetime
from tqdm import tqdm

from pathlib import Path
from sklearn import metrics

import warnings
warnings.filterwarnings("error")


import torch 
torch.set_num_threads(4)
torch.set_default_dtype(torch.float64)


import torch.optim as optim
from torch.optim import lr_scheduler

from utils import *
from modules import MLP_PIGNPI_Decoder

parser = argparse.ArgumentParser()
parser.add_argument('--no-cuda', action='store_true', default=False,
                    help='Disables CUDA training.')
parser.add_argument('--seed', type=int, default=-1, help='Random seed.') # 42
parser.add_argument('--epochs', type=int, default=500,
                    help='Number of epochs to train.')
parser.add_argument('--batch-size', type=int, default=8,
                    help='Number of samples per batch.')
parser.add_argument('--lr', type=float, default=1e-3,
                    help='Initial learning rate.')
# parser.add_argument('--encoder-hidden', type=int, default=256, help='Number of hidden units.')
parser.add_argument('--decoder-hidden', type=int, default=256,
                    help='Number of hidden units.')
parser.add_argument('--temp', type=float, default=0.5,
                    help='Temperature for Gumbel softmax.')
parser.add_argument('--num-atoms', type=int, default=5,
                    help='Number of atoms in simulation.')
# parser.add_argument('--encoder', type=str, default='mlp', help='Type of path encoder model (mlp or cnn).')
parser.add_argument('--decoder', type=str, default='pignpi',
                    help='Type of decoder model (pignpi, rnn, or sim).')
parser.add_argument('--no-factor', action='store_true', default=False,
                    help='Disables factor graph model.')
parser.add_argument('--suffix', type=str, default='_springs5',
                    help='Suffix for training data (e.g. "_charged".')
# parser.add_argument('--encoder-dropout', type=float, default=0.0, help='Dropout rate (1 - keep probability).')
parser.add_argument('--decoder-dropout', type=float, default=0.0,
                    help='Dropout rate (1 - keep probability).')
parser.add_argument('--save-folder', type=str, default='trained_EM_5particles_K2_ratio55',
                    help='Where to save the trained model [trained_VarEM_5particles_K2_ratio55]')
parser.add_argument('--load-folder', type=str, default='',
                    help='Where to load the trained model if finetunning. ' +
                         'Leave empty to train from scratch')
parser.add_argument('--edge_types', type=int, default=2,
                    help='The number of edge types to infer.')
parser.add_argument('--space-dim', type=int, default=2, help='The space dimension.')
parser.add_argument('--dims', type=int, default=5, # add mass
                    help='The number of input dimensions (position + velocity + mass).')
# parser.add_argument('--timesteps', type=int, default=500, # 49
#                     help='The number of time steps per sample.')
# parser.add_argument('--prediction_steps', type=int, default=10, metavar='N',
#                     help='Num steps to predict before re-using teacher forcing.')
parser.add_argument('--lr-decay', type=int, default=200,
                    help='After how epochs to decay LR by a factor of gamma.')
parser.add_argument('--gamma', type=float, default=0.5,
                    help='LR decay factor.')
parser.add_argument('--skip-first', action='store_true', default=False,
                    help='Skip first edge type in decoder, i.e. it represents no-edge.')
parser.add_argument('--var', type=float, default=0.05, # 0.5 for spring works well
                    help='Output variance.')
parser.add_argument('--hard', action='store_true', default=False,
                    help='Uses discrete samples in training forward pass.')
parser.add_argument('--prior', action='store_true', default=False,
                    help='Whether to use sparsity prior.')
parser.add_argument('--input-dir', type=str, default='./data/',
                    help='The directory containing input data.')
parser.add_argument('--train-examples', type=int, default=500, # 600, 3000
                    help='Number of train examples (number of simulations).')
parser.add_argument('--valid-examples', type=int, default=150, # 200, 500
                    help='Number of valid examples (number of simulations).')
parser.add_argument('--test-examples', type=int, default=150,  # 200, 500
                    help='Number of test examples (number of simulations).')

parser.add_argument('--delta-T', type=float, default=0.01, help='simulation time step size.')

# number of parallel processes
parser.add_argument('--noisy-input', action='store_true', default=False,
                    help='train the model with noisy input.')
parser.add_argument('--noisy-level', type=float, default=-1,
                    help='noisy level.')

# number of parallel processes
# parser.add_argument('--num-processes', type=int, default=10, help='Number of atoms in simulation.')

# how many time steps to use
parser.add_argument('--used-time-steps', type=int, default=100, help='Number of time steps in each simulation to use.')
# which method to use for computing the posterior
parser.add_argument('--cmpt-posterior', type=str, default='exact', help='Which method to use for computing the posterior ("exact", "mean-field", or "mean-field-block").')
# With mean-field approximation:
parser.add_argument('--MF-blocks', type=int, default=2, help='Number of blocks for the mean-field')
parser.add_argument('--MF-iterations', type=int, default=10, help='Number of iterations for computing mean-field approximation')


## input
parser.add_argument('--all_train_exp', type=int, default=-1, help='Number of ALL training time steps in each simulation')
parser.add_argument('--all_valid_exp', type=int, default=-1, help='Number of ALL validation time steps in each simulation')
parser.add_argument('--all_test_exp', type=int, default=-1, help='Number of ALL testing time steps in each simulation')





args = parser.parse_args()
args.cuda = not args.no_cuda and torch.cuda.is_available()
args.factor = not args.no_factor

if args.noisy_level != -1:
    assert args.noisy_input is True

assert args.load_folder == ''
assert args.cuda == True
print(args)

if args.seed != -1:
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    if args.cuda:
        torch.cuda.manual_seed(args.seed)
print("args.seed: ", args.seed)


data_type = args.suffix[1:7]
assert data_type in ["spring", "charge"]

### Global variables
## rel_rec: the edge-receiver matrix. Shape [num_edges, num_atoms], num_edges = num_atoms * (num_atoms - 1)
## rel_send: the edge-sender matrix. Shape [num_edges, num_atoms]
off_diag = np.ones([args.num_atoms, args.num_atoms]) - np.eye(args.num_atoms)
rel_rec = np.array(encode_onehot(np.where(off_diag)[0]), dtype=np.float64)# ; print(rel_rec.shape) ; exit() # (num_edges, num_particles)
rel_send = np.array(encode_onehot(np.where(off_diag)[1]), dtype=np.float64)
rel_rec = torch.from_numpy(rel_rec).cuda()
rel_send = torch.from_numpy(rel_send).cuda()

N_incoming_edges = args.num_atoms - 1




def return_incoming_edges():
    # return the ids of incoming edges (num_node-1) for every node
    # incoming_edges, type: np.array, shape: [num_node, num_node-1]
    incoming_edges = np.zeros(shape=(args.num_atoms, args.num_atoms-1), dtype=np.int64)
    num_edges_in_graph = args.num_atoms * (args.num_atoms - 1)
    for i in range(args.num_atoms):
        # i is the center node to consider
        assert len(rel_rec[:, i]) == num_edges_in_graph # rel_rec shape: [num_edge, num_node]
        incoming_edge_ids = [j for j in range(num_edges_in_graph) if rel_rec[j, i] == 1]
        assert len(incoming_edge_ids) == args.num_atoms - 1
        incoming_edges[i] = np.array(incoming_edge_ids)
    return incoming_edges


def return_latent_space():
    # The space for all possible realizations for every subgraph
    # for every n-1 edges (incoming edges for a node), return the all possible combinations of interaction type on these n-1 edges
    # return shape: [args.edge_types**(num_node-1), num_node-1, edge_types]
    # another implementation way: https://stackoverflow.com/questions/63839024/one-hot-encode-numpy-array-with-2-dims
    N_all_combinations = args.edge_types**N_incoming_edges
    edges_states = np.zeros(shape=(N_all_combinations, N_incoming_edges, args.edge_types), dtype=np.int64) # todo: np.int64 can be reduced
    for i in range(N_all_combinations):
        # this_binary_str = f"{i:0{N_incoming_edges}b}" # "0001"
        this_str = np.base_repr(i, base=args.edge_types)
        assert len(this_str) <= N_incoming_edges
        this_str = "0" * (N_incoming_edges - len(this_str) ) + this_str
        assert len(this_str) == N_incoming_edges
        for e_id in range(N_incoming_edges):
            mask = this_str[e_id]
            mask = int(mask)
            edges_states[i, e_id, mask] = 1
    return edges_states


### Global variables
## node_incoming_edges: the ids of incoming edges for every node. Shape: [num_node, num_node-1].
node_incoming_edges = return_incoming_edges()

## realization_edge_type: (one-hot encoded) the edge types corresponding to every realization of the subgraph (the mapping from subgraph class to its edge types). 
## realization_edge_type Shape: [#realizations (edge_types**(num_node-1)), #incoming_edges, edge_types (K)]
realization_edge_type = return_latent_space() 




## realization_allEdge_types is the types of all edges in the graph (expanding realization_edge_type)
## realization_allEdge_types shape: [#realizations, #all_edges_in_one_graph, K]
realization_allEdge_types = np.zeros(shape=(args.edge_types**N_incoming_edges, args.num_atoms * (args.num_atoms - 1),  args.edge_types), dtype=np.int64)
for i in range(args.num_atoms):
    incoming_edge_ids = node_incoming_edges[i]
    realization_allEdge_types[:, incoming_edge_ids, :] = realization_edge_type
realization_allEdge_types = torch.from_numpy(realization_allEdge_types)
if args.cuda:
    realization_allEdge_types = realization_allEdge_types.cuda()
print("### realization_allEdge_types shape: ", realization_allEdge_types.shape)


###
# S: subgraph
# IE: incoming-edges for one subgraph
# AE: all edges in one simualtion
# R: realizations

# nodeIncomingEdges_S_IE_AE: the one-hot encoding for the in-coming edges in every subgraph
nodeIncomingEdges_S_IE_AE = np.zeros(shape=(args.num_atoms, args.num_atoms-1, args.num_atoms*(args.num_atoms-1)), dtype=np.float64) 
for i in range(args.num_atoms): # iterate over subgraphs
    for j in range(args.num_atoms - 1): # iterate over the number of incoming edges
        edge_id = node_incoming_edges[i,j]
        nodeIncomingEdges_S_IE_AE[i, j, edge_id] = 1.0
nodeIncomingEdges_S_IE_AE = torch.from_numpy(nodeIncomingEdges_S_IE_AE).cuda()
#R_IE_K: the one-hot encoding from R to the incoming edges' types









## exact infer edge types
def cmpt_categorical_distribution(epoch, decoder, data_loader, mode):
    ## mode: ["training", "valid", "testing"]
    ## return edge_type_prediction (most likely interaction type for each edge?), shape: [num_graphs, args.num_atoms * (args.num_atoms-1), args.edge_types]
    ## return latent_structure_possibility, shape: [num_graphs, args.num_atoms, args.edge_types**N_incoming_edges]
    if mode == "training":
        num_graphs = args.train_examples
    elif mode == "valid":
        num_graphs = args.valid_examples
    elif mode == "testing":
        num_graphs = args.test_examples
    else:
        print("###In cmpt_categorical_distribution(), mode error")
        exit()


    ## mixing coefficients
    logTau_K = torch.log(tau.squeeze(-1) )
    realization_edgeType_R_IE_K = torch.from_numpy(realization_edge_type).cuda()
    logTau_R_IE_K = realization_edgeType_R_IE_K * logTau_K[None, None, :] # select tau on every edge
    logTau_R = torch.sum(logTau_R_IE_K, dim=(1, 2), keepdim=False)

    # print(logTau_R)
    # exit()


    ## "edge_type_prediction" is the most likely interaction type for every edge, based on current estimation of parameters.
    # edge_type_prediction shape: [num_graphs, #all_edges_in_one_graph, K]
    edge_type_prediction = np.random.randint(low=0, high=2, size=(num_graphs, args.num_atoms * (args.num_atoms-1), args.edge_types) )
    

    ## "latent_structure_possibility" is the categorical probability of every subgraph
    # latent_structure_possibility shape: [num_graphs, num_structures (num_nodes), num_realizations]
    latent_structure_possibility = torch.zeros((num_graphs, args.num_atoms, args.edge_types**N_incoming_edges ) )
    latent_structure_possibility = latent_structure_possibility.cuda()
    

    # for batch_idx, (data, acc, relations, pairwise_force, exp_id) in enumerate(data_loader):
    for batch_idx, (data, acc, relations, exp_id) in enumerate(data_loader):
        # batch_size equals to the number of simulations (the number of graphs)
        # data shape: [batch_size, num_atoms, num_timesteps, 2 * num_dims + 1]
        if args.cuda:
            # data, acc, relations, pairwise_force, exp_id = data.cuda(), acc.cuda(), relations.cuda(), pairwise_force.cuda(), exp_id.cuda()
            data, acc, relations, exp_id = data.cuda(), acc.cuda(), relations.cuda(), exp_id.cuda()

        with torch.no_grad():
            prediction_of_different_realizations = decoder.cmpt_net_force(data, realization_allEdge_types, rel_rec, rel_send)
        prediction_of_different_realizations = prediction_of_different_realizations.transpose(2, 3) #[#realizations, batch_size, time_steps, num_atoms, space_dim] -> [#realizations, batch_size, num_atoms, time_steps, space_dim]
        prediction_of_different_realizations = prediction_of_different_realizations.transpose(0, 1) # [#realizations, batch_size, num_atoms, time_steps, space_dim] -> [batch_size, #realizations, num_atoms, time_steps, space_dim]
        prediction_of_different_realizations = prediction_of_different_realizations.transpose(1, 2) # [batch_size, #realizations, num_atoms, time_steps, space_dim] -> [batch_size, num_atoms, #realizations, time_steps, space_dim]
        prediction_of_different_realizations = prediction_of_different_realizations.contiguous()
        # prediction_of_different_realizations (net_force) shape: [batch_size, num_atoms, #realizations, time_steps, space_dim]
        predict_acc_B_S_AR_T_D = prediction_of_different_realizations / data[:, :, None, :, [-1]]

        # pairwise_force = pairwise_force.transpose(1, 2).contiguous()
        # ground_truth_net_force = pairwise_force.transpose(-2, -1).matmul(rel_rec).transpose(-2, -1)
        # ground_truth_net_force = ground_truth_net_force.transpose(1, 2).contiguous()
        # # print(ground_truth_net_force.shape) # [batch_size, num_atoms, time_steps, space_dim]
        # ground_truth_net_force = ground_truth_net_force[:, :, None, :, :]
        ground_truth_acc = acc[:, :, None, :, :]


        logits_B_S_AR_T_D = - (predict_acc_B_S_AR_T_D - ground_truth_acc) ** 2 / (2 * args.var) 
        # print(exponents.shape) # shape: [batch_size, num_atoms, #realizations, time_steps, space_dim]
        logits_B_S_AR = torch.sum(logits_B_S_AR_T_D, dim=(3, 4), keepdims=False)
        # print(exponents.shape) # shape: [batch_size, num_atoms, #realizations]

        logits_B_S_AR = logits_B_S_AR + logTau_R[None, None, :]

        realization_possibilities_B_S_AR = torch.softmax(logits_B_S_AR, dim=-1)

        latent_structure_possibility[exp_id, :, :] = realization_possibilities_B_S_AR


    ## Infer the most likely realization for every subgraph currently.
    most_likely_realization_id = torch.argmax(latent_structure_possibility, dim=-1, keepdim=False)
    for i in range(len(latent_structure_possibility)):
        graph_id = i
        for v in range(args.num_atoms):
            ml_realization_id = most_likely_realization_id[i, v]

            ML_realization = realization_edge_type[ml_realization_id]
            incoming_edge_ids = node_incoming_edges[v]
            edge_type_prediction[graph_id, incoming_edge_ids, :] = ML_realization

    return latent_structure_possibility, edge_type_prediction





## mean-field approximation, in groups
def cmpt_categorical_distribution_variational_block(epoch, decoder, data_loader, itr_rounds, num_groups, mode):
    ## mode: ["training", "valid", "testing"]
    ## return mlEdgeType_AB_AE_K (most likely interaction type for each edge?), shape: [num_graphs, args.num_atoms * (args.num_atoms-1), args.edge_types]
    ## return latent_structure_possibility, shape: [num_graphs, args.num_atoms, args.edge_types**N_incoming_edges]

   
    realizationEdgeID_AR_IE = np.argmax(realization_edge_type, axis = -1)
    # print("realizationEdgeID_AR_IE shape: ", realizationEdgeID_AR_IE.shape); exit() # [16, 4]

    tau_K_1 = tau#.cpu().detach().numpy()
    logTau_K = torch.log(tau_K_1.squeeze(-1) )


    ### divide the edges to groups
    # num_groups = 2 # G
    # print("num_groups: ", num_groups)
    edges_in_groups = np.array_split([i for i in range(N_incoming_edges)], num_groups)
    # print("edges_in_groups: ", edges_in_groups) #; exit()
    group_inEdge_mapping_G_IE = torch.zeros((num_groups, N_incoming_edges)) # group, and the in-coming edges in each group
    for i in range(num_groups):
        this_edges = edges_in_groups[i]
        for e in this_edges:
            group_inEdge_mapping_G_IE[i, e] = 1.0
    group_inEdge_mapping_G_IE = group_inEdge_mapping_G_IE.cuda()
    group_size_G = torch.zeros(num_groups, dtype=torch.int32) # the number of edges in each group
    for i in range(num_groups):
        group_size_G[i] = len(edges_in_groups[i])
    
    group_realization_edge_G_SR_IE_K = []
    for i in range(num_groups):
        num_real = args.edge_types ** group_size_G[i] # the number of realizations of this group
        this_realization_edge_SR_IE_K = torch.zeros((num_real, N_incoming_edges, args.edge_types)).cuda()
        for itr_real in range(num_real):
            # this_binary_str = f"{itr_real:0{group_size_G[i]}b}" # "01"
            this_str = np.base_repr(itr_real, base=args.edge_types)
            assert len(this_str) <= group_size_G[i]
            this_str = "0" * (group_size_G[i] - len(this_str) ) + this_str
            assert len(this_str) == group_size_G[i]
            for itr_edge in range(group_size_G[i]):
                e_id = edges_in_groups[i][itr_edge]
                mask = int(this_str[itr_edge])
                this_realization_edge_SR_IE_K[itr_real, e_id, mask] = 1.0
        group_realization_edge_G_SR_IE_K.append(this_realization_edge_SR_IE_K)
    # print("group_realization_edge_G_SR_IE_K")
    # print(group_realization_edge_G_SR_IE_K)
    # exit()

                

    if mode == "training":
        num_graphs = args.train_examples
    elif mode == "valid":
        num_graphs = args.valid_examples
    elif mode == "testing":
        num_graphs = args.test_examples
    else:
        print("###In cmpt_categorical_distribution_variational_block(), mode error")
        exit()


    ## the variance in Gaussian distribution
    sigma_sqr = 0.5 # todo: the variance in Gaussian distribution
    num_all_edges = args.num_atoms * (args.num_atoms-1)

    ## the probability, of all possible realizations for all subgraphs in different simulations 
    Prob_AB_S_AR = torch.zeros((num_graphs, args.num_atoms, args.edge_types**N_incoming_edges ) ).cuda() # todo: last dimension can be reduced

    ## "mlEdgeType_AB_AE_K" is the most likely interaction type for every edge, based on current estimation of parameters.
    mlEdgeType_AB_AE_K = np.random.randint(low=0, high=2, size=(num_graphs, num_all_edges, args.edge_types) )

    ## The probability for every group
    Prob_G_AB_S_SR = []
    for i in range(num_groups):
        Prob_G_AB_S_SR.append(torch.zeros(num_graphs, args.num_atoms, args.edge_types ** group_size_G[i]).cuda() )

    # for batch_idx, (data, acc, relations, pairwise_force, exp_id) in enumerate(data_loader):
    for batch_idx, (data, acc, relations, exp_id) in enumerate(data_loader):
        # batch_size equals to the number of simulations (the number of graphs)
        if args.cuda:
            data, acc, relations, exp_id = data.cuda(), acc.cuda(), relations.cuda(), exp_id.cuda()
        this_batch_size = len(exp_id)
        with torch.no_grad():
            edgeForces_B_AE_K_T_D = decoder.cmpt_all_type_forces_all_edges(data, rel_rec, rel_send)
        # print("edgeForces_B_AE_K_T_D shape: ", edgeForces_B_AE_K_T_D.shape) # shape: [batch_size, #all_edges, K, #time_steps, dimension]

        # # ground-truth net force for every center node
        # pairwise_force = pairwise_force.transpose(1, 2).contiguous()
        # trueNetForce_B_T_S_D = pairwise_force.transpose(-2, -1).matmul(rel_rec).transpose(-2, -1)
        # trueNetForce_B_S_T_D = trueNetForce_B_T_S_D.transpose(1, 2)
        # trueNetForce_B_S_T_D = trueNetForce_B_S_T_D.contiguous()
        # # trueNetForce_B_S_T_D = trueNetForce_B_S_T_D.cpu().detach().numpy()
        # # print("trueNetForce_B_S_T_D shape: ", trueNetForce_B_S_T_D.shape); exit()
        ground_truth_acc_B_S_T_D = acc


        # pairwise force with current edge NN
        edgeForces_B_S_IE_K_T_D = torch.matmul(nodeIncomingEdges_S_IE_AE, edgeForces_B_AE_K_T_D.transpose(1, 3).unsqueeze(3) ) # shape: [batch_size, #time_steps, K, #subgraphs, #incoming_edges, dimension]
        edgeForces_B_S_IE_K_T_D = edgeForces_B_S_IE_K_T_D.permute(0, 3, 4, 2, 1, 5)
        edgeForces_B_S_IE_K_T_D = edgeForces_B_S_IE_K_T_D.contiguous()
        # edgeForces_B_S_IE_K_T_D = edgeForces_B_S_IE_K_T_D.cpu().detach().numpy()
        # print("edgeForces_B_S_IE_K_T_D shape: ", edgeForces_B_S_IE_K_T_D.shape) # shape:  [batch_size, #subgraphs, #incoming_edges, K, #time_steps, dimension]
        edgeForces_B_S_IE_K_T_D = edgeForces_B_S_IE_K_T_D / data[:, :, None, None, :, [-1]] # divide the mass !!!



        # mean-field factors
        # Prob_B_S_IE_K = (1.0 / args.edge_types) * torch.ones((this_batch_size, args.num_atoms, N_incoming_edges, args.edge_types ) ).cuda() # Initialize the probability of every factor
        Prob_G_B_S_SR = []
        for i in range(num_groups):
            # Prob_G_B_S_SR.append((1.0 / args.edge_types ** group_size_G[i]) * torch.ones((this_batch_size, args.num_atoms, args.edge_types ** group_size_G[i]) ).cuda() )
            Prob_G_B_S_SR.append((1.0 / args.edge_types ** group_size_G[i]) * torch.ones((this_batch_size, args.num_atoms, args.edge_types ** group_size_G[i]) ).cuda() )
        
        ## Update Prob_G_B_S_SR
        for _ in range(itr_rounds):
            # update very group
            for group_to_update in range(num_groups): 
                # logits_B_S_SR is the updated categorical probability of this group
                logits_B_S_SR = torch.zeros((this_batch_size, args.num_atoms, args.edge_types ** group_size_G[group_to_update])).cuda()
                
                for itr_1 in range(num_groups):
                    if itr_1 == group_to_update:
                        # the chosen dimension
                        f_itr1_B_S_SR_IE_K_T_D = edgeForces_B_S_IE_K_T_D[:, :, None, :, :, :, :] * group_realization_edge_G_SR_IE_K[itr_1][None, None, :, :, :, None, None]
                        f_itr1_B_S_SR_T_D = torch.sum(f_itr1_B_S_SR_IE_K_T_D, dim = (3, 4), keepdim = False)
                        f_itr1_B_S_SR_T_D = f_itr1_B_S_SR_T_D - ground_truth_acc_B_S_T_D[:, :, None, :, :] # absorb the net force
                        this_term_B_S_SR_T_D = f_itr1_B_S_SR_T_D * f_itr1_B_S_SR_T_D
                    else:
                        # dimension to compute expectation
                        f_itr1_B_S_SR_IE_K_T_D = edgeForces_B_S_IE_K_T_D[:, :, None, :, :, :, :] * group_realization_edge_G_SR_IE_K[itr_1][None, None, :, :, :, None, None]
                        f_itr1_B_S_SR_T_D = torch.sum(f_itr1_B_S_SR_IE_K_T_D, dim = (3, 4), keepdim = False)

                        this_term_B_S_T_D = torch.sum(f_itr1_B_S_SR_T_D * f_itr1_B_S_SR_T_D * Prob_G_B_S_SR[itr_1][:, :, :, None, None], axis = 2, keepdims=False)
                        this_term_B_S_SR_T_D = this_term_B_S_T_D[:, :, None, :, :]
                    this_term_B_S_SR = torch.sum(this_term_B_S_SR_T_D, axis=(-1, -2), keepdims = False)
                    logits_B_S_SR -= this_term_B_S_SR / (2 * args.var) # the negative in front of the logits

                # # The xy terms
                for itr_1 in range(num_groups): # absorb F_net into f_dimupdate
                    for itr_2 in range(itr_1 + 1, num_groups):

                        if itr_1 == group_to_update:
                            f_itr1_B_S_SR_IE_K_T_D = edgeForces_B_S_IE_K_T_D[:, :, None, :, :, :, :] * group_realization_edge_G_SR_IE_K[itr_1][None, None, :, :, :, None, None]
                            f_itr1_B_S_SR_T_D = torch.sum(f_itr1_B_S_SR_IE_K_T_D, dim = (3, 4), keepdim = False)
                            f_itr1_B_S_SR_T_D = f_itr1_B_S_SR_T_D - ground_truth_acc_B_S_T_D[:, :, None, :, :]# IMPORTANT: absorb F_net

                            f_itr2_B_S_SR_IE_K_T_D = edgeForces_B_S_IE_K_T_D[:, :, None, :, :, :, :] * group_realization_edge_G_SR_IE_K[itr_2][None, None, :, :, :, None, None]
                            f_itr2_B_S_SR_T_D = torch.sum(f_itr2_B_S_SR_IE_K_T_D, dim = (3, 4), keepdim = False)
                            mean_f_itr2_B_S_T_D = torch.sum(f_itr2_B_S_SR_T_D * Prob_G_B_S_SR[itr_2][:, :, :, None, None], axis = 2, keepdims=False)
                            # print("mean_f_itr2_B_S_T_D shape: ", mean_f_itr2_B_S_T_D.shape); exit() # [8, 5, 150, 2]

                            this_term_B_S_SR_T_D = f_itr1_B_S_SR_T_D * mean_f_itr2_B_S_T_D[:, :, None, :, :]

                        elif itr_2 == group_to_update:
                            f_itr1_B_S_SR_IE_K_T_D = edgeForces_B_S_IE_K_T_D[:, :, None, :, :, :, :] * group_realization_edge_G_SR_IE_K[itr_1][None, None, :, :, :, None, None]
                            f_itr1_B_S_SR_T_D = torch.sum(f_itr1_B_S_SR_IE_K_T_D, dim = (3, 4), keepdim = False)
                            mean_f_itr1_B_S_T_D = torch.sum(f_itr1_B_S_SR_T_D * Prob_G_B_S_SR[itr_1][:, :, :, None, None], axis = 2, keepdims = False)

                            f_itr2_B_S_SR_IE_K_T_D = edgeForces_B_S_IE_K_T_D[:, :, None, :, :, :, :] * group_realization_edge_G_SR_IE_K[itr_2][None, None, :, :, :, None, None]
                            f_itr2_B_S_SR_T_D = torch.sum(f_itr2_B_S_SR_IE_K_T_D, dim = (3, 4), keepdim = False)
                            f_itr2_B_S_SR_T_D = f_itr2_B_S_SR_T_D - ground_truth_acc_B_S_T_D[:, :, None, :, :] # IMPORTANT: absorb F_net

                            this_term_B_S_SR_T_D = f_itr2_B_S_SR_T_D * mean_f_itr1_B_S_T_D[:, :, None, :, :]

                        else:
                            f_itr1_B_S_SR_IE_K_T_D = edgeForces_B_S_IE_K_T_D[:, :, None, :, :, :, :] * group_realization_edge_G_SR_IE_K[itr_1][None, None, :, :, :, None, None]
                            f_itr1_B_S_SR_T_D = torch.sum(f_itr1_B_S_SR_IE_K_T_D, dim = (3, 4), keepdim = False)
                            mean_f_itr1_B_S_T_D = torch.sum(f_itr1_B_S_SR_T_D * Prob_G_B_S_SR[itr_1][:, :, :, None, None], axis = 2, keepdims = False)

                            f_itr2_B_S_SR_IE_K_T_D = edgeForces_B_S_IE_K_T_D[:, :, None, :, :, :, :] * group_realization_edge_G_SR_IE_K[itr_2][None, None, :, :, :, None, None]
                            f_itr2_B_S_SR_T_D = torch.sum(f_itr2_B_S_SR_IE_K_T_D, dim = (3, 4), keepdim = False)
                            mean_f_itr2_B_S_T_D = torch.sum(f_itr2_B_S_SR_T_D * Prob_G_B_S_SR[itr_2][:, :, :, None, None], axis = 2, keepdims = False)

                            this_term_B_S_T_D = mean_f_itr1_B_S_T_D * mean_f_itr2_B_S_T_D
                            this_term_B_S_SR_T_D = this_term_B_S_T_D[:, :, None, :, :]

                        this_term_B_S_SR = torch.sum(this_term_B_S_SR_T_D, axis=(-1, -2), keepdims = False)
                        logits_B_S_SR -= 2 * this_term_B_S_SR / (2 * args.var) # the negative in front of the logits, double (xy, yx)!!

                # the mixing coefficients
                for itr_1 in range(num_groups):
                    if itr_1 == group_to_update:
                        logTau_SR_IE_K = group_realization_edge_G_SR_IE_K[itr_1] * logTau_K[None, None, :] # select tau on every edge
                        logPi_SR_IE = torch.sum(logTau_SR_IE_K, axis = -1, keepdims = False)
                        logPi_SR = torch.sum(logPi_SR_IE, axis  = -1, keepdims = False)
                        logPi_B_S_SR = logPi_SR[None, None, :]
                        this_prior_B_S_SR = logPi_B_S_SR
                    else:
                        # the mean
                        logTau_SR_IE_K = group_realization_edge_G_SR_IE_K[itr_1] * logTau_K[None, None, :]
                        logPi_SR_IE = torch.sum(logTau_SR_IE_K, axis = -1, keepdims = False)
                        logPi_SR = torch.sum(logPi_SR_IE, axis  = -1, keepdims = False)

                        logPi_B_S_SR = Prob_G_B_S_SR[itr_1] * logPi_SR[None, None, :]
                        mean_logPi_B_S_1 = torch.sum(logPi_B_S_SR, axis = -1, keepdims = True)
                        this_prior_B_S_SR = mean_logPi_B_S_1
                    logits_B_S_SR += this_prior_B_S_SR

                realizationProb_B_S_SR = torch.softmax(logits_B_S_SR, dim = -1)
                Prob_G_B_S_SR[group_to_update][:, :, :] = realizationProb_B_S_SR
        for i in range(num_groups):
            Prob_G_AB_S_SR[i][exp_id, :, :] = Prob_G_B_S_SR[i]
    
    
    
    ## compute Prob_AB_S_AR
    Prob_AB_S_AR = torch.zeros((num_graphs, args.num_atoms, args.edge_types ** N_incoming_edges ) ).cuda()

    for realization_id in range(args.edge_types ** N_incoming_edges):
        # over the groups
        # this_binary_str = f"{realization_id:0{N_incoming_edges}b}"
        this_str = np.base_repr(realization_id, base=args.edge_types)
        assert len(this_str) <= N_incoming_edges
        this_str = "0" * (N_incoming_edges - len(this_str) ) + this_str
        assert len(this_str) == N_incoming_edges

        this_realization_prob = torch.ones((num_graphs, args.num_atoms) ).cuda()
        
        for group_id in range(num_groups):
            start_pos = edges_in_groups[group_id].min()
            end_pos = edges_in_groups[group_id].max() + 1
            sub_str = this_str[start_pos: end_pos]
            subRealization_id = int(sub_str, args.edge_types)
            # print("this_str: ", this_str, ", group_id ", group_id, ", sub_str: ", sub_str, ", subRealization_id: ", subRealization_id)
            this_realization_prob = this_realization_prob * Prob_G_AB_S_SR[group_id][:, :, subRealization_id]
        Prob_AB_S_AR[:, :, realization_id] = this_realization_prob



    realizationProb_AB_S_AR = Prob_AB_S_AR
    # print("realizationProb_AB_S_AR shape: ", realizationProb_AB_S_AR.shape) # shape: [#all_graphs, #subgraphs, #all_possible_realizations]


    ## Infer the most likely realization for every subgraph currently.
    mlRealizationId_AB_S = torch.argmax(realizationProb_AB_S_AR, dim=-1, keepdim=False)
    for i in range(len(realizationProb_AB_S_AR)):
        graph_id = i
        for v in range(args.num_atoms):
            ml_realization_id = mlRealizationId_AB_S[i, v]

            ML_realization = realization_edge_type[ml_realization_id]
            incoming_edge_ids = node_incoming_edges[v]
            mlEdgeType_AB_AE_K[graph_id, incoming_edge_ids, :] = ML_realization

    return realizationProb_AB_S_AR, mlEdgeType_AB_AE_K



def cmpt_clustering_metrics(preds, true_labels):
    AMI = metrics.adjusted_mutual_info_score(preds, true_labels)
    ARI = metrics.adjusted_rand_score(preds, true_labels)
    NMI = metrics.normalized_mutual_info_score(preds, true_labels)
    return (AMI, ARI, NMI)

def cmpt_loss(epoch, data_loader, decoder, mode):
    ## E-step
    assert args.cmpt_posterior == "exact" or args.cmpt_posterior == "mean-field-block"
    if args.cmpt_posterior == "exact":
        latent_structure_possibility, edge_type_prediction = cmpt_categorical_distribution(epoch, decoder, data_loader, mode)
        if epoch == 0:
            print("### In cmpt_loss, exactly compute posterior")
    elif args.cmpt_posterior == "mean-field":
        latent_structure_possibility, edge_type_prediction = cmpt_categorical_distribution_variational(epoch, decoder, data_loader, args.MF_iterations, mode)
        if epoch == 0:
            print("### In cmpt_loss, approximate posterior")
    elif args.cmpt_posterior == "mean-field-block":
        latent_structure_possibility, edge_type_prediction = cmpt_categorical_distribution_variational_block(epoch, decoder, data_loader, args.MF_iterations, args.MF_blocks, mode)
        if epoch == 0:
            print("### In cmpt_loss, approximate posterior (mean-field-block)")
    else:
        print("### In cmpt_loss, invalid argument for computing the posterior")

    edge_type_prediction = torch.from_numpy(edge_type_prediction)
    if args.cuda:
        edge_type_prediction = edge_type_prediction.cuda()
    _, ML_relation_preds = edge_type_prediction.max(-1) # shape: [num_simulation, num_node*(num_node-1)]

    ground_truth_relation = []
    nll_test = 0.0 # negative log likelihood loss
    MAE_acc_test = 0.0
    count_node_time = 0.0
    count_exp = 0

    decoder.eval()
    # for batch_idx, (data, acc, relations, pairwise_force, exp_id) in enumerate(data_loader):
    for batch_idx, (data, acc, relations, exp_id) in enumerate(data_loader):
        # pairwise_force shape: [batch_size, num_edges, time_steps, space_dim]
        this_batch_size = len(exp_id)
        if args.cuda:
            data, acc, relations, exp_id = data.cuda(), acc.cuda(), relations.cuda(), exp_id.cuda()                
        exp_id = exp_id.cpu().detach().numpy(); count_exp += len(exp_id)
        
        
        prediction_of_different_realizations = decoder.cmpt_net_force(data, realization_allEdge_types, rel_rec, rel_send)
        prediction_of_different_realizations = prediction_of_different_realizations.transpose(2, 3) #[#realizations, batch_size, time_steps, num_atoms, space_dim] -> [#realizations, batch_size, num_atoms, time_steps, space_dim]
        prediction_of_different_realizations = prediction_of_different_realizations.transpose(0, 1) # [#realizations, batch_size, num_atoms, time_steps, space_dim] -> [batch_size, #realizations, num_atoms, time_steps, space_dim]
        prediction_of_different_realizations = prediction_of_different_realizations.transpose(1, 2) # [batch_size, #realizations, num_atoms, time_steps, space_dim] -> [batch_size, num_atoms, #realizations, time_steps, space_dim]
        prediction_of_different_realizations = prediction_of_different_realizations.contiguous()
        predict_acc_B_S_AR_T_D = prediction_of_different_realizations / data[:, :, None, :, [-1]]

        
        ## latent_structure_possibility shape: [num_graphs, args.num_atoms, args.edge_types**N_incoming_edges]
        this_batch_realization_possibility = latent_structure_possibility[exp_id]
        # print("this_batch_realization_possibility shape: {}".format(this_batch_realization_possibility.shape)) # [batch_size, args.num_atoms, #realizations]    
        
        this_batch_realization_possibility = this_batch_realization_possibility.unsqueeze(3).unsqueeze(4)
        # this_batch_realization_possibility shape: [batch_size, args.num_atoms, #realizations, 1, 1]    
        
        target = acc[:, :, None, :, :]

        diff = (target - predict_acc_B_S_AR_T_D) ** 2 / (2 * args.var)
        # diff shape: [batch_size, args.num_atoms, #realizations, args.used_time_steps, args.space_dim]

        observation_likelihood = this_batch_realization_possibility * diff
        loss_nll = torch.sum(observation_likelihood)

        loss = loss_nll

        ground_truth_relation.append(relations)
        nll_test += loss_nll.item()
        MAE_acc_test += torch.sum(torch.abs(target - predict_acc_B_S_AR_T_D) * this_batch_realization_possibility).item()
        count_node_time += this_batch_size * args.num_atoms * args.used_time_steps


    ground_truth_relation = torch.cat(ground_truth_relation, dim=0) # shape: [num_simulations, num_node*(num_node-1)]
    assert ground_truth_relation.shape == ML_relation_preds.shape
    if args.edge_types == 2:
        equal_number = torch.eq(ML_relation_preds, ground_truth_relation).cpu().sum() # the predicted edge label corresponds to the ground-truth edge label
        opposite_equal_number = torch.eq(ML_relation_preds, 1-ground_truth_relation).cpu().sum() # the predicted edge label corresponds to the negative of the ground-truth edge label
        assert equal_number + opposite_equal_number == ML_relation_preds.shape[0] * ML_relation_preds.shape[1]
        counter = ML_relation_preds.shape[0] * ML_relation_preds.shape[1]

        assert count_exp == len(edge_type_prediction)
        acc_test = max([equal_number/float(counter), opposite_equal_number/float(counter)])
        acc_test = [acc_test]
    else:
        # compute the clustering metrics
        ML_relation_preds = ML_relation_preds.reshape(-1).cpu().detach().numpy()
        ground_truth_relation = ground_truth_relation.reshape(-1).cpu().detach().numpy()
        acc_test  = cmpt_clustering_metrics(ML_relation_preds, ground_truth_relation)
        



    mean_MAE_acc_test = MAE_acc_test / count_node_time
    mean_nll_test = nll_test / count_node_time

    return mean_MAE_acc_test, acc_test




def train(epoch, best_val_loss, verbose = True):
    t = time.time()

    ## Compute the categorical distribution of different realizations, and the current most likely realization
    ## E-step
    assert args.cmpt_posterior == "exact" or args.cmpt_posterior == "mean-field-block"
    if args.cmpt_posterior == "exact":
        latent_structure_possibility, edge_type_prediction = cmpt_categorical_distribution(epoch, decoder, train_loader, "training")
        if epoch == 0:
            print("### In train, exactly compute posterior")
    elif args.cmpt_posterior == "mean-field":
        latent_structure_possibility, edge_type_prediction = cmpt_categorical_distribution_variational(epoch, decoder, train_loader, args.MF_iterations, "training")
        if epoch == 0:
            print("### In train, approximate posterior")
    elif args.cmpt_posterior == "mean-field-block":
        latent_structure_possibility, edge_type_prediction = cmpt_categorical_distribution_variational_block(epoch, decoder, train_loader, args.MF_iterations, args.MF_blocks, "training")
        if epoch == 0:
            print("### In train, approximate posterior (mean-field-block)")
    else:
        print("### In train, invalid argument for computing the posterior")
    
    
    
    # latent_structure_possibility shape: [num_graphs, num_structures (num_nodes), num_realizations]
    # edge_type_prediction shape: [num_graphs, num_node*(num_node-1), interaction_types]
    edge_type_prediction = torch.from_numpy(edge_type_prediction)
    if args.cuda:
        edge_type_prediction = edge_type_prediction.cuda()

    _, ML_relation_preds = edge_type_prediction.max(-1) # shape: [num_training_examples, num_node*(num_node-1)]
    # print("ML_relation_preds shape: ", ML_relation_preds.shape)


    ground_truth_relation = []
    nll_train = 0.0 # negative log likelihood loss
    MAE_acc_train = 0.0
    count_node_time = 0.0
    count_exp = 0
    
    # M-step: maximizing the parameters in the neural network
    decoder.train()
    # for batch_idx, (data, acc, relations, pairwise_force, exp_id) in enumerate(train_loader):
    for batch_idx, (data, acc, relations, exp_id) in enumerate(train_loader):
        # pairwise_force shape: [batch_size, num_edges, time_steps, space_dim]
        # exp_id is a 1D tensor
        this_batch_size = len(exp_id)
        # print("In train, data shape: ", data.shape)# [8, 5, 150, 5]
        if args.cuda:
            data, acc, relations, exp_id = data.cuda(), acc.cuda(), relations.cuda(), exp_id.cuda()                

        exp_id = exp_id.cpu().detach().numpy(); count_exp += len(exp_id)
        optimizer.zero_grad()
        
        
        # partial likelihood
        # decoder.cmpt_all_type_forces_all_edges(data, rel_rec, rel_send); exit()
        prediction_of_different_realizations = decoder.cmpt_net_force(data, realization_allEdge_types, rel_rec, rel_send)
        prediction_of_different_realizations = prediction_of_different_realizations.transpose(2, 3) #[#realizations, batch_size, time_steps, num_atoms, space_dim] -> [#realizations, batch_size, num_atoms, time_steps, space_dim]
        prediction_of_different_realizations = prediction_of_different_realizations.transpose(0, 1) # [#realizations, batch_size, num_atoms, time_steps, space_dim] -> [batch_size, #realizations, num_atoms, time_steps, space_dim]
        prediction_of_different_realizations = prediction_of_different_realizations.transpose(1, 2) # [batch_size, #realizations, num_atoms, time_steps, space_dim] -> [batch_size, num_atoms, #realizations, time_steps, space_dim]
        prediction_of_different_realizations = prediction_of_different_realizations.contiguous()
        # prediction_of_different_realizations (net_force) shape: [batch_size, num_atoms, #realizations, time_steps, space_dim]
        # print("in train, data shape: ", data.shape)# [batch_size, num_atoms, time_steps, space_dim * 2 + 1]
        predict_acc_B_S_AR_T_D = prediction_of_different_realizations / data[:, :, None, :, [-1]]

        

        ############# EM algorithm
        ## latent_structure_possibility shape: [num_graphs, args.num_atoms, args.edge_types**N_incoming_edges]
        this_batch_realization_possibility = latent_structure_possibility[exp_id]
        # print("this_batch_realization_possibility shape: {}".format(this_batch_realization_possibility.shape)) # [batch_size, args.num_atoms, #realizations]    
        
        this_batch_realization_possibility = this_batch_realization_possibility.unsqueeze(3).unsqueeze(4)
        # this_batch_realization_possibility shape: [batch_size, args.num_atoms, #realizations, 1, 1]    

        
        # pairwise_force = pairwise_force.transpose(1, 2).contiguous()
        # target = pairwise_force.transpose(-2, -1).matmul(rel_rec).transpose(-2, -1)
        # target = target.transpose(1, 2) # [batch_size, time_steps, num_atoms, space_dim] -> [batch_size, num_atoms, time_steps, space_dim]
        # # print("target shape: {}".format(target.shape))#; exit()
        # # target shape: [batch_size, args.num_atoms, args.used_time_steps, args.space_dim]
        # target = target.unsqueeze(2)
        # target = target.contiguous()
        # # target shape: [batch_size, args.num_atoms, 1, args.used_time_steps, args.space_dim]
        target = acc[:, :, None, :, :]


        diff = (target - predict_acc_B_S_AR_T_D) ** 2 #/ (2 * args.var)
        # diff shape: [batch_size, args.num_atoms, #realizations, args.used_time_steps, args.space_dim]

        observation_likelihood = this_batch_realization_possibility * diff
        loss_nll = torch.sum(observation_likelihood)

        # loss_nll = nll_gaussian(output, target, args.var)        

        loss = loss_nll

        ground_truth_relation.append(relations)

        loss.backward()
        optimizer.step()

        nll_train += loss_nll.item()
        MAE_acc_train += torch.sum(torch.abs(target - predict_acc_B_S_AR_T_D) * this_batch_realization_possibility).item()
        count_node_time += this_batch_size * args.num_atoms * args.used_time_steps
    scheduler.step() # call `optimizer.step()` before `lr_scheduler.step()`. details at https://pytorch.org/docs/stable/optim.html#how-to-adjust-learning-rate


    ## M-step: maximizing the mixing coefficients
    count_K = torch.sum(torch.Tensor(realization_edge_type).cuda(), dim=1) #C_j(K) in the paper
    #count_K shape: [#realizations, K]
    gamma = torch.matmul(latent_structure_possibility, count_K)
    # gamma shape: [num_training_examples, num_node, K]
    mixing_coefficients_tau_new = torch.sum(gamma, dim=(0, 1), keepdim=False)
    mixing_coefficients_tau_new = mixing_coefficients_tau_new / torch.sum(mixing_coefficients_tau_new)
    tau = mixing_coefficients_tau_new[:, None]
    assert len(tau.shape) == 2




    ## relations prediction accuracy
    ## only suitable for K=2 case
    ground_truth_relation = torch.cat(ground_truth_relation, dim=0) # shape: [num_training_examples, num_node*(num_node-1)]
    assert ground_truth_relation.shape == ML_relation_preds.shape
    if args.edge_types == 2:
        equal_number = torch.eq(ML_relation_preds, ground_truth_relation).cpu().sum() # the predicted edge label corresponds to the ground-truth edge label
        opposite_equal_number = torch.eq(ML_relation_preds, 1-ground_truth_relation).cpu().sum() # the predicted edge label corresponds to the negative of the ground-truth edge label
        assert equal_number + opposite_equal_number == ML_relation_preds.shape[0] * ML_relation_preds.shape[1]
        counter = ML_relation_preds.shape[0] * ML_relation_preds.shape[1]

        assert count_exp == len(edge_type_prediction)
        acc_train = max([equal_number/float(counter), opposite_equal_number/float(counter)])
        acc_train = [acc_train]
    else:
        # compute the clustering metrics
        ML_relation_preds = ML_relation_preds.reshape(-1).cpu().detach().numpy()
        ground_truth_relation = ground_truth_relation.reshape(-1).cpu().detach().numpy()
        acc_train = cmpt_clustering_metrics(ML_relation_preds, ground_truth_relation)

    mean_MAE_acc_train = MAE_acc_train / count_node_time
    mean_nll_train = nll_train / count_node_time


    MAE_acc_valid, relation_acc_valid =cmpt_loss(epoch, valid_loader, decoder, "valid")

    if verbose:
        # ## save and print the training progress
        # if epoch % 1 == 0:
        #     print('Epoch: {:04d}'.format(epoch),
        #           'nll_train: {:.10f}'.format(nll_train),
        #           'mean_MAE_acc_train: {:.6E}'.format(mean_MAE_acc_train),
        #           'mean_nll_train: {:.6E}'.format(mean_nll_train),
        #           'acc_train: {:.10f}'.format(acc_train),
        #           'MAE_acc_valid: {:.6E}'.format(MAE_acc_valid),
        #           'relation_acc_valid: {:.10f}'.format(relation_acc_valid),
        #           'tau: {}'.format(tau.cpu().detach().numpy()[:, 0]), file=log)

        decoder_file_epoch = os.path.join(save_folder, 'saved_model/decoder_{}.pt'.format(epoch))
        torch.save(decoder.state_dict(), decoder_file_epoch)

        mixing_coefficients_tau_file = os.path.join(save_folder, "saved_model/tau_{}.pkl".format(epoch))
        with open(mixing_coefficients_tau_file, "wb") as f:
            pickle.dump(tau.cpu().detach().numpy(), f)


    if args.save_folder and MAE_acc_valid < best_val_loss:
    # if args.save_folder:
        print('Epoch: {:04d}'.format(epoch),
              'nll_train: {:.10f}'.format(nll_train),
              'mean_MAE_acc_train: {:.6E}'.format(mean_MAE_acc_train),
              'mean_nll_train: {:.6E}'.format(mean_nll_train),
              # 'acc_train: {:.10f}'.format(acc_train),
              'acc_train: (' + ', '.join(format(f, '.6f') for f in acc_train) + ')',
              'MAE_acc_valid: {:.6E}'.format(MAE_acc_valid),
              # 'relation_acc_valid: {:.10f}'.format(relation_acc_valid),
              'relation_acc_valid: (' + ', '.join(format(f, '.6f') for f in relation_acc_valid) + ')',
              'tau: {}'.format(tau.cpu().detach().numpy()[:, 0]))

        print('Epoch: {:04d}'.format(epoch),
              'nll_train: {:.10f}'.format(nll_train),
              'mean_MAE_acc_train: {:.6E}'.format(mean_MAE_acc_train),
              'mean_nll_train: {:.6E}'.format(mean_nll_train),
              # 'acc_train: {:.10f}'.format(acc_train),
              'acc_train: (' + ', '.join(format(f, '.6f') for f in acc_train) + ')',
              'MAE_acc_valid: {:.6E}'.format(MAE_acc_valid),
              # 'relation_acc_valid: {:.10f}'.format(relation_acc_valid),
              'relation_acc_valid: (' + ', '.join(format(f, '.6f') for f in relation_acc_valid) + ')',
              'tau: {}'.format(tau.cpu().detach().numpy()[:, 0]), file=log)
        log.flush()
        # save the best model
        torch.save(decoder.state_dict(), best_decoder_file)
        with open(best_mixing_coefficients_file, "wb") as f:
            pickle.dump(tau.cpu().detach().numpy(), f)
        
    
    edge_type_prediction = edge_type_prediction.cpu().detach().numpy()
    return MAE_acc_valid, np.argmax(edge_type_prediction, axis=-1)







if __name__ == "__main__":
    # Save model and meta-data. Always saves in a new sub-folder.
    if args.save_folder:
        from os import listdir
        
        if args.noisy_input:
            save_folder = '{}/{}simulations_{}timesteps/{}/'.format(args.save_folder, args.train_examples, args.used_time_steps, args.noisy_level)
            Path(save_folder).mkdir(parents=True, exist_ok=True)
            experiments_done = len(listdir(save_folder))
            save_folder = save_folder + 'exp{}/'.format(args.save_folder, args.train_examples, args.used_time_steps, args.noisy_level, experiments_done + 1)
        else:
            save_folder = '{}/{}simulations_{}timesteps/'.format(args.save_folder, args.train_examples, args.used_time_steps)
            Path(save_folder).mkdir(parents=True, exist_ok=True)
            experiments_done = len(listdir(save_folder))
            save_folder = save_folder + 'exp{}/'.format(experiments_done + 1)
        print("save_folder is: ", save_folder)
        
        Path(save_folder).mkdir(parents=True, exist_ok=True)    
        Path(save_folder+"saved_model/").mkdir(parents=True, exist_ok=True)
        meta_file = os.path.join(save_folder, 'metadata.pkl')
        best_decoder_file = os.path.join(save_folder, 'best_decoder.pt')
        best_mixing_coefficients_file = os.path.join(save_folder, 'best_decoder_tau.pkl')
        log_file = os.path.join(save_folder, 'log.txt')
        log = open(log_file, 'w')
        with open(meta_file, "wb") as f:
            pickle.dump({'args': args}, f)
    else:
        print("WARNING: No save_folder provided!" +
              "Testing (within this script) will throw an error.")


    ## load data
    if args.noisy_input:
        print("No nonisy input")
        exit()
        train_loader, valid_loader, test_loader, loc_max, loc_min, vel_max, vel_min = load_noisy_data(
        args.input_dir, args.batch_size, args.suffix, args.noisy_level, args.train_examples, args.valid_examples, args.test_examples)
    else:
        train_loader, valid_loader, test_loader, loc_max, loc_min, vel_max, vel_min = load_data(
        args.input_dir, args.batch_size, args.suffix, args.used_time_steps, args.train_examples, args.valid_examples, args.test_examples, save_min_max_dir=save_folder, all_train_exp = args.all_train_exp, all_valid_exp = args.all_valid_exp, all_test_exp = args.all_test_exp)


    ### The model and optimizer

    if args.decoder == 'pignpi':
        decoder = MLP_PIGNPI_Decoder(n_in_node=args.dims,
                             edge_types=args.edge_types,
                             msg_hid=args.decoder_hidden,
                             msg_out=args.decoder_hidden,
                             n_hid=args.decoder_hidden,
                             delta_t=args.delta_T, # added
                             loc_max=loc_max, # added
                             loc_min=loc_min, # added
                             vel_max=vel_max, # added
                             vel_min=vel_min, # added
                             do_prob=args.decoder_dropout,
                             skip_first=args.skip_first,
                             data_type = data_type)
    else:
        print("decoder init error")
        exit()
    decoder.cuda()

    ## the coefficients
    tau = np.array([1.0 / args.edge_types] * args.edge_types)
    tau = tau.reshape(args.edge_types, 1)
    tau = torch.from_numpy(tau).cuda()
    print("### Initialize tau as: ", tau)

    optimizer = optim.Adam(list(decoder.parameters()), lr=args.lr)
    scheduler = lr_scheduler.StepLR(optimizer, step_size=args.lr_decay, gamma=args.gamma)




    # Train model
    decoder_file_epoch = os.path.join(save_folder, 'saved_model/decoder_{}.pt'.format("before_train"))
    torch.save(decoder.state_dict(), decoder_file_epoch)
    mixing_coefficients_tau_file = os.path.join(save_folder, "saved_model/tau_{}.pt".format("before_train") )
    with open(mixing_coefficients_tau_file, "wb") as f:
        pickle.dump(tau.cpu().detach().numpy(), f)

    t_total = time.time()
    best_val_loss = np.inf
    best_epoch = 0
    for epoch in tqdm(range(args.epochs)):
        val_loss, edge_type_prediction = train(epoch, best_val_loss) #["exact", "mean-field", "mean-field-block"]
        if epoch == 0:
            edge_type_prediction_previous = edge_type_prediction
        else:
            equal_number = np.equal(edge_type_prediction_previous, edge_type_prediction).sum()
            unequal_number = (~np.equal(edge_type_prediction_previous, edge_type_prediction)).sum()
            if unequal_number > 0:
                print("Changed: {}, Unchanged: {}".format(unequal_number, equal_number))
            edge_type_prediction_previous = edge_type_prediction

        if val_loss < best_val_loss:
            best_val_loss = val_loss
            best_epoch = epoch
    print("Optimization Finished!")
    print("Best Epoch: {:04d}".format(best_epoch))
    if args.save_folder:
        print("Best Epoch: {:04d}".format(best_epoch), file=log)
        log.flush()
    log.close()



