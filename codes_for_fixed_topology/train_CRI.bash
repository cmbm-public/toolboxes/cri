## Train CRI, Spring N5K2, change the parameter --train-examples [100, 500, 1000, 5000, 10000] to use different number of simulations for training
CUDA_VISIBLE_DEVICES=0 python train_CRI.py --input-dir ../dataset/sim_springs/K_2/particle_5/30000_train_5000_valid_5000_test_BalancedTypes_2/ --save-folder trained_CRI_spring_N5_K2 --num-atoms 5 --suffix "_springs5" --edge_types 2 --cmpt-posterior exact --used-time-steps 100 --train-examples 100 --valid-examples 1000 --test-examples 1000 --batch-size 8 --var 0.1 --all_train_exp 30000 --all_valid_exp 5000 --all_test_exp 5000 --seed 123


## Train CRI, Spring N10K2, change the parameter --train-examples [100, 500, 1000, 5000, 10000] to use different number of simulations for training
CUDA_VISIBLE_DEVICES=0 python train_CRI.py --input-dir ../dataset/sim_springs/K_2/particle_10/50000_train_10000_valid_10000_test_BalancedTypes/ --save-folder trained_CRI_spring_N10_K2 --num-atoms 10 --suffix "_springs10" --edge_types 2 --cmpt-posterior exact --used-time-steps 100 --train-examples 10000 --valid-examples 1000 --test-examples 1000 --batch-size 8 --var 0.1 --all_train_exp 50000 --all_valid_exp 10000 --all_test_exp 10000 --seed 123


## Train CRI, Spring N5K4, change the parameter --train-examples [100, 500, 1000, 5000, 10000] to use different number of simulations for training
CUDA_VISIBLE_DEVICES=0 python train_CRI.py --input-dir ../dataset/sim_springs/K_4/particle_5/30000_train_5000_valid_5000_test_BalancedTypes/ --save-folder trained_CRI_spring_N5_K4 --num-atoms 5 --suffix "_springs5" --edge_types 4 --cmpt-posterior exact --used-time-steps 100 --train-examples 100 --valid-examples 1000 --test-examples 1000 --batch-size 8 --var 0.1 --all_train_exp 30000 --all_valid_exp 5000 --all_test_exp 5000 --seed 123


## Train CRI, Charge N5K2, change the parameter --train-examples [100, 500, 1000, 5000, 10000] to use different number of simulations for training
CUDA_VISIBLE_DEVICES=0 python train_CRI.py --input-dir ../dataset/sim_charge/K_2/particle_5/10000_train_2000_valid_2000_test_diffMass/ --save-folder trained_CRI_chargeDiffMass_N5 --num-atoms 5 --suffix "_charged5" --edge_types 2 --cmpt-posterior exact --used-time-steps 100 --train-examples 100 --valid-examples 1000 --test-examples 1000 --batch-size 8 --var 0.05 --all_train_exp 10000 --all_valid_exp 2000 --all_test_exp 2000 --seed 123
