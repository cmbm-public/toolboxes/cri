from __future__ import division
from __future__ import print_function

import time
import argparse
import pickle
import os
import datetime
from tqdm import tqdm
from pathlib import Path
from sklearn import metrics

import warnings
warnings.filterwarnings("error")


import torch 
torch.set_num_threads(4)
torch.set_default_dtype(torch.float64)


import torch.optim as optim
from torch.optim import lr_scheduler

from utils import *
from modules import MLP_PIGNPI_Decoder

parser = argparse.ArgumentParser()
parser.add_argument('--no-cuda', action='store_true', default=False,
                    help='Disables CUDA training.')
parser.add_argument('--seed', type=int, default=-1, help='Random seed.') # 42
parser.add_argument('--epochs', type=int, default=500,
                    help='Number of epochs to train.')
parser.add_argument('--batch-size', type=int, default=8,
                    help='Number of samples per batch.')
parser.add_argument('--lr', type=float, default=1e-3,
                    help='Initial learning rate.')
# parser.add_argument('--encoder-hidden', type=int, default=256, help='Number of hidden units.')
parser.add_argument('--decoder-hidden', type=int, default=256,
                    help='Number of hidden units.')
parser.add_argument('--temp', type=float, default=0.5,
                    help='Temperature for Gumbel softmax.')
parser.add_argument('--num-atoms', type=int, default=5,
                    help='Number of atoms in simulation.')
# parser.add_argument('--encoder', type=str, default='mlp', help='Type of path encoder model (mlp or cnn).')
parser.add_argument('--decoder', type=str, default='pignpi',
                    help='Type of decoder model (pignpi, rnn, or sim).')
parser.add_argument('--no-factor', action='store_true', default=False,
                    help='Disables factor graph model.')
parser.add_argument('--suffix', type=str, default='_springs5',
                    help='Suffix for training data (e.g. "_charged".')
# parser.add_argument('--encoder-dropout', type=float, default=0.0, help='Dropout rate (1 - keep probability).')
parser.add_argument('--decoder-dropout', type=float, default=0.0,
                    help='Dropout rate (1 - keep probability).')
parser.add_argument('--save-folder', type=str, default='trained_EM_5particles_K2_ratio55',
                    help='Where to save the trained model [trained_VarEM_5particles_K2_ratio55]')
parser.add_argument('--load-folder', type=str, default='',
                    help='Where to load the trained model if finetunning. ' +
                         'Leave empty to train from scratch')
parser.add_argument('--edge_types', type=int, default=2,
                    help='The number of edge types to infer.')
parser.add_argument('--space-dim', type=int, default=2, help='The space dimension.')
parser.add_argument('--dims', type=int, default=5, # add mass
                    help='The number of input dimensions (position + velocity + mass).')
# parser.add_argument('--timesteps', type=int, default=500, # 49
#                     help='The number of time steps per sample.')
# parser.add_argument('--prediction_steps', type=int, default=10, metavar='N',
#                     help='Num steps to predict before re-using teacher forcing.')
parser.add_argument('--lr-decay', type=int, default=200,
                    help='After how epochs to decay LR by a factor of gamma.')
parser.add_argument('--gamma', type=float, default=0.5,
                    help='LR decay factor.')
parser.add_argument('--skip-first', action='store_true', default=False,
                    help='Skip first edge type in decoder, i.e. it represents no-edge.')
parser.add_argument('--var', type=float, default=0.05, # 0.5 for spring works well
                    help='Output variance.')
parser.add_argument('--hard', action='store_true', default=False,
                    help='Uses discrete samples in training forward pass.')
parser.add_argument('--prior', action='store_true', default=False,
                    help='Whether to use sparsity prior.')
parser.add_argument('--input-dir', type=str, default='./data/',
                    help='The directory containing input data.')
parser.add_argument('--train-examples', type=int, default=500, # 600, 3000
                    help='Number of train examples (number of simulations).')
parser.add_argument('--valid-examples', type=int, default=150, # 200, 500
                    help='Number of valid examples (number of simulations).')
parser.add_argument('--test-examples', type=int, default=150,  # 200, 500
                    help='Number of test examples (number of simulations).')

parser.add_argument('--delta-T', type=float, default=0.01, help='simulation time step size.')

# number of parallel processes
parser.add_argument('--noisy-input', action='store_true', default=False,
                    help='train the model with noisy input.')
parser.add_argument('--noisy-level', type=float, default=-1,
                    help='noisy level.')

# number of parallel processes
# parser.add_argument('--num-processes', type=int, default=10, help='Number of atoms in simulation.')

# how many time steps to use
parser.add_argument('--used-time-steps', type=int, default=100, help='Number of time steps in each simulation to use.')
# which method to use for computing the posterior
parser.add_argument('--cmpt-posterior', type=str, default='exact', help='Which method to use for computing the posterior ("exact", "mean-field", or "mean-field-block").')
# With mean-field approximation:
parser.add_argument('--MF-blocks', type=int, default=2, help='Number of blocks for the mean-field')
parser.add_argument('--MF-iterations', type=int, default=10, help='Number of iterations for computing mean-field approximation')


## input
parser.add_argument('--all_train_exp', type=int, default=-1, help='Number of ALL training time steps in each simulation')
parser.add_argument('--all_valid_exp', type=int, default=-1, help='Number of ALL validation time steps in each simulation')
parser.add_argument('--all_test_exp', type=int, default=-1, help='Number of ALL testing time steps in each simulation')





args = parser.parse_args()
args.cuda = not args.no_cuda and torch.cuda.is_available()
args.factor = not args.no_factor

if args.noisy_level != -1:
    assert args.noisy_input is True

assert args.load_folder == ''
assert args.cuda == True
print(args)

if args.seed != -1:
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    if args.cuda:
        torch.cuda.manual_seed(args.seed)



data_type = args.suffix[1:7]
assert data_type in ["spring", "charge"]



### Global variables
## rel_rec: the edge-receiver matrix. Shape [num_edges, num_atoms], num_edges = num_atoms * (num_atoms - 1)
## rel_send: the edge-sender matrix. Shape [num_edges, num_atoms]
off_diag = np.ones([args.num_atoms, args.num_atoms]) - np.eye(args.num_atoms)
rel_rec = np.array(encode_onehot(np.where(off_diag)[0]), dtype=np.float64)# ; print(rel_rec.shape) ; exit() # (num_edges, num_particles)
rel_send = np.array(encode_onehot(np.where(off_diag)[1]), dtype=np.float64)
rel_rec = torch.from_numpy(rel_rec).cuda()
rel_send = torch.from_numpy(rel_send).cuda()

N_incoming_edges = args.num_atoms - 1




def return_incoming_edges():
    # return the ids of incoming edges (num_node-1) for every node
    # incoming_edges, type: np.array, shape: [num_node, num_node-1]
    incoming_edges = np.zeros(shape=(args.num_atoms, args.num_atoms-1), dtype=np.int64)
    num_edges_in_graph = args.num_atoms * (args.num_atoms - 1)
    for i in range(args.num_atoms):
        # i is the center node to consider
        assert len(rel_rec[:, i]) == num_edges_in_graph # rel_rec shape: [num_edge, num_node]
        incoming_edge_ids = [j for j in range(num_edges_in_graph) if rel_rec[j, i] == 1]
        assert len(incoming_edge_ids) == args.num_atoms - 1
        incoming_edges[i] = np.array(incoming_edge_ids)
    return incoming_edges


def return_latent_space():
    # The space for all possible realizations for every subgraph
    # for every n-1 edges (incoming edges for a node), return the all possible combinations of interaction type on these n-1 edges
    # return shape: [args.edge_types**(num_node-1), num_node-1, edge_types]
    # another implementation way: https://stackoverflow.com/questions/63839024/one-hot-encode-numpy-array-with-2-dims
    N_all_combinations = args.edge_types**N_incoming_edges
    edges_states = np.zeros(shape=(N_all_combinations, N_incoming_edges, args.edge_types), dtype=np.int64) # todo: np.int64 can be reduced
    for i in range(N_all_combinations):
        # this_binary_str = f"{i:0{N_incoming_edges}b}" # "0001"
        this_str = np.base_repr(i, base=args.edge_types)
        assert len(this_str) <= N_incoming_edges
        this_str = "0" * (N_incoming_edges - len(this_str) ) + this_str
        assert len(this_str) == N_incoming_edges
        for e_id in range(N_incoming_edges):
            mask = this_str[e_id]
            mask = int(mask)
            edges_states[i, e_id, mask] = 1
    return edges_states


### Global variables
## node_incoming_edges: the ids of incoming edges for every node. Shape: [num_node, num_node-1].
node_incoming_edges = return_incoming_edges()

## realization_edge_type: (one-hot encoded) the edge types corresponding to every realization of the subgraph (the mapping from subgraph class to its edge types). 
## realization_edge_type Shape: [#realizations (edge_types**(num_node-1)), #incoming_edges, edge_types (K)]
realization_edge_type = return_latent_space() 




## realization_allEdge_types is the types of all edges in the graph (expanding realization_edge_type)
## realization_allEdge_types shape: [#realizations, #all_edges_in_one_graph, K]
realization_allEdge_types = np.zeros(shape=(args.edge_types**N_incoming_edges, args.num_atoms * (args.num_atoms - 1),  args.edge_types), dtype=np.int64)
for i in range(args.num_atoms):
    incoming_edge_ids = node_incoming_edges[i]
    realization_allEdge_types[:, incoming_edge_ids, :] = realization_edge_type
realization_allEdge_types = torch.from_numpy(realization_allEdge_types)
if args.cuda:
    realization_allEdge_types = realization_allEdge_types.cuda()
print("### realization_allEdge_types shape: ", realization_allEdge_types.shape)


###
# S: subgraph
# IE: incoming-edges for one subgraph
# AE: all edges in one simualtion
# R: realizations
# B: batch
# AB: all graphs
# G: group (for the block variational)

# nodeIncomingEdges_S_IE_AE: the one-hot encoding for the in-coming edges in every subgraph
nodeIncomingEdges_S_IE_AE = np.zeros(shape=(args.num_atoms, args.num_atoms-1, args.num_atoms*(args.num_atoms-1)), dtype=np.float64) 
for i in range(args.num_atoms): # iterate over subgraphs
    for j in range(args.num_atoms - 1): # iterate over the number of incoming edges
        edge_id = node_incoming_edges[i,j]
        nodeIncomingEdges_S_IE_AE[i, j, edge_id] = 1.0
nodeIncomingEdges_S_IE_AE = torch.from_numpy(nodeIncomingEdges_S_IE_AE).cuda()
#R_IE_K: the one-hot encoding from R to the incoming edges' types






### divide the edges to groups
num_groups = args.MF_blocks
# print("num_groups: ", num_groups)
edges_in_groups = np.array_split([i for i in range(N_incoming_edges)], num_groups)
print("edges_in_groups: ", edges_in_groups) #; exit()
group_inEdge_mapping_G_IE = torch.zeros((num_groups, N_incoming_edges)) # group, and the in-coming edges in each group
for i in range(num_groups):
    this_edges = edges_in_groups[i]
    for e in this_edges:
        group_inEdge_mapping_G_IE[i, e] = 1.0
group_inEdge_mapping_G_IE = group_inEdge_mapping_G_IE.cuda()
group_size_G = np.zeros(num_groups, dtype=np.int32) # the number of edges in each group
for i in range(num_groups):
    group_size_G[i] = len(edges_in_groups[i])

group_realization_edge_G_SR_IE_K = []
for i in range(num_groups):
    num_real = args.edge_types ** group_size_G[i] # the number of realizations of this group
    this_realization_edge_SR_IE_K = torch.zeros((num_real, N_incoming_edges, args.edge_types)).cuda()
    for itr_real in range(num_real):
        # this_binary_str = f"{itr_real:0{group_size_G[i]}b}" # "01"
        this_str = np.base_repr(itr_real, base=args.edge_types)
        assert len(this_str) <= group_size_G[i]
        this_str = "0" * (group_size_G[i] - len(this_str) ) + this_str
        assert len(this_str) == group_size_G[i]
        for itr_edge in range(group_size_G[i]):
            e_id = edges_in_groups[i][itr_edge]
            mask = int(this_str[itr_edge])
            this_realization_edge_SR_IE_K[itr_real, e_id, mask] = 1.0
    group_realization_edge_G_SR_IE_K.append(this_realization_edge_SR_IE_K)
# print("group_realization_edge_G_SR_IE_K")
# print(group_realization_edge_G_SR_IE_K)
# exit()





## mean-field approximation, in groups
def cmpt_categorical_distribution_variational_block(epoch, decoder, data_loader, itr_rounds, mode):
    ## mode: ["training", "valid", "testing"]
    ## return mlEdgeType_AB_AE_K (most likely interaction type for each edge?), shape: [num_graphs, args.num_atoms * (args.num_atoms-1), args.edge_types]
    ## return latent_structure_possibility, shape: [num_graphs, args.num_atoms, args.edge_types**N_incoming_edges]
   

    tau_K_1 = tau#.cpu().detach().numpy()
    logTau_K = torch.log(tau_K_1.squeeze(-1) )

    if mode == "training":
        num_graphs = args.train_examples
    elif mode == "valid":
        num_graphs = args.valid_examples
    elif mode == "testing":
        num_graphs = args.test_examples
    else:
        print("###In cmpt_categorical_distribution_variational_block(), mode error")
        exit()


    num_all_edges = args.num_atoms * (args.num_atoms-1)


    ## "mlEdgeType_AB_AE_K" is the most likely interaction type for every edge, based on current estimation of parameters.
    # mlEdgeType_AB_AE_K = np.random.randint(low=0, high=2, size=(num_graphs, num_all_edges, args.edge_types) )
    mlEdgeType_AB_AE_K = -1 * np.ones((num_graphs, num_all_edges, args.edge_types), dtype=np.int64 )

    ## The probability for every group
    Prob_G_AB_S_SR = []
    for i in range(num_groups):
        Prob_G_AB_S_SR.append(torch.zeros(num_graphs, args.num_atoms, args.edge_types ** group_size_G[i]).cuda() )

    # for batch_idx, (data, acc, relations, pairwise_force, exp_id) in enumerate(data_loader):
    for batch_idx, (data, acc, relations, exp_id) in enumerate(data_loader):
        # batch_size equals to the number of simulations (the number of graphs)
        if args.cuda:
            data, acc, relations = data.cuda(), acc.cuda(), relations.cuda()
        this_batch_size = len(exp_id)
        with torch.no_grad():
            edgeForces_B_AE_K_T_D = decoder.cmpt_all_type_forces_all_edges(data, rel_rec, rel_send)
        # print("edgeForces_B_AE_K_T_D shape: ", edgeForces_B_AE_K_T_D.shape) # shape: [batch_size, #all_edges, K, #time_steps, dimension]

        
        ground_truth_acc_B_S_T_D = acc


        # pairwise force with current edge NN
        edgeForces_B_S_IE_K_T_D = torch.matmul(nodeIncomingEdges_S_IE_AE, edgeForces_B_AE_K_T_D.transpose(1, 3).unsqueeze(3) ) # shape: [batch_size, #time_steps, K, #subgraphs, #incoming_edges, dimension]
        edgeForces_B_S_IE_K_T_D = edgeForces_B_S_IE_K_T_D.permute(0, 3, 4, 2, 1, 5)
        edgeForces_B_S_IE_K_T_D = edgeForces_B_S_IE_K_T_D.contiguous()
        # edgeForces_B_S_IE_K_T_D = edgeForces_B_S_IE_K_T_D.cpu().detach().numpy()
        # print("edgeForces_B_S_IE_K_T_D shape: ", edgeForces_B_S_IE_K_T_D.shape) # shape:  [batch_size, #subgraphs, #incoming_edges, K, #time_steps, dimension]
        edgeForces_B_S_IE_K_T_D = edgeForces_B_S_IE_K_T_D / data[:, :, None, None, :, [-1]] # divide the mass !!!



        # mean-field factors
        Prob_G_B_S_SR = []
        for i in range(num_groups):
            # Prob_G_B_S_SR.append((1.0 / args.edge_types ** group_size_G[i]) * torch.ones((this_batch_size, args.num_atoms, args.edge_types ** group_size_G[i]) ).cuda() )
            Prob_G_B_S_SR.append((1.0 / args.edge_types ** group_size_G[i]) * torch.ones((this_batch_size, args.num_atoms, args.edge_types ** group_size_G[i]) ).cuda() )
        
        ## Update Prob_G_B_S_SR
        for _ in range(itr_rounds):
            # update very group
            for group_to_update in range(num_groups): 
                # logits_B_S_SR is the updated categorical probability of this group
                logits_B_S_SR = torch.zeros((this_batch_size, args.num_atoms, args.edge_types ** group_size_G[group_to_update])).cuda()
                
                for itr_1 in range(num_groups):
                    if itr_1 == group_to_update:
                        # the chosen dimension
                        f_itr1_B_S_SR_IE_K_T_D = edgeForces_B_S_IE_K_T_D[:, :, None, :, :, :, :] * group_realization_edge_G_SR_IE_K[itr_1][None, None, :, :, :, None, None]
                        f_itr1_B_S_SR_T_D = torch.sum(f_itr1_B_S_SR_IE_K_T_D, dim = (3, 4), keepdim = False)
                        f_itr1_B_S_SR_T_D = f_itr1_B_S_SR_T_D - ground_truth_acc_B_S_T_D[:, :, None, :, :] # absorb the net force
                        this_term_B_S_SR_T_D = f_itr1_B_S_SR_T_D * f_itr1_B_S_SR_T_D
                    else:
                        # dimension to compute expectation
                        f_itr1_B_S_SR_IE_K_T_D = edgeForces_B_S_IE_K_T_D[:, :, None, :, :, :, :] * group_realization_edge_G_SR_IE_K[itr_1][None, None, :, :, :, None, None]
                        f_itr1_B_S_SR_T_D = torch.sum(f_itr1_B_S_SR_IE_K_T_D, dim = (3, 4), keepdim = False)

                        this_term_B_S_T_D = torch.sum(f_itr1_B_S_SR_T_D * f_itr1_B_S_SR_T_D * Prob_G_B_S_SR[itr_1][:, :, :, None, None], axis = 2, keepdims=False)
                        this_term_B_S_SR_T_D = this_term_B_S_T_D[:, :, None, :, :]
                    this_term_B_S_SR = torch.sum(this_term_B_S_SR_T_D, axis=(-1, -2), keepdims = False)
                    logits_B_S_SR -= this_term_B_S_SR / (2 * args.var) # the negative in front of the logits

                # # The xy terms
                for itr_1 in range(num_groups): # absorb F_net into f_dimupdate
                    for itr_2 in range(itr_1 + 1, num_groups):

                        if itr_1 == group_to_update:
                            f_itr1_B_S_SR_IE_K_T_D = edgeForces_B_S_IE_K_T_D[:, :, None, :, :, :, :] * group_realization_edge_G_SR_IE_K[itr_1][None, None, :, :, :, None, None]
                            f_itr1_B_S_SR_T_D = torch.sum(f_itr1_B_S_SR_IE_K_T_D, dim = (3, 4), keepdim = False)
                            f_itr1_B_S_SR_T_D = f_itr1_B_S_SR_T_D - ground_truth_acc_B_S_T_D[:, :, None, :, :]# IMPORTANT: absorb F_net

                            f_itr2_B_S_SR_IE_K_T_D = edgeForces_B_S_IE_K_T_D[:, :, None, :, :, :, :] * group_realization_edge_G_SR_IE_K[itr_2][None, None, :, :, :, None, None]
                            f_itr2_B_S_SR_T_D = torch.sum(f_itr2_B_S_SR_IE_K_T_D, dim = (3, 4), keepdim = False)
                            mean_f_itr2_B_S_T_D = torch.sum(f_itr2_B_S_SR_T_D * Prob_G_B_S_SR[itr_2][:, :, :, None, None], axis = 2, keepdims=False)
                            # print("mean_f_itr2_B_S_T_D shape: ", mean_f_itr2_B_S_T_D.shape); exit() # [8, 5, 150, 2]

                            this_term_B_S_SR_T_D = f_itr1_B_S_SR_T_D * mean_f_itr2_B_S_T_D[:, :, None, :, :]

                        elif itr_2 == group_to_update:
                            f_itr1_B_S_SR_IE_K_T_D = edgeForces_B_S_IE_K_T_D[:, :, None, :, :, :, :] * group_realization_edge_G_SR_IE_K[itr_1][None, None, :, :, :, None, None]
                            f_itr1_B_S_SR_T_D = torch.sum(f_itr1_B_S_SR_IE_K_T_D, dim = (3, 4), keepdim = False)
                            mean_f_itr1_B_S_T_D = torch.sum(f_itr1_B_S_SR_T_D * Prob_G_B_S_SR[itr_1][:, :, :, None, None], axis = 2, keepdims = False)

                            f_itr2_B_S_SR_IE_K_T_D = edgeForces_B_S_IE_K_T_D[:, :, None, :, :, :, :] * group_realization_edge_G_SR_IE_K[itr_2][None, None, :, :, :, None, None]
                            f_itr2_B_S_SR_T_D = torch.sum(f_itr2_B_S_SR_IE_K_T_D, dim = (3, 4), keepdim = False)
                            f_itr2_B_S_SR_T_D = f_itr2_B_S_SR_T_D - ground_truth_acc_B_S_T_D[:, :, None, :, :] # IMPORTANT: absorb F_net

                            this_term_B_S_SR_T_D = f_itr2_B_S_SR_T_D * mean_f_itr1_B_S_T_D[:, :, None, :, :]

                        else:
                            f_itr1_B_S_SR_IE_K_T_D = edgeForces_B_S_IE_K_T_D[:, :, None, :, :, :, :] * group_realization_edge_G_SR_IE_K[itr_1][None, None, :, :, :, None, None]
                            f_itr1_B_S_SR_T_D = torch.sum(f_itr1_B_S_SR_IE_K_T_D, dim = (3, 4), keepdim = False)
                            mean_f_itr1_B_S_T_D = torch.sum(f_itr1_B_S_SR_T_D * Prob_G_B_S_SR[itr_1][:, :, :, None, None], axis = 2, keepdims = False)

                            f_itr2_B_S_SR_IE_K_T_D = edgeForces_B_S_IE_K_T_D[:, :, None, :, :, :, :] * group_realization_edge_G_SR_IE_K[itr_2][None, None, :, :, :, None, None]
                            f_itr2_B_S_SR_T_D = torch.sum(f_itr2_B_S_SR_IE_K_T_D, dim = (3, 4), keepdim = False)
                            mean_f_itr2_B_S_T_D = torch.sum(f_itr2_B_S_SR_T_D * Prob_G_B_S_SR[itr_2][:, :, :, None, None], axis = 2, keepdims = False)

                            this_term_B_S_T_D = mean_f_itr1_B_S_T_D * mean_f_itr2_B_S_T_D
                            this_term_B_S_SR_T_D = this_term_B_S_T_D[:, :, None, :, :]

                        this_term_B_S_SR = torch.sum(this_term_B_S_SR_T_D, axis=(-1, -2), keepdims = False)
                        logits_B_S_SR -= 2 * this_term_B_S_SR / (2 * args.var) # the negative in front of the logits, double (xy, yx)!!

                # the mixing coefficients
                for itr_1 in range(num_groups):
                    if itr_1 == group_to_update:
                        logTau_SR_IE_K = group_realization_edge_G_SR_IE_K[itr_1] * logTau_K[None, None, :] # select tau on every edge
                        logPi_SR_IE = torch.sum(logTau_SR_IE_K, axis = -1, keepdims = False)
                        logPi_SR = torch.sum(logPi_SR_IE, axis  = -1, keepdims = False)
                        logPi_B_S_SR = logPi_SR[None, None, :]
                        this_prior_B_S_SR = logPi_B_S_SR
                    else:
                        # the mean
                        logTau_SR_IE_K = group_realization_edge_G_SR_IE_K[itr_1] * logTau_K[None, None, :]
                        logPi_SR_IE = torch.sum(logTau_SR_IE_K, axis = -1, keepdims = False)
                        logPi_SR = torch.sum(logPi_SR_IE, axis  = -1, keepdims = False)

                        logPi_B_S_SR = Prob_G_B_S_SR[itr_1] * logPi_SR[None, None, :]
                        mean_logPi_B_S_1 = torch.sum(logPi_B_S_SR, axis = -1, keepdims = True)
                        this_prior_B_S_SR = mean_logPi_B_S_1
                    logits_B_S_SR += this_prior_B_S_SR

                realizationProb_B_S_SR = torch.softmax(logits_B_S_SR, dim = -1)
                Prob_G_B_S_SR[group_to_update][:, :, :] = realizationProb_B_S_SR
        
        for i in range(num_groups):
            Prob_G_AB_S_SR[i][exp_id, :, :] = Prob_G_B_S_SR[i]
    
    

    ## Infer the most likely edge types.
    for i in range(num_groups):
        subgroup_realization_edge_SR_IE_K = group_realization_edge_G_SR_IE_K[i].cpu().detach().numpy()

        ML_realization_AB_S = Prob_G_AB_S_SR[i].argmax(-1)
        assert len(ML_realization_AB_S) == num_graphs

        for graph_id in range(num_graphs):
            for v in range(args.num_atoms):
                ml_realizationId = ML_realization_AB_S[graph_id, v]
                incoming_edge_ids = node_incoming_edges[v]
                # the edges related to this group

                ml_subgroup_realization_edge_IE_K = subgroup_realization_edge_SR_IE_K[ml_realizationId]
                for itr in edges_in_groups[i]:
                    e_id = incoming_edge_ids[itr]
                    mlEdgeType_AB_AE_K[graph_id, e_id, :] = ml_subgroup_realization_edge_IE_K[itr]

    assert -1 not in mlEdgeType_AB_AE_K, "-1 in mlEdgeType_AB_AE_K"

    return Prob_G_AB_S_SR, mlEdgeType_AB_AE_K



def cmpt_clustering_metrics(preds, true_labels):
    AMI = metrics.adjusted_mutual_info_score(preds, true_labels)
    ARI = metrics.adjusted_rand_score(preds, true_labels)
    NMI = metrics.normalized_mutual_info_score(preds, true_labels)
    return (AMI, ARI, NMI)

def cmpt_loss(epoch, data_loader, decoder, mode):
    ## E-step
    assert args.cmpt_posterior == "mean-field-block"
    
    Prob_G_AB_S_SR, edge_type_prediction_AB_AE_K = cmpt_categorical_distribution_variational_block(epoch, decoder, data_loader, args.MF_iterations, mode)
    if epoch == 0:
        print("### In cmpt_loss, approximate posterior (mean-field-block)")
    


    edge_type_prediction_AB_AE_K = torch.from_numpy(edge_type_prediction_AB_AE_K)
    if args.cuda:
        edge_type_prediction_AB_AE_K = edge_type_prediction_AB_AE_K.cuda()
    _, ML_relation_preds = edge_type_prediction_AB_AE_K.max(-1) # shape: [num_simulation, num_node*(num_node-1)]


    ground_truth_relation = []
    nll_test = 0.0 # negative log likelihood loss
    MAE_acc_test = 0.0
    count_node_time = 0.0
    count_exp = 0

    decoder.eval()
    # for batch_idx, (data, acc, relations, pairwise_force, exp_id) in enumerate(data_loader):
    for batch_idx, (data, acc, relations, exp_id) in enumerate(data_loader):
        # pairwise_force shape: [batch_size, num_edges, time_steps, space_dim]
        this_batch_size = len(exp_id)
        if args.cuda:
            data, acc, relations = data.cuda(), acc.cuda(), relations.cuda()
        exp_id = exp_id.cpu().detach().numpy(); count_exp += len(exp_id)
        
        with torch.no_grad():
            edgeForces_B_AE_K_T_D = decoder.cmpt_all_type_forces_all_edges(data, rel_rec, rel_send) # [batch_size, all_edges, #edge_types, time_steps, space_dim]

        target = acc

        # pairwise force with current edge NN
        edgeForces_B_S_IE_K_T_D = torch.matmul(nodeIncomingEdges_S_IE_AE, edgeForces_B_AE_K_T_D.transpose(1, 3).unsqueeze(3) ) # shape: [batch_size, #time_steps, K, #subgraphs, #incoming_edges, dimension]
        # print("edgeForces_B_S_IE_K_T_D shape: ", edgeForces_B_S_IE_K_T_D.shape); exit() # [8, 100, 2, 5, 4, 2] [batch_size, time_steps, K, #subgraphs, num_incoming_edges, dim]

        edgeForces_B_S_IE_K_T_D = edgeForces_B_S_IE_K_T_D.permute(0, 3, 4, 2, 1, 5)
        edgeForces_B_S_IE_K_T_D = edgeForces_B_S_IE_K_T_D.contiguous()
        # edgeForces_B_S_IE_K_T_D = edgeForces_B_S_IE_K_T_D.cpu().detach().numpy()
        # print("edgeForces_B_S_IE_K_T_D shape: ", edgeForces_B_S_IE_K_T_D.shape) # shape:  [batch_size, #subgraphs, #incoming_edges, K, #time_steps, dimension]
        edgeForces_B_S_IE_K_T_D = edgeForces_B_S_IE_K_T_D / data[:, :, None, None, :, [-1]] # divide the mass !!!

        time_steps = edgeForces_B_S_IE_K_T_D.shape[-2]
        assert time_steps == args.used_time_steps
        netForce_B_S_T_D = torch.zeros((this_batch_size, args.num_atoms, time_steps, args.space_dim) ).cuda()


        for itr_1 in range(num_groups):
            f_itr1_B_S_SR_IE_K_T_D = edgeForces_B_S_IE_K_T_D[:, :, None, :, :, :, :] * group_realization_edge_G_SR_IE_K[itr_1][None, None, :, :, :, None, None]
            f_itr1_B_S_SR_T_D = torch.sum(f_itr1_B_S_SR_IE_K_T_D, dim = (3, 4), keepdim = False)
            this_term_B_S_T_D = torch.sum(f_itr1_B_S_SR_T_D * Prob_G_AB_S_SR[itr_1][exp_id, :, :, None, None], axis = 2, keepdims=False)
            netForce_B_S_T_D += this_term_B_S_T_D

        diff = (target - netForce_B_S_T_D) ** 2
        # diff shape: [batch_size, args.num_atoms, #realizations, args.used_time_steps, args.space_dim]

        loss_nll = torch.sum(diff)

        loss = loss_nll

        ground_truth_relation.append(relations)
        nll_test += loss_nll.item()
        MAE_acc_test += torch.sum(torch.abs(target - netForce_B_S_T_D) ).item()
        count_node_time += this_batch_size * args.num_atoms * args.used_time_steps


    ground_truth_relation = torch.cat(ground_truth_relation, dim=0) # shape: [num_simulations, num_node*(num_node-1)]
    assert ground_truth_relation.shape == ML_relation_preds.shape
    if args.edge_types == 2:
        equal_number = torch.eq(ML_relation_preds, ground_truth_relation).cpu().sum() # the predicted edge label corresponds to the ground-truth edge label
        opposite_equal_number = torch.eq(ML_relation_preds, 1-ground_truth_relation).cpu().sum() # the predicted edge label corresponds to the negative of the ground-truth edge label
        assert equal_number + opposite_equal_number == ML_relation_preds.shape[0] * ML_relation_preds.shape[1]
        counter = ML_relation_preds.shape[0] * ML_relation_preds.shape[1]

        assert count_exp == len(edge_type_prediction_AB_AE_K)
        acc_test = max([equal_number/float(counter), opposite_equal_number/float(counter)])
        acc_test = [acc_test]
    else:
        # compute the clustering metrics
        ML_relation_preds = ML_relation_preds.reshape(-1).cpu().detach().numpy()
        ground_truth_relation = ground_truth_relation.reshape(-1).cpu().detach().numpy()
        acc_test  = cmpt_clustering_metrics(ML_relation_preds, ground_truth_relation)
        

    mean_MAE_acc_test = MAE_acc_test / count_node_time
    mean_nll_test = nll_test / count_node_time

    return mean_MAE_acc_test, acc_test




def train(epoch, best_val_loss, verbose = False):
    t = time.time()

    ## Compute the categorical distribution of different realizations, and the current most likely realization
    ## E-step
    assert args.cmpt_posterior == "mean-field-block"
    if args.cmpt_posterior != "mean-field-block":
        print("Use variation approximation in this train script")
        exit()
    

    Prob_G_AB_S_SR, edge_type_prediction_AB_AE_K = cmpt_categorical_distribution_variational_block(epoch, decoder, train_loader, args.MF_iterations, "training")
    if epoch == 0:
        print("### In train, approximate posterior (mean-field-block)")
    
    
    # latent_structure_possibility shape: [num_graphs, num_structures (num_nodes), num_realizations]
    # edge_type_prediction shape: [num_graphs, num_node*(num_node-1), interaction_types]
    edge_type_prediction_AB_AE_K = torch.from_numpy(edge_type_prediction_AB_AE_K)
    if args.cuda:
        edge_type_prediction_AB_AE_K = edge_type_prediction_AB_AE_K.cuda()

    _, ML_relation_preds_AB_AE = edge_type_prediction_AB_AE_K.max(-1) # shape: [num_training_examples, num_node*(num_node-1)]
    # print("ML_relation_preds shape: ", ML_relation_preds.shape)


    ground_truth_relation = []
    nll_train = 0.0 # negative log likelihood loss
    MAE_acc_train = 0.0
    count_node_time = 0.0
    count_exp = 0
    
    # M-step: maximizing the parameters in the neural network
    decoder.train()
    # for batch_idx, (data, acc, relations, pairwise_force, exp_id) in enumerate(train_loader):
    for batch_idx, (data, acc, relations, exp_id) in enumerate(train_loader):
        # pairwise_force shape: [batch_size, num_edges, time_steps, space_dim]
        # exp_id is a 1D tensor
        this_batch_size = len(exp_id)
        # print("In train, data shape: ", data.shape)# [8, 5, 150, 5]
        if args.cuda:
            data, acc, relations, exp_id = data.cuda(), acc.cuda(), relations.cuda(), exp_id.cuda()                

        exp_id = exp_id.cpu().detach().numpy(); count_exp += len(exp_id)
        optimizer.zero_grad()
        

        edgeForces_B_AE_K_T_D = decoder.cmpt_all_type_forces_all_edges(data, rel_rec, rel_send)

        target = acc

        # pairwise force with current edge NN
        edgeForces_B_S_IE_K_T_D = torch.matmul(nodeIncomingEdges_S_IE_AE, edgeForces_B_AE_K_T_D.transpose(1, 3).unsqueeze(3) ) # shape: [batch_size, #time_steps, K, #subgraphs, #incoming_edges, dimension]
        # print("edgeForces_B_S_IE_K_T_D shape: ", edgeForces_B_S_IE_K_T_D.shape); exit()

        edgeForces_B_S_IE_K_T_D = edgeForces_B_S_IE_K_T_D.permute(0, 3, 4, 2, 1, 5)
        edgeForces_B_S_IE_K_T_D = edgeForces_B_S_IE_K_T_D.contiguous()
        # edgeForces_B_S_IE_K_T_D = edgeForces_B_S_IE_K_T_D.cpu().detach().numpy()
        # print("edgeForces_B_S_IE_K_T_D shape: ", edgeForces_B_S_IE_K_T_D.shape) # shape:  [batch_size, #subgraphs, #incoming_edges, K, #time_steps, dimension]
        edgeForces_B_S_IE_K_T_D = edgeForces_B_S_IE_K_T_D / data[:, :, None, None, :, [-1]] # divide the mass !!!

        time_steps = edgeForces_B_S_IE_K_T_D.shape[-2]
        assert time_steps == args.used_time_steps
        
        netForce_B_S_T_D = torch.zeros((this_batch_size, args.num_atoms, time_steps, args.space_dim) ).cuda()

        for itr_1 in range(num_groups):
            f_itr1_B_S_SR_IE_K_T_D = edgeForces_B_S_IE_K_T_D[:, :, None, :, :, :, :] * group_realization_edge_G_SR_IE_K[itr_1][None, None, :, :, :, None, None]
            f_itr1_B_S_SR_T_D = torch.sum(f_itr1_B_S_SR_IE_K_T_D, dim = (3, 4), keepdim = False)
            this_term_B_S_T_D = torch.sum(f_itr1_B_S_SR_T_D * Prob_G_AB_S_SR[itr_1][exp_id, :, :, None, None], axis = 2, keepdims=False)
            netForce_B_S_T_D += this_term_B_S_T_D

        diff = (target - netForce_B_S_T_D) ** 2


        loss_nll = torch.sum(diff)        
        

        # loss_nll = nll_gaussian(output, target, args.var)        

        loss = loss_nll

        ground_truth_relation.append(relations)

        loss.backward()
        optimizer.step()

        nll_train += loss_nll.item()
        MAE_acc_train += torch.sum(torch.abs(target - netForce_B_S_T_D) ).item()
        count_node_time += this_batch_size * args.num_atoms * args.used_time_steps
    scheduler.step() # call `optimizer.step()` before `lr_scheduler.step()`. details at https://pytorch.org/docs/stable/optim.html#how-to-adjust-learning-rate

    # print("group_realization_edge_G_SR_IE_K")
    # print(group_realization_edge_G_SR_IE_K[0])
    # print(group_realization_edge_G_SR_IE_K[1])
    # exit()

    mixing_coefficients_tau_new = torch.zeros((args.edge_types) ).cuda()
    for i in range(num_groups):
        countK_SR_K = group_realization_edge_G_SR_IE_K[i].sum(dim = 1)
        # print(" countK_SR_K"); print(countK_SR_K)
        gamma_AB_S_K = torch.matmul(Prob_G_AB_S_SR[i], countK_SR_K)
        # print("gamma_AB_S_K shape: ", gamma_AB_S_K.shape); exit() # shape: [num_training_examples, num_node, K]
        mixing_coefficients_tau_new += gamma_AB_S_K.sum(dim = (0, 1) )

    mixing_coefficients_tau_new = mixing_coefficients_tau_new / torch.sum(mixing_coefficients_tau_new )
    tau = mixing_coefficients_tau_new[:, None]

    assert len(tau.shape) == 2    




    ## relations prediction accuracy
    ## only suitable for K=2 case
    ground_truth_relation = torch.cat(ground_truth_relation, dim=0) # shape: [num_training_examples, num_node*(num_node-1)]
    assert ground_truth_relation.shape == ML_relation_preds_AB_AE.shape
    if args.edge_types == 2:
        equal_number = torch.eq(ML_relation_preds_AB_AE, ground_truth_relation).cpu().sum() # the predicted edge label corresponds to the ground-truth edge label
        opposite_equal_number = torch.eq(ML_relation_preds_AB_AE, 1-ground_truth_relation).cpu().sum() # the predicted edge label corresponds to the negative of the ground-truth edge label
        assert equal_number + opposite_equal_number == ML_relation_preds_AB_AE.shape[0] * ML_relation_preds_AB_AE.shape[1]
        counter = ML_relation_preds_AB_AE.shape[0] * ML_relation_preds_AB_AE.shape[1]

        assert count_exp == len(edge_type_prediction_AB_AE_K)
        acc_train = max([equal_number/float(counter), opposite_equal_number/float(counter)])
        acc_train = [acc_train]
    else:
        # compute the clustering metrics
        ML_relation_preds_AB_AE = ML_relation_preds_AB_AE.reshape(-1).cpu().detach().numpy()
        ground_truth_relation = ground_truth_relation.reshape(-1).cpu().detach().numpy()
        acc_train = cmpt_clustering_metrics(ML_relation_preds_AB_AE, ground_truth_relation)

    mean_MAE_acc_train = MAE_acc_train / count_node_time
    mean_nll_train = nll_train / count_node_time


    MAE_acc_valid, relation_acc_valid =cmpt_loss(epoch, valid_loader, decoder, "valid")

    if verbose:
        # ## save and print the training progress
        # if epoch % 1 == 0:
        #     print('Epoch: {:04d}'.format(epoch),
        #           'nll_train: {:.10f}'.format(nll_train),
        #           'mean_MAE_acc_train: {:.6E}'.format(mean_MAE_acc_train),
        #           'mean_nll_train: {:.6E}'.format(mean_nll_train),
        #           'acc_train: {:.10f}'.format(acc_train),
        #           'MAE_acc_valid: {:.6E}'.format(MAE_acc_valid),
        #           'relation_acc_valid: {:.10f}'.format(relation_acc_valid),
        #           'tau: {}'.format(tau.cpu().detach().numpy()[:, 0]), file=log)

        decoder_file_epoch = os.path.join(save_folder, 'saved_model/decoder_{}.pt'.format(epoch))
        torch.save(decoder.state_dict(), decoder_file_epoch)

        mixing_coefficients_tau_file = os.path.join(save_folder, "saved_model/tau_{}.pkl".format(epoch))
        with open(mixing_coefficients_tau_file, "wb") as f:
            pickle.dump(tau.cpu().detach().numpy(), f)


    if args.save_folder and MAE_acc_valid < best_val_loss:
    # if args.save_folder:
        print('Epoch: {:04d}'.format(epoch),
              'nll_train: {:.10f}'.format(nll_train),
              'mean_MAE_acc_train: {:.6E}'.format(mean_MAE_acc_train),
              'mean_nll_train: {:.6E}'.format(mean_nll_train),
              # 'acc_train: {:.10f}'.format(acc_train),
              'acc_train: (' + ', '.join(format(f, '.6f') for f in acc_train) + ')',
              'MAE_acc_valid: {:.6E}'.format(MAE_acc_valid),
              # 'relation_acc_valid: {:.10f}'.format(relation_acc_valid),
              'relation_acc_valid: (' + ', '.join(format(f, '.6f') for f in relation_acc_valid) + ')',
              'tau: {}'.format(tau.cpu().detach().numpy()[:, 0]))

        print('Epoch: {:04d}'.format(epoch),
              'nll_train: {:.10f}'.format(nll_train),
              'mean_MAE_acc_train: {:.6E}'.format(mean_MAE_acc_train),
              'mean_nll_train: {:.6E}'.format(mean_nll_train),
              # 'acc_train: {:.10f}'.format(acc_train),
              'acc_train: (' + ', '.join(format(f, '.6f') for f in acc_train) + ')',
              'MAE_acc_valid: {:.6E}'.format(MAE_acc_valid),
              # 'relation_acc_valid: {:.10f}'.format(relation_acc_valid),
              'relation_acc_valid: (' + ', '.join(format(f, '.6f') for f in relation_acc_valid) + ')',
              'tau: {}'.format(tau.cpu().detach().numpy()[:, 0]), file=log)
        log.flush()
        # save the best model
        torch.save(decoder.state_dict(), best_decoder_file)
        with open(best_mixing_coefficients_file, "wb") as f:
            pickle.dump(tau.cpu().detach().numpy(), f)
        
    
    edge_type_prediction_AB_AE_K = edge_type_prediction_AB_AE_K.cpu().detach().numpy()
    return MAE_acc_valid, np.argmax(edge_type_prediction_AB_AE_K, axis=-1)







if __name__ == "__main__":
    # Save model and meta-data. Always saves in a new sub-folder.
    if args.save_folder:
        from os import listdir
        
        if args.noisy_input:
            save_folder = '{}/{}simulations_{}timesteps/{}/'.format(args.save_folder, args.train_examples, args.used_time_steps, args.noisy_level)
            Path(save_folder).mkdir(parents=True, exist_ok=True)
            experiments_done = len(listdir(save_folder))
            save_folder = save_folder + 'exp{}/'.format(args.save_folder, args.train_examples, args.used_time_steps, args.noisy_level, experiments_done + 1)
        else:
            save_folder = '{}/{}simulations_{}timesteps/'.format(args.save_folder, args.train_examples, args.used_time_steps)
            Path(save_folder).mkdir(parents=True, exist_ok=True)
            experiments_done = len(listdir(save_folder))
            save_folder = save_folder + 'exp{}/'.format(experiments_done + 1)
        print("save_folder is: ", save_folder)
        
        Path(save_folder).mkdir(parents=True, exist_ok=True)    
        Path(save_folder+"saved_model/").mkdir(parents=True, exist_ok=True)
        meta_file = os.path.join(save_folder, 'metadata.pkl')
        best_decoder_file = os.path.join(save_folder, 'best_decoder.pt')
        best_mixing_coefficients_file = os.path.join(save_folder, 'best_decoder_tau.pkl')
        log_file = os.path.join(save_folder, 'log.txt')
        log = open(log_file, 'w')
        with open(meta_file, "wb") as f:
            pickle.dump({'args': args}, f)
    else:
        print("WARNING: No save_folder provided!" +
              "Testing (within this script) will throw an error.")


    ## load data
    if args.noisy_input:
        print("No nonisy input")
        exit()
        train_loader, valid_loader, test_loader, loc_max, loc_min, vel_max, vel_min = load_noisy_data(
        args.input_dir, args.batch_size, args.suffix, args.noisy_level, args.train_examples, args.valid_examples, args.test_examples)
    else:
        train_loader, valid_loader, test_loader, loc_max, loc_min, vel_max, vel_min = load_data(
        args.input_dir, args.batch_size, args.suffix, args.used_time_steps, args.train_examples, args.valid_examples, args.test_examples, save_min_max_dir=save_folder, all_train_exp = args.all_train_exp, all_valid_exp = args.all_valid_exp, all_test_exp = args.all_test_exp)


    ### The model and optimizer

    if args.decoder == 'pignpi':
        decoder = MLP_PIGNPI_Decoder(n_in_node=args.dims,
                             edge_types=args.edge_types,
                             msg_hid=args.decoder_hidden,
                             msg_out=args.decoder_hidden,
                             n_hid=args.decoder_hidden,
                             delta_t=args.delta_T, # added
                             loc_max=loc_max, # added
                             loc_min=loc_min, # added
                             vel_max=vel_max, # added
                             vel_min=vel_min, # added
                             do_prob=args.decoder_dropout,
                             skip_first=args.skip_first,
                             data_type = data_type)
    else:
        print("decoder init error")
        exit()
    decoder.cuda()

    ## the coefficients
    tau = np.array([1.0 / args.edge_types] * args.edge_types)
    tau = tau.reshape(args.edge_types, 1)
    tau = torch.from_numpy(tau).cuda()
    print("### Initialize tau as: ", tau)

    optimizer = optim.Adam(list(decoder.parameters()), lr=args.lr)
    scheduler = lr_scheduler.StepLR(optimizer, step_size=args.lr_decay, gamma=args.gamma)




    # Train model
    decoder_file_epoch = os.path.join(save_folder, 'saved_model/decoder_{}.pt'.format("before_train"))
    torch.save(decoder.state_dict(), decoder_file_epoch)
    mixing_coefficients_tau_file = os.path.join(save_folder, "saved_model/tau_{}.pt".format("before_train") )
    with open(mixing_coefficients_tau_file, "wb") as f:
        pickle.dump(tau.cpu().detach().numpy(), f)

    t_total = time.time()
    best_val_loss = np.inf
    best_epoch = 0
    for epoch in tqdm(range(args.epochs)):
        val_loss, edge_type_prediction = train(epoch, best_val_loss) #["exact", "mean-field", "mean-field-block"]
        if epoch == 0:
            edge_type_prediction_previous = edge_type_prediction
        else:
            equal_number = np.equal(edge_type_prediction_previous, edge_type_prediction).sum()
            unequal_number = (~np.equal(edge_type_prediction_previous, edge_type_prediction)).sum()
            if unequal_number > 0:
                print("Changed: {}, Unchanged: {}".format(unequal_number, equal_number))
            edge_type_prediction_previous = edge_type_prediction

        if val_loss < best_val_loss:
            best_val_loss = val_loss
            best_epoch = epoch
    print("Optimization Finished!")
    print("Best Epoch: {:04d}".format(best_epoch))
    if args.save_folder:
        print("Best Epoch: {:04d}".format(best_epoch), file=log)
        log.flush()
    log.close()



