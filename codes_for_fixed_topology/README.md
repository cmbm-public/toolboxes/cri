# CRI and Var-CRI

This folder contains the codes of CRI and Var-CRI, which are designed for relational inference for the particle systems with the fixed graph topology, _i.e._, each particle interacts with consistent neighbors across different time steps.  



### Usage
- Check `train_CRI.bash` to train CRI for different experiments  
- Check `train_VarCRI.bash` to train Var-CRI for different experiments  