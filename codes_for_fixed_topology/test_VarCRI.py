from __future__ import division
from __future__ import print_function

import time
import argparse
import pickle
import os
import datetime
from tqdm import tqdm

from pathlib import Path
import itertools
import math

import warnings
warnings.filterwarnings("error")


import torch 
torch.set_num_threads(4)
torch.set_default_dtype(torch.float64)


import torch.optim as optim
from torch.optim import lr_scheduler

from utils import *
from modules import MLP_PIGNPI_Decoder

parser = argparse.ArgumentParser()
parser.add_argument('--no-cuda', action='store_true', default=False,
                    help='Disables CUDA training.')
parser.add_argument('--seed', type=int, default=-1, help='Random seed.') # 42
parser.add_argument('--epochs', type=int, default=500,
                    help='Number of epochs to train.')
parser.add_argument('--batch-size', type=int, default=8,
                    help='Number of samples per batch.')
parser.add_argument('--lr', type=float, default=1e-3,
                    help='Initial learning rate.')
# parser.add_argument('--encoder-hidden', type=int, default=256, help='Number of hidden units.')
parser.add_argument('--decoder-hidden', type=int, default=256,
                    help='Number of hidden units.')
parser.add_argument('--temp', type=float, default=0.5,
                    help='Temperature for Gumbel softmax.')
parser.add_argument('--num-atoms', type=int, default=5,
                    help='Number of atoms in simulation.')
# parser.add_argument('--encoder', type=str, default='mlp', help='Type of path encoder model (mlp or cnn).')
parser.add_argument('--decoder', type=str, default='pignpi',
                    help='Type of decoder model (pignpi, rnn, or sim).')
parser.add_argument('--no-factor', action='store_true', default=False,
                    help='Disables factor graph model.')
parser.add_argument('--suffix', type=str, default='_springs5',
                    help='Suffix for training data (e.g. "_charged".')
# parser.add_argument('--encoder-dropout', type=float, default=0.0, help='Dropout rate (1 - keep probability).')
parser.add_argument('--decoder-dropout', type=float, default=0.0,
                    help='Dropout rate (1 - keep probability).')
parser.add_argument('--load-folder', type=str, default='',
                    help='Where to load the trained model if finetunning. ' +
                         'Leave empty to train from scratch')
parser.add_argument('--edge_types', type=int, default=2,
                    help='The number of edge types to infer.')
parser.add_argument('--space-dim', type=int, default=2, help='The space dimension.')
parser.add_argument('--dims', type=int, default=5, # add mass
                    help='The number of input dimensions (position + velocity + mass).')
# parser.add_argument('--timesteps', type=int, default=500, # 49
#                     help='The number of time steps per sample.')
# parser.add_argument('--prediction_steps', type=int, default=10, metavar='N',
#                     help='Num steps to predict before re-using teacher forcing.')
parser.add_argument('--lr-decay', type=int, default=200,
                    help='After how epochs to decay LR by a factor of gamma.')
parser.add_argument('--gamma', type=float, default=0.5,
                    help='LR decay factor.')
parser.add_argument('--skip-first', action='store_true', default=False,
                    help='Skip first edge type in decoder, i.e. it represents no-edge.')
parser.add_argument('--var', type=float, default=0.05, # 0.5 for spring works well
                    help='Output variance.')
parser.add_argument('--hard', action='store_true', default=False,
                    help='Uses discrete samples in training forward pass.')
parser.add_argument('--prior', action='store_true', default=False,
                    help='Whether to use sparsity prior.')
parser.add_argument('--input-dir', type=str, default='./data/',
                    help='The directory containing input data.')
parser.add_argument('--train-examples', type=int, default=500, # 600, 3000
                    help='Number of train examples (number of simulations).')
parser.add_argument('--valid-examples', type=int, default=150, # 200, 500
                    help='Number of valid examples (number of simulations).')
parser.add_argument('--test-examples', type=int, default=150,  # 200, 500
                    help='Number of test examples (number of simulations).')

parser.add_argument('--delta-T', type=float, default=0.01, help='simulation time step size.')

# number of parallel processes
parser.add_argument('--noisy-input', action='store_true', default=False,
                    help='train the model with noisy input.')
parser.add_argument('--noisy-level', type=float, default=-1,
                    help='noisy level.')

# number of parallel processes
# parser.add_argument('--num-processes', type=int, default=10, help='Number of atoms in simulation.')

# how many time steps to use
parser.add_argument('--used-time-steps', type=int, default=100, help='Number of time steps in each simulation to use.')
# which method to use for computing the posterior
parser.add_argument('--cmpt_posterior', type=str, default='mean-field-block', help='Which method to use for computing the posterior ("exact", "mean-field", or "mean-field-block").')
# With mean-field approximation:
parser.add_argument('--MF-blocks', type=int, default=2, help='Number of blocks for the mean-field')
parser.add_argument('--MF-iterations', type=int, default=10, help='Number of iterations for computing mean-field approximation')


## input
parser.add_argument('--all_train_exp', type=int, default=-1, help='Number of ALL training time steps in each simulation')
parser.add_argument('--all_valid_exp', type=int, default=-1, help='Number of ALL validation time steps in each simulation')
parser.add_argument('--all_test_exp', type=int, default=-1, help='Number of ALL testing time steps in each simulation')



# path of the trained model
parser.add_argument('--trained_model_path', type=str, default='', help='Path of trained model')

## test generalization or not
parser.add_argument('--test_generalization', type=str, default='', help='FALSE or TRUE' )



args = parser.parse_args()
args.cuda = not args.no_cuda and torch.cuda.is_available()
args.factor = not args.no_factor

if args.noisy_level != -1:
    assert args.noisy_input is True

assert args.load_folder == ''
assert args.cuda == True
assert args.cmpt_posterior == "mean-field-block", "args.cmpt_posterior: {}".format(args.cmpt_posterior)
print(args)

if args.seed != -1:
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    if args.cuda:
        torch.cuda.manual_seed(args.seed)


data_type = args.suffix[1:7]
assert data_type in ["spring", "charge"]


assert args.test_generalization in ["TRUE", "FALSE"]


### Global variables
## rel_rec: the edge-receiver matrix. Shape [num_edges, num_atoms], num_edges = num_atoms * (num_atoms - 1)
## rel_send: the edge-sender matrix. Shape [num_edges, num_atoms]
off_diag = np.ones([args.num_atoms, args.num_atoms]) - np.eye(args.num_atoms)
rel_rec = np.array(encode_onehot(np.where(off_diag)[0]), dtype=np.float64)# ; print(rel_rec.shape) ; exit() # (num_edges, num_particles)
rel_send = np.array(encode_onehot(np.where(off_diag)[1]), dtype=np.float64)
rel_rec = torch.from_numpy(rel_rec).cuda()
rel_send = torch.from_numpy(rel_send).cuda()

N_incoming_edges = args.num_atoms - 1




def return_incoming_edges():
    # return the ids of incoming edges (num_node-1) for every node
    # incoming_edges, type: np.array, shape: [num_node, num_node-1]
    incoming_edges = np.zeros(shape=(args.num_atoms, args.num_atoms-1), dtype=np.int64)
    num_edges_in_graph = args.num_atoms * (args.num_atoms - 1)
    for i in range(args.num_atoms):
        # i is the center node to consider
        assert len(rel_rec[:, i]) == num_edges_in_graph # rel_rec shape: [num_edge, num_node]
        incoming_edge_ids = [j for j in range(num_edges_in_graph) if rel_rec[j, i] == 1]
        assert len(incoming_edge_ids) == args.num_atoms - 1
        incoming_edges[i] = np.array(incoming_edge_ids)
    return incoming_edges


def return_latent_space():
    # The space for all possible realizations for every subgraph
    # for every n-1 edges (incoming edges for a node), return the all possible combinations of interaction type on these n-1 edges
    # return shape: [args.edge_types**(num_node-1), num_node-1, edge_types]
    # another implementation way: https://stackoverflow.com/questions/63839024/one-hot-encode-numpy-array-with-2-dims
    N_all_combinations = args.edge_types**N_incoming_edges
    edges_states = np.zeros(shape=(N_all_combinations, N_incoming_edges, args.edge_types), dtype=np.int64) # todo: np.int64 can be reduced
    for i in range(N_all_combinations):
        # this_binary_str = f"{i:0{N_incoming_edges}b}" # "0001"
        this_str = np.base_repr(i, base=args.edge_types)
        assert len(this_str) <= N_incoming_edges
        this_str = "0" * (N_incoming_edges - len(this_str) ) + this_str
        assert len(this_str) == N_incoming_edges
        for e_id in range(N_incoming_edges):
            mask = this_str[e_id]
            mask = int(mask)
            edges_states[i, e_id, mask] = 1
    return edges_states


### Global variables
## node_incoming_edges: the ids of incoming edges for every node. Shape: [num_node, num_node-1].
node_incoming_edges = return_incoming_edges()

## realization_edge_type: (one-hot encoded) the edge types corresponding to every realization of the subgraph (the mapping from subgraph class to its edge types). 
## realization_edge_type Shape: [#realizations (edge_types**(num_node-1)), #incoming_edges, edge_types (K)]
realization_edge_type = return_latent_space() 




## realization_allEdge_types is the types of all edges in the graph (expanding realization_edge_type)
## realization_allEdge_types shape: [#realizations, #all_edges_in_one_graph, K]
realization_allEdge_types = np.zeros(shape=(args.edge_types**N_incoming_edges, args.num_atoms * (args.num_atoms - 1),  args.edge_types), dtype=np.int64)
for i in range(args.num_atoms):
    incoming_edge_ids = node_incoming_edges[i]
    realization_allEdge_types[:, incoming_edge_ids, :] = realization_edge_type
realization_allEdge_types = torch.from_numpy(realization_allEdge_types)
if args.cuda:
    realization_allEdge_types = realization_allEdge_types.cuda()
print("### realization_allEdge_types shape: ", realization_allEdge_types.shape)


###
# S: subgraph
# IE: incoming-edges for one subgraph
# AE: all edges in one simualtion
# R: realizations

# nodeIncomingEdges_S_IE_AE: the one-hot encoding for the in-coming edges in every subgraph
nodeIncomingEdges_S_IE_AE = np.zeros(shape=(args.num_atoms, args.num_atoms-1, args.num_atoms*(args.num_atoms-1)), dtype=np.float64) 
for i in range(args.num_atoms): # iterate over subgraphs
    for j in range(args.num_atoms - 1): # iterate over the number of incoming edges
        edge_id = node_incoming_edges[i,j]
        nodeIncomingEdges_S_IE_AE[i, j, edge_id] = 1.0
nodeIncomingEdges_S_IE_AE = torch.from_numpy(nodeIncomingEdges_S_IE_AE).cuda()
#R_IE_K: the one-hot encoding from R to the incoming edges' types




### divide the edges to groups
num_groups = args.MF_blocks
# print("num_groups: ", num_groups)
edges_in_groups = np.array_split([i for i in range(N_incoming_edges)], num_groups)
print("edges_in_groups: ", edges_in_groups) #; exit()
group_inEdge_mapping_G_IE = torch.zeros((num_groups, N_incoming_edges)) # group, and the in-coming edges in each group
for i in range(num_groups):
    this_edges = edges_in_groups[i]
    for e in this_edges:
        group_inEdge_mapping_G_IE[i, e] = 1.0
group_inEdge_mapping_G_IE = group_inEdge_mapping_G_IE.cuda()
group_size_G = np.zeros(num_groups, dtype=np.int32) # the number of edges in each group
for i in range(num_groups):
    group_size_G[i] = len(edges_in_groups[i])

group_realization_edge_G_SR_IE_K = []
for i in range(num_groups):
    num_real = args.edge_types ** group_size_G[i] # the number of realizations of this group
    this_realization_edge_SR_IE_K = torch.zeros((num_real, N_incoming_edges, args.edge_types)).cuda()
    for itr_real in range(num_real):
        # this_binary_str = f"{itr_real:0{group_size_G[i]}b}" # "01"
        this_str = np.base_repr(itr_real, base=args.edge_types)
        assert len(this_str) <= group_size_G[i]
        this_str = "0" * (group_size_G[i] - len(this_str) ) + this_str
        assert len(this_str) == group_size_G[i]
        for itr_edge in range(group_size_G[i]):
            e_id = edges_in_groups[i][itr_edge]
            mask = int(this_str[itr_edge])
            this_realization_edge_SR_IE_K[itr_real, e_id, mask] = 1.0
    group_realization_edge_G_SR_IE_K.append(this_realization_edge_SR_IE_K)
# print("group_realization_edge_G_SR_IE_K")
# print(group_realization_edge_G_SR_IE_K)
# exit()






## mean-field approximation, in groups
def cmpt_categorical_distribution_variational_block(decoder, tau, data_loader, itr_rounds, mode):
    ## mode: ["training", "valid", "testing"]
    ## return mlEdgeType_AB_AE_K (most likely interaction type for each edge?), shape: [num_graphs, args.num_atoms * (args.num_atoms-1), args.edge_types]
    ## return latent_structure_possibility, shape: [num_graphs, args.num_atoms, args.edge_types**N_incoming_edges]
   

    tau_K_1 = tau#.cpu().detach().numpy()
    logTau_K = torch.log(tau_K_1.squeeze(-1) )

    if mode == "training":
        num_graphs = args.train_examples
    elif mode == "valid":
        num_graphs = args.valid_examples
    elif mode == "testing":
        num_graphs = args.test_examples
    else:
        print("###In cmpt_categorical_distribution_variational_block(), mode error")
        exit()


    num_all_edges = args.num_atoms * (args.num_atoms-1)


    ## "mlEdgeType_AB_AE_K" is the most likely interaction type for every edge, based on current estimation of parameters.
    # mlEdgeType_AB_AE_K = np.random.randint(low=0, high=2, size=(num_graphs, num_all_edges, args.edge_types) )
    mlEdgeType_AB_AE_K = -1 * np.ones((num_graphs, num_all_edges, args.edge_types), dtype=np.int64 )

    ## The probability for every group
    Prob_G_AB_S_SR = []
    for i in range(num_groups):
        Prob_G_AB_S_SR.append(torch.zeros(num_graphs, args.num_atoms, args.edge_types ** group_size_G[i]).cuda() )

    for batch_idx, (data, acc, relations, pairwise_force, exp_id) in enumerate(data_loader):
    # for batch_idx, (data, acc, relations, exp_id) in enumerate(data_loader):
        # batch_size equals to the number of simulations (the number of graphs)
        if args.cuda:
            data, acc, relations = data.cuda(), acc.cuda(), relations.cuda()
        this_batch_size = len(exp_id)
        with torch.no_grad():
            edgeForces_B_AE_K_T_D = decoder.cmpt_all_type_forces_all_edges(data, rel_rec, rel_send)
        # print("edgeForces_B_AE_K_T_D shape: ", edgeForces_B_AE_K_T_D.shape) # shape: [batch_size, #all_edges, K, #time_steps, dimension]

        
        ground_truth_acc_B_S_T_D = acc


        # pairwise force with current edge NN
        edgeForces_B_S_IE_K_T_D = torch.matmul(nodeIncomingEdges_S_IE_AE, edgeForces_B_AE_K_T_D.transpose(1, 3).unsqueeze(3) ) # shape: [batch_size, #time_steps, K, #subgraphs, #incoming_edges, dimension]
        edgeForces_B_S_IE_K_T_D = edgeForces_B_S_IE_K_T_D.permute(0, 3, 4, 2, 1, 5)
        edgeForces_B_S_IE_K_T_D = edgeForces_B_S_IE_K_T_D.contiguous()
        # edgeForces_B_S_IE_K_T_D = edgeForces_B_S_IE_K_T_D.cpu().detach().numpy()
        # print("edgeForces_B_S_IE_K_T_D shape: ", edgeForces_B_S_IE_K_T_D.shape) # shape:  [batch_size, #subgraphs, #incoming_edges, K, #time_steps, dimension]
        edgeForces_B_S_IE_K_T_D = edgeForces_B_S_IE_K_T_D / data[:, :, None, None, :, [-1]] # divide the mass !!!



        # mean-field factors
        Prob_G_B_S_SR = []
        for i in range(num_groups):
            # Prob_G_B_S_SR.append((1.0 / args.edge_types ** group_size_G[i]) * torch.ones((this_batch_size, args.num_atoms, args.edge_types ** group_size_G[i]) ).cuda() )
            Prob_G_B_S_SR.append((1.0 / args.edge_types ** group_size_G[i]) * torch.ones((this_batch_size, args.num_atoms, args.edge_types ** group_size_G[i]) ).cuda() )
        
        ## Update Prob_G_B_S_SR
        for _ in range(itr_rounds):
            # update very group
            for group_to_update in range(num_groups): 
                # logits_B_S_SR is the updated categorical probability of this group
                logits_B_S_SR = torch.zeros((this_batch_size, args.num_atoms, args.edge_types ** group_size_G[group_to_update])).cuda()
                
                for itr_1 in range(num_groups):
                    if itr_1 == group_to_update:
                        # the chosen dimension
                        f_itr1_B_S_SR_IE_K_T_D = edgeForces_B_S_IE_K_T_D[:, :, None, :, :, :, :] * group_realization_edge_G_SR_IE_K[itr_1][None, None, :, :, :, None, None]
                        f_itr1_B_S_SR_T_D = torch.sum(f_itr1_B_S_SR_IE_K_T_D, dim = (3, 4), keepdim = False)
                        f_itr1_B_S_SR_T_D = f_itr1_B_S_SR_T_D - ground_truth_acc_B_S_T_D[:, :, None, :, :] # absorb the net force
                        this_term_B_S_SR_T_D = f_itr1_B_S_SR_T_D * f_itr1_B_S_SR_T_D
                    else:
                        # dimension to compute expectation
                        f_itr1_B_S_SR_IE_K_T_D = edgeForces_B_S_IE_K_T_D[:, :, None, :, :, :, :] * group_realization_edge_G_SR_IE_K[itr_1][None, None, :, :, :, None, None]
                        f_itr1_B_S_SR_T_D = torch.sum(f_itr1_B_S_SR_IE_K_T_D, dim = (3, 4), keepdim = False)

                        this_term_B_S_T_D = torch.sum(f_itr1_B_S_SR_T_D * f_itr1_B_S_SR_T_D * Prob_G_B_S_SR[itr_1][:, :, :, None, None], axis = 2, keepdims=False)
                        this_term_B_S_SR_T_D = this_term_B_S_T_D[:, :, None, :, :]
                    this_term_B_S_SR = torch.sum(this_term_B_S_SR_T_D, axis=(-1, -2), keepdims = False)
                    logits_B_S_SR -= this_term_B_S_SR / (2 * args.var) # the negative in front of the logits

                # # The xy terms
                for itr_1 in range(num_groups): # absorb F_net into f_dimupdate
                    for itr_2 in range(itr_1 + 1, num_groups):

                        if itr_1 == group_to_update:
                            f_itr1_B_S_SR_IE_K_T_D = edgeForces_B_S_IE_K_T_D[:, :, None, :, :, :, :] * group_realization_edge_G_SR_IE_K[itr_1][None, None, :, :, :, None, None]
                            f_itr1_B_S_SR_T_D = torch.sum(f_itr1_B_S_SR_IE_K_T_D, dim = (3, 4), keepdim = False)
                            f_itr1_B_S_SR_T_D = f_itr1_B_S_SR_T_D - ground_truth_acc_B_S_T_D[:, :, None, :, :]# IMPORTANT: absorb F_net

                            f_itr2_B_S_SR_IE_K_T_D = edgeForces_B_S_IE_K_T_D[:, :, None, :, :, :, :] * group_realization_edge_G_SR_IE_K[itr_2][None, None, :, :, :, None, None]
                            f_itr2_B_S_SR_T_D = torch.sum(f_itr2_B_S_SR_IE_K_T_D, dim = (3, 4), keepdim = False)
                            mean_f_itr2_B_S_T_D = torch.sum(f_itr2_B_S_SR_T_D * Prob_G_B_S_SR[itr_2][:, :, :, None, None], axis = 2, keepdims=False)
                            # print("mean_f_itr2_B_S_T_D shape: ", mean_f_itr2_B_S_T_D.shape); exit() # [8, 5, 150, 2]

                            this_term_B_S_SR_T_D = f_itr1_B_S_SR_T_D * mean_f_itr2_B_S_T_D[:, :, None, :, :]

                        elif itr_2 == group_to_update:
                            f_itr1_B_S_SR_IE_K_T_D = edgeForces_B_S_IE_K_T_D[:, :, None, :, :, :, :] * group_realization_edge_G_SR_IE_K[itr_1][None, None, :, :, :, None, None]
                            f_itr1_B_S_SR_T_D = torch.sum(f_itr1_B_S_SR_IE_K_T_D, dim = (3, 4), keepdim = False)
                            mean_f_itr1_B_S_T_D = torch.sum(f_itr1_B_S_SR_T_D * Prob_G_B_S_SR[itr_1][:, :, :, None, None], axis = 2, keepdims = False)

                            f_itr2_B_S_SR_IE_K_T_D = edgeForces_B_S_IE_K_T_D[:, :, None, :, :, :, :] * group_realization_edge_G_SR_IE_K[itr_2][None, None, :, :, :, None, None]
                            f_itr2_B_S_SR_T_D = torch.sum(f_itr2_B_S_SR_IE_K_T_D, dim = (3, 4), keepdim = False)
                            f_itr2_B_S_SR_T_D = f_itr2_B_S_SR_T_D - ground_truth_acc_B_S_T_D[:, :, None, :, :] # IMPORTANT: absorb F_net

                            this_term_B_S_SR_T_D = f_itr2_B_S_SR_T_D * mean_f_itr1_B_S_T_D[:, :, None, :, :]

                        else:
                            f_itr1_B_S_SR_IE_K_T_D = edgeForces_B_S_IE_K_T_D[:, :, None, :, :, :, :] * group_realization_edge_G_SR_IE_K[itr_1][None, None, :, :, :, None, None]
                            f_itr1_B_S_SR_T_D = torch.sum(f_itr1_B_S_SR_IE_K_T_D, dim = (3, 4), keepdim = False)
                            mean_f_itr1_B_S_T_D = torch.sum(f_itr1_B_S_SR_T_D * Prob_G_B_S_SR[itr_1][:, :, :, None, None], axis = 2, keepdims = False)

                            f_itr2_B_S_SR_IE_K_T_D = edgeForces_B_S_IE_K_T_D[:, :, None, :, :, :, :] * group_realization_edge_G_SR_IE_K[itr_2][None, None, :, :, :, None, None]
                            f_itr2_B_S_SR_T_D = torch.sum(f_itr2_B_S_SR_IE_K_T_D, dim = (3, 4), keepdim = False)
                            mean_f_itr2_B_S_T_D = torch.sum(f_itr2_B_S_SR_T_D * Prob_G_B_S_SR[itr_2][:, :, :, None, None], axis = 2, keepdims = False)

                            this_term_B_S_T_D = mean_f_itr1_B_S_T_D * mean_f_itr2_B_S_T_D
                            this_term_B_S_SR_T_D = this_term_B_S_T_D[:, :, None, :, :]

                        this_term_B_S_SR = torch.sum(this_term_B_S_SR_T_D, axis=(-1, -2), keepdims = False)
                        logits_B_S_SR -= 2 * this_term_B_S_SR / (2 * args.var) # the negative in front of the logits, double (xy, yx)!!

                # the mixing coefficients
                for itr_1 in range(num_groups):
                    if itr_1 == group_to_update:
                        logTau_SR_IE_K = group_realization_edge_G_SR_IE_K[itr_1] * logTau_K[None, None, :] # select tau on every edge
                        logPi_SR_IE = torch.sum(logTau_SR_IE_K, axis = -1, keepdims = False)
                        logPi_SR = torch.sum(logPi_SR_IE, axis  = -1, keepdims = False)
                        logPi_B_S_SR = logPi_SR[None, None, :]
                        this_prior_B_S_SR = logPi_B_S_SR
                    else:
                        # the mean
                        logTau_SR_IE_K = group_realization_edge_G_SR_IE_K[itr_1] * logTau_K[None, None, :]
                        logPi_SR_IE = torch.sum(logTau_SR_IE_K, axis = -1, keepdims = False)
                        logPi_SR = torch.sum(logPi_SR_IE, axis  = -1, keepdims = False)

                        logPi_B_S_SR = Prob_G_B_S_SR[itr_1] * logPi_SR[None, None, :]
                        mean_logPi_B_S_1 = torch.sum(logPi_B_S_SR, axis = -1, keepdims = True)
                        this_prior_B_S_SR = mean_logPi_B_S_1
                    logits_B_S_SR += this_prior_B_S_SR

                realizationProb_B_S_SR = torch.softmax(logits_B_S_SR, dim = -1)
                Prob_G_B_S_SR[group_to_update][:, :, :] = realizationProb_B_S_SR
        
        for i in range(num_groups):
            Prob_G_AB_S_SR[i][exp_id, :, :] = Prob_G_B_S_SR[i]
    
    

    ## Infer the most likely edge types.
    for i in range(num_groups):
        subgroup_realization_edge_SR_IE_K = group_realization_edge_G_SR_IE_K[i].cpu().detach().numpy()

        ML_realization_AB_S = Prob_G_AB_S_SR[i].argmax(-1)
        assert len(ML_realization_AB_S) == num_graphs

        for graph_id in range(num_graphs):
            for v in range(args.num_atoms):
                ml_realizationId = ML_realization_AB_S[graph_id, v]
                incoming_edge_ids = node_incoming_edges[v]
                # the edges related to this group

                ml_subgroup_realization_edge_IE_K = subgroup_realization_edge_SR_IE_K[ml_realizationId]
                for itr in edges_in_groups[i]:
                    e_id = incoming_edge_ids[itr]
                    mlEdgeType_AB_AE_K[graph_id, e_id, :] = ml_subgroup_realization_edge_IE_K[itr]

    assert -1 not in mlEdgeType_AB_AE_K, "-1 in mlEdgeType_AB_AE_K"

    return Prob_G_AB_S_SR, mlEdgeType_AB_AE_K





def cmpt_permutation_accuracy(preds, true_labels):
    
    all_permutations = list(itertools.permutations([i for i in range(args.edge_types)]))
    assert len(all_permutations) == math.factorial(args.edge_types)

    equal_number_count = 0.0
    acc_list = []
    for i in range(len(all_permutations)):
        this_list = list(all_permutations[i])
        this_map = dict()
        assert len(this_list) == args.edge_types
        for l in range(len(this_list)):
            this_map[l] = this_list[l]
        print(this_list)
        print(this_map)
        # exit()
        preds_transformed = [this_map[k] for k in preds]
        preds_transformed = np.array(preds_transformed)
        equal_number = np.equal(preds_transformed, true_labels).sum() # the predicted edge label corresponds to the ground-truth edge label
        equal_number_count += equal_number
        counter = len(true_labels)
        acc = equal_number / float(counter)
        acc_list.append(acc)
    print("equal_number_count: ", equal_number_count)
    # print("accuracy: ", np.max(acc_list))
    print("acc_list: ", acc_list)
    return np.max(acc_list), list(all_permutations[np.argmax(acc_list)])



def cmpt_relation_accuracy(decoder, tau, data_loader, mode):
    ## compute the categorical distributions exactly
    # latent_structure_possibility, edge_type_prediction = cmpt_categorical_distribution(decoder, tau, data_loader, mode)
    Prob_G_AB_S_SR, edge_type_prediction_AB_AE_K = cmpt_categorical_distribution_variational_block(decoder, tau, data_loader, args.MF_iterations, mode)

    edge_type_prediction_AB_AE_K = torch.from_numpy(edge_type_prediction_AB_AE_K)
    _, edge_type_prediction_AB_AE = edge_type_prediction_AB_AE_K.max(-1) # shape: [num_simulation, num_node*(num_node-1)]

    ground_truth_relation = []
    
    for batch_idx, (data, acc, relations, pairwise_force, exp_id) in enumerate(data_loader):
        # relations shape: [batch_size, num_edges] 
        ground_truth_relation.append(relations)
        

    ## compute the cmpt_permutation_accuracy
    ground_truth_relation = torch.cat(ground_truth_relation, dim=0) # shape: [num_simulations, num_node*(num_node-1)]
    assert ground_truth_relation.shape == edge_type_prediction_AB_AE.shape
    
    ML_relation_preds = edge_type_prediction_AB_AE.reshape(-1).cpu().detach().numpy()
    ground_truth_relation = ground_truth_relation.reshape(-1).cpu().detach().numpy()
    acc_test, map_pred2true   = cmpt_permutation_accuracy(ML_relation_preds, ground_truth_relation)
    assert len(map_pred2true) == args.edge_types
    map_true2pred = [-1] * args.edge_types
    for i in range(len(map_pred2true) ):
        true_label_id = map_pred2true[i]
        map_true2pred[true_label_id] = i
    assert -1 not in map_true2pred

    return acc_test, map_pred2true, map_true2pred






def cmpt_ef_MAE(decoder, tau, map_true2pred, data_loader, mode):
    ## E-step
    assert args.cmpt_posterior == "mean-field-block"
    
    Prob_G_AB_S_SR, edge_type_prediction_AB_AE_K = cmpt_categorical_distribution_variational_block(decoder, tau, data_loader, args.MF_iterations, mode)

    edge_type_prediction_AB_AE_K = torch.from_numpy(edge_type_prediction_AB_AE_K)
    if args.cuda:
        edge_type_prediction_AB_AE_K = edge_type_prediction_AB_AE_K.cuda()
    _, ML_relation_preds = edge_type_prediction_AB_AE_K.max(-1) # shape: [num_simulation, num_node*(num_node-1)]


    ground_truth_relation = []

    count_node_time = 0.0
    count_edge_time = 0.0

    MAE_ef = 0.0
    MAE_symm = 0.0
    count_pairwise_force_magnitude = 0.0

    decoder.eval()
    for batch_idx, (data, acc, relations, pairwise_force, exp_id) in enumerate(data_loader):
        # pairwise_force shape: [batch_size, num_edges, time_steps, space_dim]
        # data shape: [batch_size, num_nodes, time_steps, dims]
        # acc shape: [batch_size, num_nodes, time_steps, space_dim]
        # relations shape: [batch_size, num_edges]

        this_batch_size = len(exp_id)
        ## mapping label
        assert len(relations.shape) == 2
        for i in range(relations.shape[0]):
            for j in range(relations.shape[1]):
                relations[i, j] = map_true2pred[relations[i, j] ]

        if args.cuda:
            data, acc, relations = data.cuda(), acc.cuda(), relations.cuda()
            pairwise_force = pairwise_force.cuda()
        exp_id = exp_id.cpu().detach().numpy()
        
        with torch.no_grad():
            predicted_pairwise_force_B_E_K_T_D = decoder.cmpt_all_type_forces_all_edges(data, rel_rec, rel_send) # [batch_size, all_edges, #edge_types, time_steps, space_dim]

        ## pairwise force
        truth_relations_B_AE_K = torch.nn.functional.one_hot(relations, num_classes = args.edge_types).double()
        predicted_pairwise_force_B_E_T_D = (predicted_pairwise_force_B_E_K_T_D * truth_relations_B_AE_K[:, :, :, None, None]).sum(dim = 2)
        assert predicted_pairwise_force_B_E_T_D.shape == pairwise_force.shape, "pairwise_force shape: {}".format(pairwise_force.shape)
        diff_pairwise_force = torch.sum(torch.abs(predicted_pairwise_force_B_E_T_D * np.sqrt(acc_var) - pairwise_force ))
        MAE_ef += diff_pairwise_force.item()
        count_pairwise_force_magnitude += torch.abs(pairwise_force).sum().item()


        ## symmetry
        with torch.no_grad():
            predicted_pairwise_force_inverse_B_E_K_T_D = decoder.cmpt_all_type_forces_all_edges(data, rel_send, rel_rec)
        predicted_pairwise_force_inverse_B_E_T_D = (predicted_pairwise_force_inverse_B_E_K_T_D * truth_relations_B_AE_K[:, :, :, None, None]).sum(dim = 2)
        diff_symmetry = torch.sum(torch.abs(predicted_pairwise_force_B_E_T_D + predicted_pairwise_force_inverse_B_E_T_D) ).item() * np.sqrt(acc_var)
        MAE_symm += diff_symmetry


        ground_truth_relation.append(relations)
        count_node_time += this_batch_size * args.num_atoms * args.used_time_steps
        count_edge_time += this_batch_size * relations.shape[1] * args.used_time_steps

    ## compute the relation accuracy
    ground_truth_relation = torch.cat(ground_truth_relation, dim=0) # shape: [num_simulations, num_node*(num_node-1)]
    assert ground_truth_relation.shape == ML_relation_preds.shape
    equal_number = torch.eq(ML_relation_preds, ground_truth_relation).cpu().sum() # the predicted edge label corresponds to the ground-truth edge label
    counter = ML_relation_preds.shape[0] * ML_relation_preds.shape[1]
    relation_accuracy = equal_number/float(counter)

    ## MAE pairwise force
    # MAE_ef = MAE_ef / count_pairwise_force_magnitude
    MAE_ef = MAE_ef / count_edge_time
    MAE_symm = MAE_symm / count_edge_time

    return MAE_ef, MAE_symm, relation_accuracy




def extract_next_time_pos(decoder, data, Prob_G_AB_S_SR, exp_id):
    this_batch_size = len(exp_id)
    decoder.eval()
    with torch.no_grad():
        predicted_pairwise_force_B_E_K_T_D = decoder.cmpt_all_type_forces_all_edges(data, rel_rec, rel_send) # [batch_size, all_edges, #edge_types, time_steps, space_dim]


    ## acceleration
    # pairwise force with current edge NN
    edgeForces_B_S_IE_K_T_D = torch.matmul(nodeIncomingEdges_S_IE_AE, predicted_pairwise_force_B_E_K_T_D.transpose(1, 3).unsqueeze(3) ) # shape: [batch_size, #time_steps, K, #subgraphs, #incoming_edges, dimension]
    # print("edgeForces_B_S_IE_K_T_D shape: ", edgeForces_B_S_IE_K_T_D.shape); exit() # [8, 100, 2, 5, 4, 2] [batch_size, time_steps, K, #subgraphs, num_incoming_edges, dim]

    edgeForces_B_S_IE_K_T_D = edgeForces_B_S_IE_K_T_D.permute(0, 3, 4, 2, 1, 5)
    edgeForces_B_S_IE_K_T_D = edgeForces_B_S_IE_K_T_D.contiguous()
    # edgeForces_B_S_IE_K_T_D = edgeForces_B_S_IE_K_T_D.cpu().detach().numpy()
    # print("edgeForces_B_S_IE_K_T_D shape: ", edgeForces_B_S_IE_K_T_D.shape) # shape:  [batch_size, #subgraphs, #incoming_edges, K, #time_steps, dimension]
    particle_masses = data[:, :, :, -1][:, :, :, None]
    edgeForces_B_S_IE_K_T_D = edgeForces_B_S_IE_K_T_D / data[:, :, None, None, :, [-1]] # divide the mass !!! force -> acceleration

    time_steps = edgeForces_B_S_IE_K_T_D.shape[-2]
    # assert time_steps == args.used_time_steps
    predictAcceleration_B_S_T_D = torch.zeros((this_batch_size, args.num_atoms, time_steps, args.space_dim) ).cuda()

    for itr_1 in range(num_groups):
        f_itr1_B_S_SR_IE_K_T_D = edgeForces_B_S_IE_K_T_D[:, :, None, :, :, :, :] * group_realization_edge_G_SR_IE_K[itr_1][None, None, :, :, :, None, None]
        f_itr1_B_S_SR_T_D = torch.sum(f_itr1_B_S_SR_IE_K_T_D, dim = (3, 4), keepdim = False)
        this_term_B_S_T_D = torch.sum(f_itr1_B_S_SR_T_D * Prob_G_AB_S_SR[itr_1][exp_id, :, :, None, None], axis = 2, keepdims=False)
        predictAcceleration_B_S_T_D += this_term_B_S_T_D

    predictAcceleration_B_S_T_D = predictAcceleration_B_S_T_D * np.sqrt(acc_var) ## denormalize the predicted acceleration !!!

    loc = data[:, :, :, 0:2].contiguous()
    vel = data[:, :, :, 2:4].contiguous()
    loc, vel = decoder.unnormalize(loc, vel)

    loc = loc + decoder.delta_t * vel
    vel = vel + decoder.delta_t * predictAcceleration_B_S_T_D
        
    loc, vel = decoder.renormalize(loc, vel)

    pred = torch.cat([loc, vel, particle_masses], axis=3)

    return pred





def MAE_position_vel(decoder, tau, data_loader, mode, N=1):
    
    MAE_pos_vel = 0.0
    count_node_time = 0.0

    assert args.cmpt_posterior == "mean-field-block"
    Prob_G_AB_S_SR, edge_type_prediction_AB_AE_K = cmpt_categorical_distribution_variational_block(decoder, tau, data_loader, args.MF_iterations, mode)

    
    decoder.eval()
    for batch_idx, (data, acc, relations, pairwise_force, exp_id) in enumerate(data_loader):
        # print("data shape: ", data.shape) # [8, 10, 100, 5]
        # pairwise_force shape: [batch_size, num_edges, time_steps, space_dim]
        # data shape: [batch_size, num_nodes, time_steps, dims]
        # acc shape: [batch_size, num_nodes, time_steps, space_dim]
        # relations shape: [batch_size, num_edges]
        this_batch_size = len(exp_id)
        ## mapping label
        assert len(relations.shape) == 2

        if args.cuda:
            data, exp_id = data.cuda(), exp_id.cuda()   
        exp_id = exp_id.cpu().detach().numpy()

        
        data_decoder = data[:, :, -args.used_time_steps:-N, :].contiguous()

        output = data_decoder
        for i in range(N):
            output = extract_next_time_pos(decoder, output, Prob_G_AB_S_SR, exp_id)

        predict_loc, predict_vel = decoder.unnormalize(output[:, :, :, 0:2], output[:, :, :, 2:4])
        output = torch.cat([predict_loc, predict_vel], axis=-1)

        target = data[:, :, -args.used_time_steps+N:, 0:4].contiguous()
        true_loc, true_vel = decoder.unnormalize(target[:, :, :, 0:2], target[:, :, :, 2:4])
        target = torch.cat([true_loc, true_vel], axis=-1)

        assert output.shape == target.shape

        # make sure it is the un-normalized position
        diff_pos_vel = torch.sum(torch.abs(output - target ) )
        MAE_pos_vel += diff_pos_vel.item()
        count_node_time += output.shape[0] * output.shape[1] * output.shape[2] 

    MAE_pos_vel = MAE_pos_vel / count_node_time
    print('after {} time steps, position MAE: {:.8f}'.format(N, MAE_pos_vel ) ) 
    return MAE_pos_vel
        
        




# def test(decoder, tau):
#     t = time.time()
    
#     relation_acc_valid, map_pred2true, map_true2pred = cmpt_relation_accuracy(decoder, tau, valid_loader, "valid")

#     print("relation_acc_valid: ", relation_acc_valid)
#     print("map_pred2true: ", map_pred2true)
#     print("map_true2pred: ", map_true2pred)


#     MAE_acceleration_valid, MAE_ef_valid, MAE_symm_valid, relation_accuracy_valid = cmpt_loss(decoder, tau, map_true2pred, valid_loader, "valid")
#     print("MAE_acceleration_valid: ", MAE_acceleration_valid)
#     print("MAE_ef_valid: ", MAE_ef_valid)
#     print("MAE_symm_valid: ", MAE_symm_valid)
#     print("relation_accuracy_valid: ", relation_accuracy_valid)

#     return MAE_acceleration_valid, MAE_ef_valid, MAE_symm_valid, relation_accuracy_valid







if __name__ == "__main__":

    # trained_model_path = "./trained_VarEM_chargeDiffMass_N5/" 
    # trained_model_path = "./trained_VarEM_spring_N5_K2/" 
    # trained_model_path = "./trained_VarEM_spring_N10_K2/"
    # trained_model_path = "./trained_VarEM_spring_N5_K4/"

    trained_model_path = args.trained_model_path

    for num_simulation in tqdm([100, 500, 1000, 5000, 10000]):
    # for num_simulation in [100]:
        trained_model_path_2 = trained_model_path + "{}simulations_100timesteps/".format(num_simulation)
        MAE_ef_list = []
        MAE_symm_list = []
        relation_accuracy_list = []

        MAE_pos_vel_1_list = []
        MAE_pos_vel_10_list = []

        print("\n")
        print(trained_model_path_2)
        for exp in [1, 2, 3, 4, 5]:
        # for exp in [1]:
            print("num_simulation: ", num_simulation, ", exp: ", exp)
            load_path = trained_model_path_2 + "exp{}/".format(exp)
            print(load_path)

            ## load data
            train_loader, valid_loader, test_loader, loc_max, loc_min, vel_max, vel_min = load_test_data(
            args.input_dir, load_path, args.batch_size, args.suffix, args.used_time_steps, args.train_examples, args.valid_examples, args.test_examples, all_train_exp = args.all_train_exp, all_valid_exp = args.all_valid_exp, all_test_exp = args.all_test_exp)

            assert os.path.isfile(load_path + "min_max_dict.pkl")
            with open(load_path + "min_max_dict.pkl", "rb") as f:
                min_max_dict = pkl.load(f)
            assert loc_max == min_max_dict["loc_max"]
            assert loc_min == min_max_dict["loc_min"]
            assert vel_max == min_max_dict["vel_max"]
            assert vel_min == min_max_dict["vel_min"]
            acc_var = min_max_dict["acc_var"]


            ### Define model 
            decoder = MLP_PIGNPI_Decoder(n_in_node=args.dims,
                                 edge_types=args.edge_types,
                                 msg_hid=args.decoder_hidden,
                                 msg_out=args.decoder_hidden,
                                 n_hid=args.decoder_hidden,
                                 delta_t=args.delta_T, # added
                                 loc_max=loc_max, # added
                                 loc_min=loc_min, # added
                                 vel_max=vel_max, # added
                                 vel_min=vel_min, # added
                                 do_prob=args.decoder_dropout,
                                 skip_first=args.skip_first,
                                 data_type =data_type)
            
            model_state_dict = decoder.state_dict().keys()
            decoder_file_name = load_path + "best_decoder.pt"
            loaded_state_dict = torch.load(decoder_file_name).keys()
            assert model_state_dict == loaded_state_dict           

            decoder.cuda()


            
            ## load the trained model and the tau
            decoder_file_name = load_path + "best_decoder.pt"
            decoder.load_state_dict(torch.load(decoder_file_name))

            tau_file_name = load_path + "best_decoder_tau.pkl"
            with open(tau_file_name, "rb") as f:
                tau = pickle.load(f)
            tau = torch.from_numpy(tau).cuda()
            print(tau)


            ## testing

            relation_accuracy, map_pred2true, map_true2pred = cmpt_relation_accuracy(decoder, tau, test_loader, "testing")
            relation_accuracy_list.append(relation_accuracy)
            print("relation_accuracy: ", relation_accuracy)
            print("map_pred2true: ", map_pred2true)
            print("map_true2pred: ", map_true2pred)


            MAE_ef_test, MAE_symm_test, relation_accuracy_test = cmpt_ef_MAE(decoder, tau, map_true2pred, test_loader, "testing")
            MAE_ef_list.append(MAE_ef_test)
            MAE_symm_list.append(MAE_symm_test)
            print("MAE_ef_test: ", MAE_ef_test)
            print("MAE_symm_test: ", MAE_symm_test)
            print("relation_accuracy_test: ", relation_accuracy_test)

            
            MAE_pos_vel_1 = MAE_position_vel(decoder, tau, test_loader, "testing", N=1)
            MAE_pos_vel_1_list.append(MAE_pos_vel_1)

            MAE_pos_vel_10 = MAE_position_vel(decoder, tau, test_loader, "testing", N=10)
            MAE_pos_vel_10_list.append(MAE_pos_vel_10)

        
        res = {
            "MAE_ef_list": MAE_ef_list,
            "MAE_symm_list": MAE_symm_list,
            "relation_accuracy_list": relation_accuracy_list, 
            "MAE_pos_vel_1_list": MAE_pos_vel_1_list, 
            "MAE_pos_vel_10_list": MAE_pos_vel_10_list
        }
        

        if args.test_generalization == "FALSE":
            with open(trained_model_path_2+"evaluation_results.pkl", "wb") as f:
                pkl.dump(res, f)
        else:
            with open(trained_model_path_2+"generalization_results.pkl", "wb") as f:
                pkl.dump(res, f)

        print("num_simulation: ", num_simulation, " , MAE_ef mean: ", np.mean(MAE_ef_list) )
        print("num_simulation: ", num_simulation, " , MAE_ef std: ", np.std(MAE_ef_list) )
        print("num_simulation: ", num_simulation, " , MAE_symm mean: ", np.mean(MAE_symm_list) )
        print("num_simulation: ", num_simulation, " , MAE_symm std: ", np.std(MAE_symm_list) )
        print("num_simulation: ", num_simulation, " , relation_accuracy mean: ", np.mean(relation_accuracy_list) )
        print("num_simulation: ", num_simulation, " , relation_accuracy std: ", np.std(relation_accuracy_list) )

        print("num_simulation: ", num_simulation, " , pos_vel_N1 mean: ", np.mean(MAE_pos_vel_1_list) )
        print("num_simulation: ", num_simulation, " , pos_vel_N1 std: ", np.std(MAE_pos_vel_1_list) )

        print("num_simulation: ", num_simulation, " , pos_vel_N10 mean: ", np.mean(MAE_pos_vel_10_list) )
        print("num_simulation: ", num_simulation, " , pos_vel_N10 std: ", np.std(MAE_pos_vel_10_list) )







# trained_VarEM_spring_N5_K2
# CUDA_VISIBLE_DEVICES=1 python --trained_model_path trained_VarEM_spring_N5_K2/ test_VarEM.py --input-dir ../dataset/sim_springs/K_2/particle_5/30000_train_5000_valid_5000_test_BalancedTypes_2/ --num-atoms 5 --suffix "_springs5" --edge_types 2 --used-time-steps 100 --train-examples 100 --valid-examples 1000 --test-examples 1000 --batch-size 8 --var 0.1 --all_train_exp 30000 --all_valid_exp 5000 --all_test_exp 5000 --test_generalization FALSE > trained_VarEM_spring_N5_K2/evaluation.txt


# trained_VarEM_spring_N5_K2, generalization
# CUDA_VISIBLE_DEVICES=1 python --trained_model_path trained_VarEM_spring_N5_K2/ test_VarEM.py --input-dir ../dataset/sim_springs/K_2/particle_10/50000_train_10000_valid_10000_test_BalancedTypes/ --num-atoms 10 --suffix "_springs10" --edge_types 2 --used-time-steps 100 --train-examples 100 --valid-examples 1000 --test-examples 1000 --batch-size 8 --var 0.1 --all_train_exp 50000 --all_valid_exp 10000 --all_test_exp 10000 --test_generalization TRUE > trained_VarEM_spring_N5_K2/evaluation_generalization.txt


# trained_VarEM_spring_N10_K2
# CUDA_VISIBLE_DEVICES=1 python --trained_model_path trained_VarEM_spring_N10_K2/ test_VarEM.py --input-dir ../dataset/sim_springs/K_2/particle_10/50000_train_10000_valid_10000_test_BalancedTypes/ --num-atoms 10 --suffix "_springs10" --edge_types 2 --used-time-steps 100 --train-examples 100 --valid-examples 1000 --test-examples 1000 --batch-size 8 --var 0.1 --all_train_exp 50000 --all_valid_exp 10000 --all_test_exp 10000 --test_generalization FALSE > trained_VarEM_spring_N10_K2/evaluation.txt


# trained_VarEM_spring_N5_K4
# CUDA_VISIBLE_DEVICES=1 python --trained_model_path trained_VarEM_spring_N5_K4/ test_VarEM.py --input-dir ../dataset/sim_springs/K_4/particle_5/30000_train_5000_valid_5000_test_BalancedTypes/ --num-atoms 5 --suffix "_springs5" --edge_types 4 --used-time-steps 100 --train-examples 100 --valid-examples 1000 --test-examples 1000 --batch-size 8 --var 0.1 --all_train_exp 30000 --all_valid_exp 5000 --all_test_exp 5000 --test_generalization FALSE > trained_VarEM_spring_N5_K4/evaluation.txt


# trained_VarEM_chargeDiffMass_N5
# CUDA_VISIBLE_DEVICES=1 python --trained_model_path trained_VarEM_chargeDiffMass_N5/ test_VarEM.py --input-dir ../dataset/sim_charge/K_2/particle_5/10000_train_2000_valid_2000_test_diffMass/ --num-atoms 5 --suffix "_charged5" --edge_types 2 --used-time-steps 100 --train-examples 100 --valid-examples 1000 --test-examples 1000 --batch-size 8 --var 0.05 --all_train_exp 10000 --all_valid_exp 2000 --all_test_exp 2000 --test_generalization FALSE > trained_VarEM_chargeDiffMass_N5/evaluation.txt
