import torch
import torch.nn as nn
import torch.nn.functional as F
import math
import numpy as np

torch.set_default_dtype(torch.float64)


class SiLU(nn.Module):    
    # from https://pytorch.org/docs/stable/_modules/torch/nn/modules/activation.html#SiLU, since it is missing in 1.6.0
    def __init__(self, inplace = False):
        super(SiLU, self).__init__()
        self.inplace = inplace

    def forward(self, x):
        return x.mul_(x.sigmoid() ) if self.inplace else x.mul(x.sigmoid() )

    def extra_repr(self):
        inplace_str = 'inplace=True' if self.inplace else ''
        return inplace_str


class MLP_PIGNPI_Decoder(nn.Module):
    """PIGNPI decoder module."""

    def __init__(self, n_in_node, edge_types, msg_hid, msg_out, n_hid, delta_t, loc_max, loc_min, vel_max, vel_min,
                 do_prob=0., skip_first=False, data_type=None):
        super(MLP_PIGNPI_Decoder, self).__init__()

        self.data_type = data_type
        assert data_type in ["spring", "charge"]
        

        self.msg_fc1_weight = torch.nn.parameter.Parameter(torch.randn(edge_types, 2 * n_in_node, msg_hid), requires_grad=True)
        self.msg_fc1_bias = torch.nn.parameter.Parameter(torch.randn(msg_hid), requires_grad=True)
        self.msg_fc2_weight = torch.nn.parameter.Parameter(torch.randn(edge_types, msg_hid, msg_hid), requires_grad=True)
        self.msg_fc2_bias = torch.nn.parameter.Parameter(torch.randn(msg_hid), requires_grad=True)
        self.msg_fc3_weight = torch.nn.parameter.Parameter(torch.randn(edge_types, msg_hid, 2), requires_grad=True)
        self.msg_fc3_bias = torch.nn.parameter.Parameter(torch.rand(2), requires_grad=True)

        # # ## additional layers for charge
        if self.data_type == "charge":
            self.msg_fc11_weight = torch.nn.parameter.Parameter(torch.randn(edge_types, msg_hid, msg_hid), requires_grad=True)
            self.msg_fc11_bias = torch.nn.parameter.Parameter(torch.randn(msg_hid), requires_grad=True)
            self.msg_fc12_weight = torch.nn.parameter.Parameter(torch.randn(edge_types, msg_hid, msg_hid), requires_grad=True)
            self.msg_fc12_bias = torch.nn.parameter.Parameter(torch.randn(msg_hid), requires_grad=True)

        self.activation = SiLU() # F.silu()
        

        self.initialize_weights()

        # self.msg_out_shape = msg_out
        self.msg_out_shape = 2 # 2D case
        self.skip_first_edge_type = skip_first
        self.edge_types = edge_types

        print('Using learned interaction net decoder.')

        self.dropout_prob = do_prob
        self.delta_t = delta_t

        self.loc_max = loc_max
        self.loc_min = loc_min
        self.vel_max = vel_max
        self.vel_min = vel_min

    def initialize_weights(self):
        bound = 1.0 / math.sqrt(self.msg_fc1_weight.shape[-2])
        torch.nn.init.uniform_(self.msg_fc1_weight, -bound, bound)
        torch.nn.init.uniform_(self.msg_fc1_bias, -bound, bound)

        bound = 1.0 / math.sqrt(self.msg_fc2_weight.shape[-2])
        torch.nn.init.uniform_(self.msg_fc2_weight, -bound, bound)
        torch.nn.init.uniform_(self.msg_fc2_bias, -bound, bound)

        bound = 1.0 / math.sqrt(self.msg_fc3_weight.shape[-2])
        torch.nn.init.uniform_(self.msg_fc3_weight, -bound, bound)
        torch.nn.init.uniform_(self.msg_fc3_bias, -bound, bound)

        if self.data_type == "charge":
            ## additional layers
            bound = 1.0 / math.sqrt(self.msg_fc11_weight.shape[-2])
            torch.nn.init.uniform_(self.msg_fc11_weight, -bound, bound)
            torch.nn.init.uniform_(self.msg_fc11_bias, -bound, bound)

            bound = 1.0 / math.sqrt(self.msg_fc12_weight.shape[-2])
            torch.nn.init.uniform_(self.msg_fc12_weight, -bound, bound)
            torch.nn.init.uniform_(self.msg_fc12_bias, -bound, bound)


    def unnormalize(self, loc, vel):
        loc = 0.5 * (loc + 1) * (self.loc_max - self.loc_min) + self.loc_min
        vel = 0.5 * (vel + 1) * (self.vel_max - self.vel_min) + self.vel_min
        return loc, vel

    def renormalize(self, loc, vel):
        loc = 2 * (loc - self.loc_min) / (self.loc_max - self.loc_min) - 1
        vel = 2 * (vel - self.vel_min) / (self.vel_max - self.vel_min) - 1
        return loc, vel




    def cmpt_net_force(self, inputs, rel_type, rel_rec, rel_send):        
        # compute the predicted net forces for every node
        # inputs shape: [num_sims, num_atoms, num_timesteps, num_dims]
        # rel_type shape: [#all_realizations, #all_edges, #edge_types]
        # rel_rec shape: [num_edges, num_particles]
        # rel_send shape: [num_edges, num_particles]
        inputs = inputs.transpose(1, 2).contiguous() # input size: [num_sims, num_timesteps, num_atoms, num_dims]
        # sizes = [inputs.size(0), inputs.size(1), rel_type.size(0), rel_type.size(1), rel_type.size(2)] # [batch_size, num_timesteps, #all_realizations, num_edges, args.edge_types]

        # rel_type = rel_type.repeat(inputs.size(0), inputs.size(1), 1, 1, 1)
        # rel_type shape: [batch_size, #time_steps, #realizations, #all_edges, #edge_types] 
        # print("rel_type shape: {}".format(rel_type.shape)); exit()

        # batch_size, time_steps, num_realizations, num_all_edges, num_edge_type = rel_type.shape
        num_realizations, num_all_edges, num_edge_type = rel_type.shape

        receiver_input_state = torch.matmul(rel_rec, inputs)
        sender_input_state = torch.matmul(rel_send, inputs)
        pre_msg = torch.cat([sender_input_state, receiver_input_state], dim=-1)
        # print(pre_msg.shape); exit()
        # pre_msg shape: [batch_size, time_steps, all_edges, 2*num_features]

        pre_msg = pre_msg.unsqueeze(dim = 2)
        # print("pre_msg shape: ")
        # print(pre_msg.shape)

        pre_msg = self.activation(torch.matmul(pre_msg, self.msg_fc1_weight) + self.msg_fc1_bias)
        # print("pre_msg shape")
        # print(pre_msg.shape)

        pre_msg = F.dropout(pre_msg, p=self.dropout_prob)
        pre_msg = self.activation(torch.matmul(pre_msg, self.msg_fc2_weight) + self.msg_fc2_bias)

        if self.data_type == "charge":
            ## additional layers for charge
            pre_msg = self.activation(torch.matmul(pre_msg, self.msg_fc11_weight) + self.msg_fc11_bias)
            pre_msg = self.activation(torch.matmul(pre_msg, self.msg_fc12_weight) + self.msg_fc12_bias)


        pre_msg = F.dropout(pre_msg, p=self.dropout_prob)
        pre_msg = torch.matmul(pre_msg, self.msg_fc3_weight) + self.msg_fc3_bias

        # print("pre_msg shape")
        # print(pre_msg.shape) # [batch_size, time_steps, #edge_types, all_edges, space_dim]

        msg = pre_msg.transpose(2, 3) 
        # print("msg shape")
        # print(msg.shape)
        # msg shape: [batch_size, time_steps, all_edges, #edge_types, space_dim]

        # print("rel_type shape")
        # print(rel_type.shape) #[#all_realizations, #all_edges, #edge_types]
        rel_type = rel_type.unsqueeze(1)
        rel_type = rel_type.unsqueeze(1)
        rel_type = rel_type.unsqueeze(-1)
        # print("rel_type shape")
        # print(rel_type.shape)  # [#all_realizations, 1, 1, #all_edges, #edge_types, 1]

        all_msgs = msg * rel_type
        # print("all_msgs shape: ", all_msgs.shape) # [#all_realizations, batch_size, time_steps, all_edges, #edge_types, space_dim]
        all_msgs = torch.sum(all_msgs, dim=-2) # sum over the massage for every edge, over all possible interaction types
        # print("all_msgs shape: ", all_msgs.shape) # [#all_realizations, batch_size, time_steps, all_edges, space_dim]

        
        agg_msgs = all_msgs.transpose(-2, -1).matmul(rel_rec).transpose(-2, -1)
        agg_msgs = agg_msgs.contiguous()
        # print("agg_msgs shape: {}".format(agg_msgs.shape)); exit()
        # agg_msgs shape: [#realizations, batch_size, time_steps, num_atoms, space_dim]
        return agg_msgs


    def cmpt_all_type_forces_all_edges(self, inputs, rel_rec, rel_send):
        ## compute the pairwise force for every type of interaction for all edges in one simulation
        # inputs shape: [num_sims, num_atoms, num_timesteps, num_dims]
        # rel_rec shape: [num_edges, num_particles]
        # rel_send shape: [num_edges, num_particles]
        # return: [num_sims, num_atoms*(num_atoms-1), args.edge_types, num_timesteps, num_dims]
        inputs = inputs.transpose(1, 2).contiguous() # inputs shape: [num_sims, num_timesteps, num_atoms, num_dims]
        receiver_input_state = torch.matmul(rel_rec, inputs)
        sender_input_state = torch.matmul(rel_send, inputs)
        pre_msg = torch.cat([sender_input_state, receiver_input_state], dim=-1) # [batch_size, num_timesteps, num_edges, num_dims(input_feature) *2]

        pre_msg = pre_msg.unsqueeze(dim = 2)
        # print("pre_msg shape: ")
        # print(pre_msg.shape) #[batch_size, num_timesteps, 1, num_edges, num_dims(input_feature)*2]

        # msg_fc1_weight shape: torch.randn(edge_types, 2*n_in_node, msg_hid)
        pre_msg = self.activation(torch.matmul(pre_msg, self.msg_fc1_weight) + self.msg_fc1_bias)
        # print(pre_msg.shape); # [batch_size, num_timesteps, edge_types, num_edges, hidden_dim]

        pre_msg = F.dropout(pre_msg, p=self.dropout_prob)
        pre_msg = self.activation(torch.matmul(pre_msg, self.msg_fc2_weight) + self.msg_fc2_bias)


        if self.data_type == "charge":
            ## additional layers for charge
            pre_msg = self.activation(torch.matmul(pre_msg, self.msg_fc11_weight) + self.msg_fc11_bias)
            pre_msg = self.activation(torch.matmul(pre_msg, self.msg_fc12_weight) + self.msg_fc12_bias)

        

        pre_msg = F.dropout(pre_msg, p=self.dropout_prob)
        pre_msg = torch.matmul(pre_msg, self.msg_fc3_weight) + self.msg_fc3_bias

        # print("pre_msg shape")
        # print(pre_msg.shape) # [batch_size, time_steps, #edge_types, all_edges, space_dim]

        # msg = pre_msg.transpose(2, 3) 
        msg = pre_msg.permute(0, 3, 2, 1, 4)
        # print("msg shape")
        # print(msg.shape) # [batch_size, all_edges, #edge_types, time_steps, space_dim]

        return msg


