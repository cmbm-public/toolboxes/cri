import torch
import torch.nn as nn
import torch.nn.functional as F
import math
from torch.autograd import Variable
from torch.nn import Sequential as Seq, Linear

torch.set_default_dtype(torch.float64)


class SiLU(nn.Module):    
    # from https://pytorch.org/docs/stable/_modules/torch/nn/modules/activation.html#SiLU, since it is missing in 1.6.0
    def __init__(self, inplace = False):
        super(SiLU, self).__init__()
        self.inplace = inplace

    def forward(self, x):
        return x.mul_(x.sigmoid() ) if self.inplace else x.mul(x.sigmoid() )

    def extra_repr(self):
        inplace_str = 'inplace=True' if self.inplace else ''
        return inplace_str



class MLP_PIGNPI_Decoder(nn.Module):
    """PIGNPI decoder module."""

    def __init__(self, n_in_node, edge_types, msg_hid, msg_out, n_hid,
                 do_prob=0.):
        super(MLP_PIGNPI_Decoder, self).__init__()        

        self.msg_fc1_weight = torch.nn.parameter.Parameter(torch.randn(edge_types, 4, msg_hid), requires_grad=True) #2 * n_in_node
        self.msg_fc1_bias = torch.nn.parameter.Parameter(torch.randn(msg_hid), requires_grad=True)
        self.msg_fc2_weight = torch.nn.parameter.Parameter(torch.randn(edge_types, msg_hid, msg_hid), requires_grad=True)
        self.msg_fc2_bias = torch.nn.parameter.Parameter(torch.randn(msg_hid), requires_grad=True)
        self.msg_fc3_weight = torch.nn.parameter.Parameter(torch.randn(edge_types, msg_hid, msg_out), requires_grad=True)
        self.msg_fc3_bias = torch.nn.parameter.Parameter(torch.rand(msg_out), requires_grad=True)

        ## additional layers for LJ
        self.msg_fc11_weight = torch.nn.parameter.Parameter(torch.randn(edge_types, msg_hid, msg_hid), requires_grad=True)
        self.msg_fc11_bias = torch.nn.parameter.Parameter(torch.randn(msg_hid), requires_grad=True)
        self.msg_fc12_weight = torch.nn.parameter.Parameter(torch.randn(edge_types, msg_hid, msg_hid), requires_grad=True)
        self.msg_fc12_bias = torch.nn.parameter.Parameter(torch.randn(msg_hid), requires_grad=True)
        # self.msg_fc13_weight = torch.nn.parameter.Parameter(torch.randn(edge_types, msg_hid, msg_hid), requires_grad=True)
        # self.msg_fc13_bias = torch.nn.parameter.Parameter(torch.randn(msg_hid), requires_grad=True)
         
        
        self.activation = SiLU() # F.silu()
        
        self.initialize_weights()


        self.output_dim = msg_out
        self.edge_types = edge_types


        print('Using learned interaction net decoder.')

        self.dropout_prob = do_prob


    def initialize_weights(self):
        bound = 1.0 / math.sqrt(self.msg_fc1_weight.shape[-2])
        torch.nn.init.uniform_(self.msg_fc1_weight, -bound, bound)
        torch.nn.init.uniform_(self.msg_fc1_bias, -bound, bound)

        bound = 1.0 / math.sqrt(self.msg_fc2_weight.shape[-2])
        torch.nn.init.uniform_(self.msg_fc2_weight, -bound, bound)
        torch.nn.init.uniform_(self.msg_fc2_bias, -bound, bound)

        bound = 1.0 / math.sqrt(self.msg_fc3_weight.shape[-2])
        torch.nn.init.uniform_(self.msg_fc3_weight, -bound, bound)
        torch.nn.init.uniform_(self.msg_fc3_bias, -bound, bound)


        ## additional layers
        bound = 1.0 / math.sqrt(self.msg_fc11_weight.shape[-2])
        torch.nn.init.uniform_(self.msg_fc11_weight, -bound, bound)
        torch.nn.init.uniform_(self.msg_fc11_bias, -bound, bound)

        bound = 1.0 / math.sqrt(self.msg_fc12_weight.shape[-2])
        torch.nn.init.uniform_(self.msg_fc12_weight, -bound, bound)
        torch.nn.init.uniform_(self.msg_fc12_bias, -bound, bound)

        # bound = 1.0 / math.sqrt(self.msg_fc13_weight.shape[-2])
        # torch.nn.init.uniform_(self.msg_fc13_weight, -bound, bound)
        # torch.nn.init.uniform_(self.msg_fc13_bias, -bound, bound)


    ###
    def cmpt_position_distance(self, sender_pos, receiver_pos):
        diff = sender_pos - receiver_pos
        norm = torch.norm(diff, p=2, dim=-1, keepdim=True)
        return norm
    ### 
    def cmpt_position_direction(self, sender_pos, receiver_pos):
        diff = sender_pos - receiver_pos
        norm = torch.norm(diff, p=2, dim=-1, keepdim=True)
        return diff / norm




    ###
    def cmpt_all_type_forces_all_edges(self, inputs, rel_rec, rel_send):
        ## compute the pairwise force for every type of interaction for all edges in one simulation
        # inputs shape: [num_timesteps, num_atoms, num_dims]
        # rel_rec shape: [num_timesteps, num_edges, num_particles (all particles)]
        # rel_send shape: [num_timesteps, num_edges, num_particles]
        # return: [num_timesteps, #AE, args.edge_types, num_dims]
        # inputs shape: [num_timesteps, num_atoms, num_dims]

        receiver_input_state = torch.matmul(rel_rec, inputs[:, :, 0:2]) # shape: ([num_timesteps, num_E, num_dims]
        sender_input_state = torch.matmul(rel_send, inputs[:, :, 0:2])# shape: ([num_timesteps, num_E, num_dims]

        # receiver_input_state = receiver_input_state.cuda()
        # sender_input_state = sender_input_state.cuda()
        
                
        pre_msg = torch.cat([sender_input_state, receiver_input_state], dim=-1) 
        # print("pre_msg shape: ", pre_msg.shape) # [num_timesteps, #AE, 2D]
        # print("pre_msg shape: ", pre_msg.shape); exit()

        pre_msg = pre_msg.unsqueeze(dim = 1)
        # print("pre_msg shape: ")
        # print(pre_msg.shape) #[num_timesteps, 1, num_edges, num_dims(input_feature)*2]

        # msg_fc1_weight shape: torch.randn(edge_types, 2*n_in_node, msg_hid)
        pre_msg = self.activation(torch.matmul(pre_msg, self.msg_fc1_weight) + self.msg_fc1_bias)
        # print(pre_msg.shape); # [num_timesteps, edge_types, num_edges, msg_hid]

        pre_msg = F.dropout(pre_msg, p=self.dropout_prob)
        pre_msg = self.activation(torch.matmul(pre_msg, self.msg_fc2_weight) + self.msg_fc2_bias)

        ## additional layers
        pre_msg = self.activation(torch.matmul(pre_msg, self.msg_fc11_weight) + self.msg_fc11_bias)
        pre_msg = self.activation(torch.matmul(pre_msg, self.msg_fc12_weight) + self.msg_fc12_bias)
        # pre_msg = self.activation(torch.matmul(pre_msg, self.msg_fc13_weight) + self.msg_fc13_bias)

        pre_msg = F.dropout(pre_msg, p=self.dropout_prob)
        pre_msg = torch.matmul(pre_msg, self.msg_fc3_weight) + self.msg_fc3_bias

        # print("pre_msg shape")
        # print(pre_msg.shape) # [time_steps, #edge_types, all_edges, space_dim]

        msg = pre_msg.transpose(1, 2) 
        # print("msg shape: ", msg.shape) # [time_steps, all_edges, #edge_types, space_dim]
        
        return msg


    ###
    def cmpt_all_type_forces_all_edges_with_ground_truth(self, inputs, rel_rec, rel_send):
        ## compute the pairwise force for every type of interaction for all edges in one simulation
        # inputs shape: [num_timesteps, num_atoms, num_dims]
        # rel_rec shape: [num_timesteps, num_edges, num_particles (all particles)]
        # rel_send shape: [num_timesteps, num_edges, num_particles]
        # return: [num_timesteps, #AE, args.edge_types, num_dims]
        # inputs shape: [num_timesteps, num_atoms, num_dims]

        receiver_input_state = torch.matmul(rel_rec, inputs[:, :, 0:2]) # shape: ([num_timesteps, num_E, num_dims]
        sender_input_state = torch.matmul(rel_send, inputs[:, :, 0:2])# shape: ([num_timesteps, num_E, num_dims]


        
        dr_rec_to_sender_T_AE_D = sender_input_state - receiver_input_state
        dist_T_AE_1 = self.cmpt_position_distance(receiver_input_state, sender_input_state)

        norm_vector_T_AE_D = dr_rec_to_sender_T_AE_D / dist_T_AE_1
        norm_vector_T_AE_1_D = norm_vector_T_AE_D.unsqueeze(2)
        # print("norm_vector_T_AE_1_D shape: ", norm_vector_T_AE_1_D.shape) # torch.Size([128, 300, 1, 2])
        # exit()


        type0_force_magnitude_T_AE_1 = 3. / torch.pow(dist_T_AE_1, 2) - 1. / torch.pow(dist_T_AE_1, 3)
        type1_force_magnitude_T_AE_1 = - 3. / torch.pow(dist_T_AE_1, 2) - 1. / torch.pow(dist_T_AE_1, 3)
        force_magnitude_T_AE_K_1 = torch.stack([type0_force_magnitude_T_AE_1, type1_force_magnitude_T_AE_1], dim = 2)
        
        
        pairwise_force_T_AE_K_D = norm_vector_T_AE_1_D * force_magnitude_T_AE_K_1
        # print("pairwise_force_T_AE_K_D shape: ", pairwise_force_T_AE_K_D.shape) # torch.Size([128, 300, 2, 2])

        return pairwise_force_T_AE_K_D

    


