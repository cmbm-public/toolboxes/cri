### Version two: at every time step, update the estimated interaction type for every edge
### 
### 
### 
from __future__ import division
from __future__ import print_function

import time
import argparse
import pickle
import os
import datetime
from tqdm import tqdm
# from multiprocessing import Pool
from pathlib import Path
from sklearn import metrics
import scipy
import numpy as np
import pickle as pkl

import warnings
warnings.filterwarnings("error")


import torch 
torch.set_num_threads(4)
torch.set_default_dtype(torch.float64)

torch.backends.cuda.matmul.allow_tf32 = False
torch.backends.cudnn.allow_tf32 = False


import torch.optim as optim
from torch.optim import lr_scheduler
from torch.optim.lr_scheduler import OneCycleLR


from utils_LJ import load_LJ_data_test
from modules import MLP_PIGNPI_Decoder


parser = argparse.ArgumentParser()
## particle system configuration
parser.add_argument('--num_all_particles', type=int, default = 100, 
                    help='Number of all particles in the simulation.')
parser.add_argument('--num_subgraphs', type=int, default = 100, 
                    help='Number of chosen particles.')
parser.add_argument('--edge_types', type=int, default=2, help='The number of edge types to infer.')
parser.add_argument('--num_IE', type = int, default=5, 
                    help='The number of in-coming edges for every subgraph. Determined by the simulation.') # start the simple case with 5
parser.add_argument('--space_dim', type=int, default=2, help='The space dimension.')
parser.add_argument('--input_dims', type=int, default=4, help='The number of input dimensions (position + velocity).')

## training configuration
parser.add_argument('--input_dir', type=str, default='../dataset/crystallization/', help='The directory containing input data.')
parser.add_argument('--no-cuda', action='store_true', default=False,
                    help='Disables CUDA training.')
parser.add_argument('--seed', type=int, default=42, help='Random seed.')
parser.add_argument('--epochs', type=int, default=10,
                    help='Number of epochs to train.')
parser.add_argument('--batch-size', type=int, default=128,
                    help='Number of samples per batch.')
parser.add_argument('--lr', type=float, default=1e-3,
                    help='Initial learning rate.')
parser.add_argument('--decoder_hidden', type=int, default=300,
                    help='Number of hidden units.')
parser.add_argument('--decoder', type=str, default='pignpi',
                    help='Type of decoder model (pignpi, rnn, or sim).')
parser.add_argument('--decoder-dropout', type=float, default=0.0,
                    help='Dropout rate (1 - keep probability).')
parser.add_argument('--var', type=float, default=0.001, # 0.001 for LJ
                    help='Output variance.')
parser.add_argument('--hard', action='store_true', default=False,
                    help='Uses discrete samples in training forward pass.')
parser.add_argument('--lr-decay', type=int, default=200,
                    help='After how epochs to decay LR by a factor of gamma.')
parser.add_argument('--gamma', type=float, default=0.5,
                    help='LR decay factor.')
parser.add_argument('--train-examples', type=int, default=7000, # 600, 3000
                    help='Number of train examples (number of simulations).')
parser.add_argument('--valid-examples', type=int, default=1500, # 200, 500
                    help='Number of valid examples (number of simulations).')
parser.add_argument('--test-examples', type=int, default=1500,  # 200, 500
                    help='Number of test examples (number of simulations).')
# which method to use for computing the posterior
parser.add_argument('--cmpt-posterior', type=str, default='exact', help='Which method to use for computing the posterior ("exact", "mean-field", or "mean-field-block").')
# With mean-field approximation:
parser.add_argument('--MF-blocks', type=int, default=2, help='Number of blocks for the mean-field')
parser.add_argument('--MF-iterations', type=int, default=10, help='Number of iterations for computing mean-field approximation')


## model loading
parser.add_argument('--load_folder', type=str, default='',
                    help='Where to load the trained model if finetunning. ' + 'Leave empty to train from scratch')


## which data to use
parser.add_argument('--train_data', type=str, default='True')
parser.add_argument('--valid_data', type=str, default='True')
parser.add_argument('--test_data', type=str, default='True')


parser.add_argument('--mode', type=str, default='', help='[interpolation] or [extrapolation]')



args = parser.parse_args()
args.cuda = not args.no_cuda and torch.cuda.is_available()




assert args.cuda == True
print(args)


assert args.mode in ["interpolation", "extrapolation"]


# np.random.seed(args.seed)
# torch.manual_seed(args.seed)
# if args.cuda:
#     torch.cuda.manual_seed(args.seed)




### Global variables
# realization_edgeType_R_IE_K



num_realizations = args.edge_types ** args.num_IE 
num_complete_edges = args.num_all_particles * (args.num_all_particles - 1) # if some edge does not appear in the whole simulation, just do not infer it
print("######## In main ########")
print("num_types: ", args.edge_types)
print("num_IE: ", args.num_IE)
print("num_AE: ", args.num_IE * args.num_subgraphs)
print("num_realizations: ", num_realizations)
print("num_complete_edges: ", num_complete_edges)



def return_latent_space() -> np.ndarray:
    # The space for all possible realizations for every subgraph
    # for every n-1 edges (incoming edges for a node), return the all possible combinations of interaction type on these n-1 edges
    # return shape: [args.edge_types**(num_node-1), num_node-1, edge_types]
    # another implementation way: https://stackoverflow.com/questions/63839024/one-hot-encode-numpy-array-with-2-dims
    edges_states = np.zeros(shape=(num_realizations, args.num_IE, args.edge_types), dtype=np.float64) # todo: np.int64 can be reduced
    for i in range(num_realizations):
        this_str = np.base_repr(i, base=args.edge_types)
        assert len(this_str) <= args.num_IE
        this_str = "0" * (args.num_IE - len(this_str) ) + this_str
        assert len(this_str) == args.num_IE
        for e_id in range(args.num_IE):
            mask = this_str[e_id]
            mask = int(mask)
            edges_states[i, e_id, mask] = 1
    return edges_states



realization_edgeType_R_IE_K = return_latent_space()
realization_edgeType_R_IE_K = torch.from_numpy(realization_edgeType_R_IE_K).cuda()
print("realization_edgeType_R_IE_K shape: ", realization_edgeType_R_IE_K.shape)
# print("realization_edgeType_R_IE_K")
# print(realization_edgeType_R_IE_K)


## for computing the marginal distribution
def return_edgeType_realizations(realization_edgeType_R_IE_K) -> np.ndarray:
    ## the previously constructed realization_edgeType_R_IE_K
    ## return edgeType_realization_IE_K_R. All realizations that contribute to the marginal distribution
    edgeType_realization_IE_K_R = np.zeros(shape = (args.num_IE, args.edge_types, num_realizations), dtype = np.float64)    
    realization_edgeType_R_IE_K  = realization_edgeType_R_IE_K.cpu().detach().numpy()
    realization_edgeType_R_IE = realization_edgeType_R_IE_K.argmax(axis = -1)
    for i in range(num_realizations):
        for j in range(args.num_IE):
            this_edge_type = realization_edgeType_R_IE[i, j]
            edgeType_realization_IE_K_R[j, this_edge_type, i] = 1.0
    return edgeType_realization_IE_K_R
marginal_edgeType_realization_IE_K_R = return_edgeType_realizations(realization_edgeType_R_IE_K)
marginal_edgeType_realization_IE_K_R = torch.from_numpy(marginal_edgeType_realization_IE_K_R).cuda()
# print("marginal_edgeType_realization_IE_K_R")
# print(marginal_edgeType_realization_IE_K_R)
# exit()

## Note: realization_edgeType_R_IE_K and marginal_edgeType_realization_IE_K_R are different in that: realization_edgeType_R_IE_K return the
## edge type for every realization. marginal_edgeType_realization_IE_K_R returns all realizations relavent to (IE, K).


## for incorporating the prior of other incoming edges than the center edge
def return_realization_edgeType_IE_R_IE_K(realization_edgeType_R_IE_K) -> np.ndarray:
    ## return realization_edgeType_IE_R_IE_K, which do not consider the center edge in the realization
    realization_edgeType_R_IE_K  = realization_edgeType_R_IE_K.cpu().detach().numpy()
    realization_edgeType_IE_R_IE_K = []
    for i in range(args.num_IE):
        this_realization_edgeType_R_IE_K = np.array(realization_edgeType_R_IE_K, copy = True)
        for j in range(num_realizations):
            this_realization_edgeType_R_IE_K[j, i, :] = 0.0
        realization_edgeType_IE_R_IE_K.append(this_realization_edgeType_R_IE_K)
    realization_edgeType_IE_R_IE_K = np.stack(realization_edgeType_IE_R_IE_K)
    return realization_edgeType_IE_R_IE_K

realization_edgeType_IE_R_IE_K = return_realization_edgeType_IE_R_IE_K(realization_edgeType_R_IE_K)
realization_edgeType_IE_R_IE_K = torch.from_numpy(realization_edgeType_IE_R_IE_K).cuda()
print("realization_edgeType_IE_R_IE_K shape: ", realization_edgeType_IE_R_IE_K.shape)
# print("realization_edgeType_IE_R_IE_K[0]")
# print(realization_edgeType_IE_R_IE_K[0])



###
# S: subgraph
# IE: incoming-edges for one subgraph
# AE: all edges in at one timestep
# CE: The complete edge set appeared in the whole simulation across different timesteps
# R: realizations


def get_ground_truth_relation(epoch, nodeIncomingEdges_B_S_IE_AE, truth_relations_B_AE):
    # nodeIncomingEdges_B_S_IE_AE in cuda
    # truth_relations_B_AE in cuda
    # return Prob_B_S_R

    nodeIncomingEdges_B_S_IE_AE = nodeIncomingEdges_B_S_IE_AE
    truth_relations_B_AE_K = torch.nn.functional.one_hot(truth_relations_B_AE, num_classes = args.edge_types).double()
    truth_relations_B_1_AE_K = truth_relations_B_AE_K.unsqueeze(1)
    truth_relation_B_S_IE_K = torch.matmul(nodeIncomingEdges_B_S_IE_AE, truth_relations_B_1_AE_K)
    truth_relation_B_S_IE = truth_relation_B_S_IE_K.argmax(dim = -1, keepdim = False).double()
    truth_relation_B_S_1_IE = truth_relation_B_S_IE.unsqueeze(2)

    base_incoding_IE_1 = torch.zeros((args.num_IE, 1), dtype=torch.float64)
    for i in range(args.num_IE):
        base_incoding_IE_1[i, 0] = args.edge_types ** (args.num_IE -1 - i)
    base_incoding_IE_1 = base_incoding_IE_1.cuda()

    # print(truth_relation_B_S_1_IE, truth_relation_B_S_1_IE.dtype)


    truth_realization_B_S_1_1 = torch.matmul(truth_relation_B_S_1_IE, base_incoding_IE_1.unsqueeze(0).unsqueeze(0))
    truth_realization_B_S = truth_realization_B_S_1_1.squeeze().type(torch.LongTensor)
    Prob_B_S_R = torch.nn.functional.one_hot(truth_realization_B_S, num_classes=num_realizations).double()

    ## verify
    realization_edgeType_R_IE = realization_edgeType_R_IE_K.argmax(dim = -1, keepdim = False).cpu().detach().numpy()
    nodeIncomingEdges_B_S_IE = nodeIncomingEdges_B_S_IE_AE.argmax(dim = -1, keepdim = False).cpu().detach().numpy()
    truth_relations_B_AE = truth_relations_B_AE.cpu().detach().numpy()
    this_batch_size = len(truth_relations_B_AE)
    
    for t in range(this_batch_size):
        for i in range(args.num_subgraphs):
            true_edgeType_IE = [truth_relations_B_AE[t, eid] for eid in nodeIncomingEdges_B_S_IE[t, i] ]
            # print("true_edgeType_IE")
            # print(true_edgeType_IE); exit()
            realization_id = truth_realization_B_S[t, i].item()
            this_realization_edgeType_IE = realization_edgeType_R_IE[realization_id].tolist()
            assert this_realization_edgeType_IE == true_edgeType_IE

    return Prob_B_S_R.cuda()


## exact infer edge types
def cmpt_categorical_distribution(epoch, decoder, input_B_AN_D, target_B_S_D, nodeIncomingEdges_B_S_IE_AE, rel_rec_B_E_AN, rel_send_B_E_AN, centerNodeID_B_S_AN, only_conditional):
    ## Input:
    # input_B_AN_D: the input states for all nodes, T * All_nodes * dimension
    # target_B_S_D: the target of subgraphs, T * subgraphs * dimensions
    # nodeIncomingEdges_B_S_IE_AE: the in-coming edges for all subgraphs. TODO, can be sparse input
    # rel_rec_B_E_AN: the edge-receiver matrix, T * AE * AN. TODO: Can be sparse input
    # rel_send_B_E_AN: the edge-sender matrix, T * AE * AN. TODO: Can be sparse input
    # only_conditional: ignore the mixing coefficients, only compute the conditional probability

    ## Output:
    # Prob_B_S_R: the probability of each subgraph, T * S * num_realizations

    this_batch_size = len(input_B_AN_D)
    num_subgraphs = target_B_S_D.shape[1]
    num_all_edges = rel_rec_B_E_AN.shape[1]
    assert num_all_edges == nodeIncomingEdges_B_S_IE_AE.shape[-1]
    
      
    with torch.no_grad():
        edgeForces_B_AE_K_D = decoder.cmpt_all_type_forces_all_edges(input_B_AN_D, rel_rec_B_E_AN, rel_send_B_E_AN)
        # edgeForces_B_AE_K_D = decoder.cmpt_all_type_forces_all_edges_with_ground_truth(input_B_AN_D, rel_rec_B_E_AN, rel_send_B_E_AN)

    # print("edgeForces_B_AE_K_D shape: ", edgeForces_B_AE_K_D.shape); exit() torch.Size([8, 100, 2, 2])
    assert edgeForces_B_AE_K_D.shape[-1] == args.space_dim

    ## dense way
    nodeIncomingEdges_B_S_1_IE_AE = nodeIncomingEdges_B_S_IE_AE.unsqueeze(2)
    nodeIncomingEdges_B_S_1_IE_AE = nodeIncomingEdges_B_S_1_IE_AE.cuda()
    edgeForces_B_S_K_IE_D = torch.matmul(nodeIncomingEdges_B_S_1_IE_AE, edgeForces_B_AE_K_D.transpose(1, 2).unsqueeze(1) )
    # print("edgeForces_B_S_K_IE_D shape: ", edgeForces_B_S_K_IE_D.shape); exit() torch.Size([8, 20, 2, 5, 2])


    edgeForce_B_S_IE_K_D = edgeForces_B_S_K_IE_D.transpose(2, 3)

    edgeForce_B_S_R_IE_K_D = edgeForce_B_S_IE_K_D.unsqueeze(2) * realization_edgeType_R_IE_K.unsqueeze(-1).unsqueeze(0).unsqueeze(0)
    # print("edgeForce_B_S_R_IE_K_D shape: ", edgeForce_B_S_R_IE_K_D.shape) #  torch.Size([8, 20, 32, 5, 2, 2])

    netForce_B_S_R_D = edgeForce_B_S_R_IE_K_D.sum(dim=(3, 4), keepdim=False)

    trueNetForce_B_S_1_D = target_B_S_D.unsqueeze(2)

    
    logits_B_S_R_D = - (netForce_B_S_R_D - trueNetForce_B_S_1_D) ** 2
    # print("logits_B_S_R_D shape: ", logits_B_S_R_D.shape)

    logits_B_S_R = logits_B_S_R_D.sum(dim = (-1), keepdim = False)

    logits_B_S_R = logits_B_S_R / (2 * args.var)

    if only_conditional == False:
        ## mixing coefficients
        assert tau.shape == (args.edge_types, 1)
        tau_K_1 = torch.from_numpy(tau).cuda()
        logTau_K = torch.log(tau_K_1.squeeze(-1) )
        logTau_R_IE_K = realization_edgeType_R_IE_K * logTau_K[None, None, :] # select tau on every edge
        logTau_R = torch.sum(logTau_R_IE_K, dim=(1, 2), keepdim=False)

        logits_B_S_R = logits_B_S_R + logTau_R[None, None, :]


    Prob_B_S_R = torch.softmax(logits_B_S_R, dim=-1)
    # print("Prob_B_S_R shape: ", Prob_B_S_R.shape)


    return Prob_B_S_R



### [deleted] cmpt_CE_categorical_distribution(epoch, decoder) -> np.ndarray, does not multiplying the prior of P(z_{-i})
   
def cmpt_CE_categorical_distribution_2(epoch, decoder, train_data = None, valid_data = None, test_data = None):   
    ## compute the categorical distribution of the COMPLETE EDGEs
    ## consider the current tau, and conditional distribution give training data
    ## multiply other prior distribution as "smoothing"
    ground_truth_relation_AB_AE = [] # np.array
    predict_relation_AB_AE = [] # np.array
    # inferred_edgeType_B_AE_K = []
    # ML_realization_B_S = []
    nll_train = 0.0 # negative log likelihood loss
    MAE_acc_train = 0.0
    count_node_time = 0.0
    count_exp = 0

    min_prob = 1e-20
    
    ## check the consistency of the inferred interaction type of every edge at different time steps
    predict_edgeType_logProb_CE_K = np.zeros((num_complete_edges, args.edge_types), dtype = np.float64 ) # do not consider the elements at diagonal
    target_edgeType_CE = -1 * np.ones((num_complete_edges), dtype = np.int64 ) # initialize the ground-truth edge type
    count_times_edgeType_CE = np.zeros(num_complete_edges, dtype = np.int64) # count the number of each edge appeared in the whole simulation
    #initialize predict_edgeType_logProb_CE_K
    for i in range(args.edge_types):
        predict_edgeType_logProb_CE_K[:, i] = np.log(tau[i, 0])
    predict_edgeType_logProb_CE_K = torch.from_numpy(predict_edgeType_logProb_CE_K).cuda()

    # print("predict_edgeType_logProb_CE_K at beginning")
    # print(predict_edgeType_logProb_CE_K)

    decoder.eval()
    # train_val_test_data_loader = [train_loader, valid_loader, test_loader] # similar to the "semi-supervised classification"
    # train_val_test_data_loader = [train_loader, valid_loader] # similar to the "semi-supervised classification"
    train_val_test_data_loader = []
    if train_data == True:
        train_val_test_data_loader.append(train_loader)
    if valid_data == True:
        train_val_test_data_loader.append(valid_loader)
    if test_data == True:
        train_val_test_data_loader.append(test_loader)

    assert len(train_val_test_data_loader) > 0

    for data_loader in train_val_test_data_loader:
        for batch_idx, (input_B_AN_D, target_B_S_D, true_pairwise_force_B_AE_D, centerNodeID_B_S_AN, nodeIncomingEdges_B_S_IE_AE, rel_rec_B_E_AN, rel_send_B_E_AN, edgeID_B_AE, truth_relations_B_AE, exp_id) in enumerate(data_loader):        
            this_batch_size = len(exp_id)
            count_exp += this_batch_size
            assert args.num_subgraphs == target_B_S_D.shape[1]
            num_all_edges = rel_rec_B_E_AN.shape[1]

            ## move to cuda
            input_B_AN_D, target_B_S_D, centerNodeID_B_S_AN, nodeIncomingEdges_B_S_IE_AE, rel_rec_B_E_AN, rel_send_B_E_AN, truth_relations_B_AE = \
            input_B_AN_D.cuda(), target_B_S_D.cuda(), centerNodeID_B_S_AN.cuda(), nodeIncomingEdges_B_S_IE_AE.cuda(), rel_rec_B_E_AN.cuda(), rel_send_B_E_AN.cuda(), truth_relations_B_AE.cuda()
            # edgeID_B_AE = edgeID_B_AE.cuda()

            ## predicted conditional probability?
            Prob_B_S_R = cmpt_categorical_distribution(epoch, decoder, input_B_AN_D, target_B_S_D, nodeIncomingEdges_B_S_IE_AE, rel_rec_B_E_AN, rel_send_B_E_AN, centerNodeID_B_S_AN, only_conditional = True)

            # Prob_B_S_R = Prob_B_S_R + min_prob # avoid 0 in logarithm

            ## update predict_edgeType_logProb_CE_K at every time step
            for i in range(this_batch_size):
                # extract the current prior probability
                edgeID_AE = edgeID_B_AE[i].detach().numpy()

                prior_logProb_AE_K = predict_edgeType_logProb_CE_K[edgeID_AE, :]
                # print("prior_logProb_AE_K shape: ", prior_logProb_AE_K.shape); exit() # torch.Size([300, 2])

                nodeIncomingEdges_S_IE_AE = nodeIncomingEdges_B_S_IE_AE[i]
                prior_logProb_S_IE_AE_K = nodeIncomingEdges_S_IE_AE[:, :, :, None] * prior_logProb_AE_K[None, None, :, :]
                prior_logProb_S_IE_K = prior_logProb_S_IE_AE_K.sum(dim = 2, keepdim = False)
                # print("prior_logProb_S_IE_K shape: ", prior_logProb_S_IE_K.shape); exit() # torch.Size([60, 5, 2])


                prior_logProb_IE_R_S_IE_K = realization_edgeType_IE_R_IE_K[:, :, None, :, :] * prior_logProb_S_IE_K[None, None, :, :, :]
                prior_logProb_IE_R_S = prior_logProb_IE_R_S_IE_K.sum(dim = (3, 4), keepdim = False)

                prior_logProb_S_IE_R = prior_logProb_IE_R_S.permute(2, 0, 1)
                # print("prior_logProb_S_IE_R shape: ", prior_logProb_S_IE_R.shape); exit() # torch.Size([60, 5, 32])

                # print("check prior_logProb_S_IE_R last dimension")
                # print(torch.exp(prior_logProb_S_IE_R)[0, :, :16].sum(-1)); exit()

                prior_Prob_S_IE_R = torch.exp(prior_logProb_S_IE_R)

                # the likelihood
                marginalProb_S_IE_K_R = marginal_edgeType_realization_IE_K_R[None, :, :, :] * Prob_B_S_R[i, :, None, None, :]
                # print("marginalProb_S_IE_K_R[0, 0, 0]")
                # print(marginalProb_S_IE_K_R[0, 0, 0]); exit()
                # print(marginalProb_S_IE_K_R.sum(-1).sum(-1)); exit()

                marginalProb_S_IE_K_R = marginalProb_S_IE_K_R * prior_Prob_S_IE_R[:, :, None, :]

                marginalProb_S_IE_K = marginalProb_S_IE_K_R.sum(dim = -1, keepdim = False)
                marginalProb_S_IE_K = marginalProb_S_IE_K / marginalProb_S_IE_K.sum(dim = -1, keepdim = True)
                # print("marginalProb_S_IE_K shape: ", marginalProb_S_IE_K.shape)
                # exit()

                # marginalProb_AE_K <- marginalProb_S_IE_K, nodeIncomingEdges_B_S_IE_AE
                marginalProb_S_IE_K_AE = marginalProb_S_IE_K[:, :, :, None] * nodeIncomingEdges_B_S_IE_AE[i, :, :, None, :]
                # print("marginalProb_S_IE_K_AE shape: ", marginalProb_S_IE_K_AE.shape); exit() # torch.Size([60, 5, 2, 300])

                marginalProb_K_AE = marginalProb_S_IE_K_AE.sum(dim=(0, 1), keepdim=False)

                marginalProb_AE_K = marginalProb_K_AE.permute(1, 0)

                # log_marginal_prob_AE_K = torch.log(marginalProb_AE_K)
                prob_AE_K = torch.exp(prior_logProb_AE_K) * marginalProb_AE_K

                prob_AE_K = prob_AE_K + min_prob

                prob_AE_K = prob_AE_K / prob_AE_K.sum(dim = -1, keepdim = True)

                log_prob_AE_K = torch.log(prob_AE_K)

                predict_edgeType_logProb_CE_K[edgeID_AE, :] = log_prob_AE_K

        
    predict_edgeType_logProb_CE_K = predict_edgeType_logProb_CE_K.cpu().detach().numpy()
    predict_edgeType_Prob_CE_K = np.exp(predict_edgeType_logProb_CE_K)
    
    return predict_edgeType_Prob_CE_K

 

def cmpt_MAE_acceleration(epoch, decoder, train_data = None, valid_data = None, test_data = None):
    t = time.time()                        
    print("## In cmpt_MAE_acceleration, train_data: {}, valid_data: {}, test_data: {}".format(train_data, valid_data, test_data) )
    MAE_acc_metric = 0.0
    count_node_time = 0.0
    count_exp = 0
    
    ## predict the type of COMPLETE EDGEs
    Prob_CE_K = cmpt_CE_categorical_distribution_2(epoch, decoder, train_data, valid_data, test_data)
    Prob_CE_K = torch.from_numpy(Prob_CE_K).cuda()

    decoder.eval()
    
    ## use test data
    for batch_idx, (input_B_AN_D, target_B_S_D, true_pairwise_force_B_AE_D, centerNodeID_B_S_AN, nodeIncomingEdges_B_S_IE_AE, rel_rec_B_E_AN, rel_send_B_E_AN, edgeID_B_AE, truth_relations_B_AE, exp_id) in tqdm(enumerate(test_loader)):

        this_batch_size = len(exp_id)
        count_exp += this_batch_size
        assert args.num_subgraphs == target_B_S_D.shape[1]
        num_all_edges = rel_rec_B_E_AN.shape[1]

        ## move to cuda
        input_B_AN_D, target_B_S_D, centerNodeID_B_S_AN, nodeIncomingEdges_B_S_IE_AE, rel_rec_B_E_AN, rel_send_B_E_AN, truth_relations_B_AE = \
        input_B_AN_D.cuda(), target_B_S_D.cuda(), centerNodeID_B_S_AN.cuda(), nodeIncomingEdges_B_S_IE_AE.cuda(), rel_rec_B_E_AN.cuda(), rel_send_B_E_AN.cuda(), truth_relations_B_AE.cuda()
        
        edgeForces_B_AE_K_D = decoder.cmpt_all_type_forces_all_edges(input_B_AN_D, rel_rec_B_E_AN, rel_send_B_E_AN)

        ## compute the average force for every edge
        edgeID_B_AE_CE = torch.nn.functional.one_hot(edgeID_B_AE, num_classes = num_complete_edges ).cuda().double()
        # Prob_CE_K, edgeID_B_AE_CE -> Prob_B_AE_K
        Prob_B_AE_CE_K = Prob_CE_K[None, None, :, :] * edgeID_B_AE_CE[:, :, :, None]
        Prob_B_AE_K = Prob_B_AE_CE_K.sum(dim = 2, keepdim = False)


        # Prob_B_AE_K, edgeForces_B_AE_K_D -> edge_mean_Forces_B_AE_D
        edgeForces_B_AE_K_D = edgeForces_B_AE_K_D * Prob_B_AE_K[:, :, :, None]
        edge_mean_Forces_B_AE_D = edgeForces_B_AE_K_D.sum(dim = 2, keepdim = False)
        # print("edge_mean_Forces_B_AE_D shape: ", edge_mean_Forces_B_AE_D.shape); exit() torch.Size([128, 300, 2])

        # edge_mean_Forces_B_AE_D, nodeIncomingEdges_B_S_IE_AE -> edgeForces_B_S_IE_D
        edgeForces_B_S_IE_D = torch.matmul(nodeIncomingEdges_B_S_IE_AE, edge_mean_Forces_B_AE_D.unsqueeze(1) )
        # print("edgeForces_B_S_IE_D shape: ", edgeForces_B_S_IE_D.shape); exit() # torch.Size([128, 60, 5, 2])

        netForce_B_S_D = edgeForces_B_S_IE_D.sum(dim=2, keepdim=False)

        diff_B_S_D = (target_B_S_D - netForce_B_S_D) ** 2 

        MAE_acc_metric += torch.sum(torch.abs(target_B_S_D * np.sqrt(target_var) - netForce_B_S_D * np.sqrt(target_var) ) ).item()
        count_node_time += this_batch_size * args.num_subgraphs
        
    ## evaluation metrics
    mean_MAE_acceleration_metric = MAE_acc_metric / count_node_time
    return mean_MAE_acceleration_metric

    


def cmpt_MAE_symm(decoder, map_true2pred, train_data = None, valid_data = None, test_data = None):
    t = time.time()                        
    print("## In cmpt_MAE_symm, train_data: {}, valid_data: {}, test_data: {}".format(train_data, valid_data, test_data) )
    MAE_symm_error = 0.0
    count_edge_time = 0.0
    count_exp = 0
    
    # ## predict the type of COMPLETE EDGEs
    # Prob_CE_K = cmpt_CE_categorical_distribution_2(epoch, decoder, train_data, valid_data, test_data)
    # Prob_CE_K = torch.from_numpy(Prob_CE_K).cuda()

    decoder.eval()
    
    ## use test data
    for batch_idx, (input_B_AN_D, target_B_S_D, true_pairwise_force_B_AE_D, centerNodeID_B_S_AN, nodeIncomingEdges_B_S_IE_AE, rel_rec_B_E_AN, rel_send_B_E_AN, edgeID_B_AE, truth_relations_B_AE, exp_id) in tqdm(enumerate(test_loader)):

        for i in range(truth_relations_B_AE.shape[0]):
            for j in range(truth_relations_B_AE.shape[1]):
                truth_relations_B_AE[i, j] = map_true2pred[truth_relations_B_AE[i, j] ]


        this_batch_size = len(exp_id)
        count_exp += this_batch_size
        assert args.num_subgraphs == target_B_S_D.shape[1]
        num_all_edges = rel_rec_B_E_AN.shape[1]

        ## move to cuda
        input_B_AN_D, target_B_S_D, centerNodeID_B_S_AN, nodeIncomingEdges_B_S_IE_AE, rel_rec_B_E_AN, rel_send_B_E_AN, truth_relations_B_AE = \
        input_B_AN_D.cuda(), target_B_S_D.cuda(), centerNodeID_B_S_AN.cuda(), nodeIncomingEdges_B_S_IE_AE.cuda(), rel_rec_B_E_AN.cuda(), rel_send_B_E_AN.cuda(), truth_relations_B_AE.cuda()
        
        with torch.no_grad():
            edgeForces_B_AE_K_D = decoder.cmpt_all_type_forces_all_edges(input_B_AN_D, rel_rec_B_E_AN, rel_send_B_E_AN)
            inverse_edgeForces_B_AE_K_D = decoder.cmpt_all_type_forces_all_edges(input_B_AN_D, rel_send_B_E_AN, rel_rec_B_E_AN)

        # ## compute the average force for every edge
        # edgeID_B_AE_CE = torch.nn.functional.one_hot(edgeID_B_AE, num_classes = num_complete_edges ).cuda().double()
        # # Prob_CE_K, edgeID_B_AE_CE -> Prob_B_AE_K
        # Prob_B_AE_CE_K = Prob_CE_K[None, None, :, :] * edgeID_B_AE_CE[:, :, :, None]
        # Prob_B_AE_K = Prob_B_AE_CE_K.sum(dim = 2, keepdim = False)

        Prob_B_AE_K = torch.nn.functional.one_hot(truth_relations_B_AE, num_classes = args.edge_types).double()


        # Prob_B_AE_K, edgeForces_B_AE_K_D -> edge_mean_Forces_B_AE_D
        edgeForces_B_AE_K_D = edgeForces_B_AE_K_D * Prob_B_AE_K[:, :, :, None]
        edge_mean_Forces_B_AE_D = edgeForces_B_AE_K_D.sum(dim = 2, keepdim = False)
        # print("edge_mean_Forces_B_AE_D shape: ", edge_mean_Forces_B_AE_D.shape); exit() torch.Size([128, 300, 2])

        inverse_edgeForces_B_AE_K_D  = inverse_edgeForces_B_AE_K_D * Prob_B_AE_K[:, :, :, None]
        inverse_edge_mean_Forces_B_AE_D = inverse_edgeForces_B_AE_K_D.sum(dim = 2, keepdim = False)
 

        MAE_symm_error += torch.sum(torch.abs(edge_mean_Forces_B_AE_D * np.sqrt(target_var) + inverse_edge_mean_Forces_B_AE_D * np.sqrt(target_var) ) ).item()
        count_edge_time += this_batch_size * edge_mean_Forces_B_AE_D.shape[1]
        
    ## evaluation metrics
    mean_MAE_symm = MAE_symm_error / count_edge_time
    return mean_MAE_symm



def cmpt_MAE_ef(decoder, map_true2pred, train_data = None, valid_data = None, test_data = None):
    t = time.time()                        
    print("## In cmpt_MAE_ef, train_data: {}, valid_data: {}, test_data: {}".format(train_data, valid_data, test_data) )
    MAE_ef_error = 0.0
    average_true_pairwise_force = 0.0
    count_edge_time = 0.0
    count_exp = 0
    
    # ## predict the type of COMPLETE EDGEs
    # Prob_CE_K = cmpt_CE_categorical_distribution_2(epoch, decoder, train_data, valid_data, test_data)
    # Prob_CE_K = torch.from_numpy(Prob_CE_K).cuda()

    decoder.eval()
    
    ## use test data
    for batch_idx, (input_B_AN_D, target_B_S_D, true_pairwise_force_B_AE_D, centerNodeID_B_S_AN, nodeIncomingEdges_B_S_IE_AE, rel_rec_B_E_AN, rel_send_B_E_AN, edgeID_B_AE, truth_relations_B_AE, exp_id) in tqdm( enumerate(test_loader)):
        
        for i in range(truth_relations_B_AE.shape[0]):
            for j in range(truth_relations_B_AE.shape[1]):
                truth_relations_B_AE[i, j] = map_true2pred[truth_relations_B_AE[i, j] ]

        this_batch_size = len(exp_id)
        count_exp += this_batch_size
        assert args.num_subgraphs == target_B_S_D.shape[1]
        num_all_edges = rel_rec_B_E_AN.shape[1]

        ## move to cuda
        input_B_AN_D, target_B_S_D, centerNodeID_B_S_AN, nodeIncomingEdges_B_S_IE_AE, rel_rec_B_E_AN, rel_send_B_E_AN, truth_relations_B_AE = \
        input_B_AN_D.cuda(), target_B_S_D.cuda(), centerNodeID_B_S_AN.cuda(), nodeIncomingEdges_B_S_IE_AE.cuda(), rel_rec_B_E_AN.cuda(), rel_send_B_E_AN.cuda(), truth_relations_B_AE.cuda()

        true_pairwise_force_B_AE_D = true_pairwise_force_B_AE_D.cuda() # note true_pairwise_force_B_AE_D is not normalized
        
        with torch.no_grad():
            edgeForces_B_AE_K_D = decoder.cmpt_all_type_forces_all_edges(input_B_AN_D, rel_rec_B_E_AN, rel_send_B_E_AN)


        # ## compute the average force for every edge
        # edgeID_B_AE_CE = torch.nn.functional.one_hot(edgeID_B_AE, num_classes = num_complete_edges ).cuda().double()
        # # Prob_CE_K, edgeID_B_AE_CE -> Prob_B_AE_K
        # Prob_B_AE_CE_K = Prob_CE_K[None, None, :, :] * edgeID_B_AE_CE[:, :, :, None]
        # Prob_B_AE_K = Prob_B_AE_CE_K.sum(dim = 2, keepdim = False)

                        
        Prob_B_AE_K = torch.nn.functional.one_hot(truth_relations_B_AE, num_classes = args.edge_types).double()
        

        # Prob_B_AE_K, edgeForces_B_AE_K_D -> edge_mean_Forces_B_AE_D
        edgeForces_B_AE_K_D = edgeForces_B_AE_K_D * Prob_B_AE_K[:, :, :, None]
        edge_mean_Forces_B_AE_D = edgeForces_B_AE_K_D.sum(dim = 2, keepdim = False)
        # print("edge_mean_Forces_B_AE_D shape: ", edge_mean_Forces_B_AE_D.shape); exit() torch.Size([128, 300, 2])

        MAE_ef_error += torch.sum(torch.abs(edge_mean_Forces_B_AE_D * np.sqrt(target_var) - true_pairwise_force_B_AE_D ) ).item()
        average_true_pairwise_force += torch.sum(torch.abs( true_pairwise_force_B_AE_D ) ).item()
        count_edge_time += this_batch_size * edge_mean_Forces_B_AE_D.shape[1]
        
    ## evaluation metrics
    mean_MAE_ef = MAE_ef_error / count_edge_time
    return mean_MAE_ef, average_true_pairwise_force/count_edge_time



    
def test_relation_accuracy_2(epoch, decoder, train_data = None, valid_data = None, test_data = None): 
    print("train_data ", train_data, "valid_data ",  valid_data, "test_data ", test_data) 
    if train_data == False and valid_data == False and test_data == False:
        print("In test_relation_accuracy_2, at least one dataset in [train], [valid], [test] should be used")
        exit()
    # check the predicted edge type and the ground-truth edge type at every time step
    ground_truth_relation_AB_AE = [] # np.array
    predict_relation_AB_AE = [] # np.array
    # inferred_edgeType_B_AE_K = []
    # ML_realization_B_S = []
    nll_train = 0.0 # negative log likelihood loss
    MAE_acc_train = 0.0
    count_node_time = 0.0
    count_exp = 0

    min_prob = 1e-20
    
    ## check the consistency of the inferred interaction type of every edge at different time steps
    predict_edgeType_logProb_CE_K = np.zeros((num_complete_edges, args.edge_types), dtype = np.float64 ) # do not consider the elements at diagonal
    target_edgeType_CE = -1 * np.ones((num_complete_edges), dtype = np.int64 ) # initialize the ground-truth edge type
    count_times_edgeType_CE = np.zeros(num_complete_edges, dtype = np.int64) # count the number of each edge appeared in the whole simulation
    #initialize predict_edgeType_logProb_CE_K
    for i in range(args.edge_types):
        predict_edgeType_logProb_CE_K[:, i] = np.log(tau[i, 0])
    predict_edgeType_logProb_CE_K = torch.from_numpy(predict_edgeType_logProb_CE_K).cuda()

    # print("predict_edgeType_logProb_CE_K at beginning")
    # print(predict_edgeType_logProb_CE_K)

    decoder.eval()
    # train_val_test_data_loader = [train_loader, valid_loader, test_loader] # similar to the "semi-supervised classification"
    # train_val_test_data_loader = [train_loader, valid_loader] # similar to the "semi-supervised classification"

    train_val_test_data_loader = []
    if train_data == True:
        train_val_test_data_loader.append(train_loader)
    if valid_data == True:
        train_val_test_data_loader.append(valid_loader)
    if test_data == True:
        train_val_test_data_loader.append(test_loader)

    assert len(train_val_test_data_loader) > 0

    for data_loader in train_val_test_data_loader:
        for batch_idx, (input_B_AN_D, target_B_S_D, true_pairwise_force_B_AE_D, centerNodeID_B_S_AN, nodeIncomingEdges_B_S_IE_AE, rel_rec_B_E_AN, rel_send_B_E_AN, edgeID_B_AE, truth_relations_B_AE, exp_id) in tqdm(enumerate(data_loader)):        

            this_batch_size = len(exp_id)
            count_exp += this_batch_size
            assert args.num_subgraphs == target_B_S_D.shape[1]
            num_all_edges = rel_rec_B_E_AN.shape[1]

            ## move to cuda
            input_B_AN_D, target_B_S_D, centerNodeID_B_S_AN, nodeIncomingEdges_B_S_IE_AE, rel_rec_B_E_AN, rel_send_B_E_AN, truth_relations_B_AE = \
            input_B_AN_D.cuda(), target_B_S_D.cuda(), centerNodeID_B_S_AN.cuda(), nodeIncomingEdges_B_S_IE_AE.cuda(), rel_rec_B_E_AN.cuda(), rel_send_B_E_AN.cuda(), truth_relations_B_AE.cuda()
            # edgeID_B_AE = edgeID_B_AE.cuda()

            ## predicted conditional probability?
            Prob_B_S_R = cmpt_categorical_distribution(epoch, decoder, input_B_AN_D, target_B_S_D, nodeIncomingEdges_B_S_IE_AE, rel_rec_B_E_AN, rel_send_B_E_AN, centerNodeID_B_S_AN, only_conditional = True)

            # Prob_B_S_R = Prob_B_S_R + min_prob # avoid 0 in logarithm

            ## update predict_edgeType_logProb_CE_K at every time step
            for i in range(this_batch_size):
                # extract the current prior probability
                edgeID_AE = edgeID_B_AE[i].detach().numpy()

                prior_logProb_AE_K = predict_edgeType_logProb_CE_K[edgeID_AE, :]
                # print("prior_logProb_AE_K shape: ", prior_logProb_AE_K.shape); exit() # torch.Size([300, 2])

                nodeIncomingEdges_S_IE_AE = nodeIncomingEdges_B_S_IE_AE[i]
                prior_logProb_S_IE_AE_K = nodeIncomingEdges_S_IE_AE[:, :, :, None] * prior_logProb_AE_K[None, None, :, :]
                prior_logProb_S_IE_K = prior_logProb_S_IE_AE_K.sum(dim = 2, keepdim = False)
                # print("prior_logProb_S_IE_K shape: ", prior_logProb_S_IE_K.shape); exit() # torch.Size([60, 5, 2])


                prior_logProb_IE_R_S_IE_K = realization_edgeType_IE_R_IE_K[:, :, None, :, :] * prior_logProb_S_IE_K[None, None, :, :, :]
                prior_logProb_IE_R_S = prior_logProb_IE_R_S_IE_K.sum(dim = (3, 4), keepdim = False)

                prior_logProb_S_IE_R = prior_logProb_IE_R_S.permute(2, 0, 1)
                # print("prior_logProb_S_IE_R shape: ", prior_logProb_S_IE_R.shape); exit() # torch.Size([60, 5, 32])

                # print("check prior_logProb_S_IE_R last dimension")
                # print(torch.exp(prior_logProb_S_IE_R)[0, :, :16].sum(-1)); exit()

                prior_Prob_S_IE_R = torch.exp(prior_logProb_S_IE_R)

                # the likelihood
                marginalProb_S_IE_K_R = marginal_edgeType_realization_IE_K_R[None, :, :, :] * Prob_B_S_R[i, :, None, None, :]
                # print("marginalProb_S_IE_K_R[0, 0, 0]")
                # print(marginalProb_S_IE_K_R[0, 0, 0]); exit()
                # print(marginalProb_S_IE_K_R.sum(-1).sum(-1)); exit()

                marginalProb_S_IE_K_R = marginalProb_S_IE_K_R * prior_Prob_S_IE_R[:, :, None, :]

                marginalProb_S_IE_K = marginalProb_S_IE_K_R.sum(dim = -1, keepdim = False)
                marginalProb_S_IE_K = marginalProb_S_IE_K / marginalProb_S_IE_K.sum(dim = -1, keepdim = True)
                # print("marginalProb_S_IE_K shape: ", marginalProb_S_IE_K.shape)
                # print("marginalProb_S_IE_K")
                # print(marginalProb_S_IE_K)            
                # exit()

                # sum last dimesion should be 1

                # marginalProb_AE_K <- marginalProb_S_IE_K, nodeIncomingEdges_B_S_IE_AE
                marginalProb_S_IE_K_AE = marginalProb_S_IE_K[:, :, :, None] * nodeIncomingEdges_B_S_IE_AE[i, :, :, None, :]
                # print("marginalProb_S_IE_K_AE shape: ", marginalProb_S_IE_K_AE.shape); exit() # torch.Size([60, 5, 2, 300])


                marginalProb_K_AE = marginalProb_S_IE_K_AE.sum(dim=(0, 1), keepdim=False)

                marginalProb_AE_K = marginalProb_K_AE.permute(1, 0)
                # print("marginalProb_AE_K shape: ", marginalProb_AE_K.shape)
                # print("marginalProb_AE_K")
                # print(marginalProb_AE_K)
                # exit()

                # log_marginal_prob_AE_K = torch.log(marginalProb_AE_K)
                prob_AE_K = torch.exp(prior_logProb_AE_K) * marginalProb_AE_K

                prob_AE_K = prob_AE_K + min_prob

                prob_AE_K = prob_AE_K / prob_AE_K.sum(dim = -1, keepdim = True)


                # for itr in range(num_all_edges):
                #     if False in (prob_AE_K[itr] > 0.0):
                #         print("batch_idx: {}, i: {}, prob_AE_K[itr]: {}".format(batch_idx, i, prob_AE_K[itr]) )
                if False in (prob_AE_K > 0.0):
                    print("prob_AE_K has zero or smaller")
                    exit()

                log_prob_AE_K = torch.log(prob_AE_K)

                predict_edgeType_logProb_CE_K[edgeID_AE, :] = log_prob_AE_K


            ## update target_edgeType_CE
            truth_relations_B_AE = truth_relations_B_AE.cpu().detach().numpy()
            edgeID_B_AE = edgeID_B_AE.detach().numpy()
            for i in range(this_batch_size):
                CE_ids = edgeID_B_AE[i]
                CE_values = truth_relations_B_AE[i]
                target_edgeType_CE[CE_ids] = CE_values
                count_times_edgeType_CE[CE_ids] += 1
        
    predict_edgeType_logProb_CE_K = predict_edgeType_logProb_CE_K.cpu().detach().numpy()
    # predict_edgeType_Prob_CE_K = scipy.special.softmax(predict_edgeType_logProb_CE_K, axis = -1)
    predict_edgeType_Prob_CE_K = np.exp(predict_edgeType_logProb_CE_K)
    # print("predict_edgeType_Prob_CE_K in the end")
    # print(predict_edgeType_Prob_CE_K)
    predict_edgeType_CE = np.argmax(predict_edgeType_Prob_CE_K, axis = -1, keepdims = False)

    # consider the "meaningful" values in target_edgeType_CE
    mask = target_edgeType_CE != -1

    # for i in range(1000):
    #     if mask[i] == False:
    #         print("No mask, ", predict_edgeType_Prob_CE_K[i])
    #     else:
    #         print("Mask, ", predict_edgeType_Prob_CE_K[i])

    equal_edges_CE = np.equal(predict_edgeType_CE, target_edgeType_CE)
    equal_number = (equal_edges_CE * mask).sum()

    opposite_edges_CE = (np.equal(1 - predict_edgeType_CE, target_edgeType_CE) )
    opposite_equal_number = (opposite_edges_CE * mask).sum()

    assert equal_number + opposite_equal_number == mask.sum()

    accuracy = max([equal_number/mask.sum(), opposite_equal_number/mask.sum()])
    print("epoch {}, total_appeared_edges: {}, equal_number: {}, opposite_equal_number: {}, accuracy: {}".format(epoch, mask.sum(), equal_number, opposite_equal_number, accuracy))    

    map_true2pred = []
    if equal_number > opposite_equal_number:
        map_true2pred = [0, 1]
    else:
        map_true2pred= [1, 0]

    return accuracy, map_true2pred, predict_edgeType_CE, target_edgeType_CE, count_times_edgeType_CE

 

if __name__ == "__main__":

    ## test
    print("###### testing #####")

    ## data to use
    if args.train_data == "True":
        train_data = True
    elif args.train_data == "False":
        train_data = False
    else:
        raise RuntimeError('invalid args.train_data')
    
    if args.valid_data == "True":
        valid_data = True
    elif args.valid_data == "False":
        valid_data = False
    else:
        raise RuntimeError('invalid args.valid_data')
    
    if args.test_data == "True":
        test_data = True
    elif args.test_data == "False":
        test_data = False
    else:
        raise RuntimeError("Invalid args.test_data")


    ### The model
    print("#### Initialize model ####")
    if args.decoder == 'pignpi':
        decoder = MLP_PIGNPI_Decoder(n_in_node=args.input_dims,
                             edge_types=args.edge_types,
                             msg_hid=args.decoder_hidden,
                             msg_out=args.space_dim,
                             n_hid=args.decoder_hidden,
                             do_prob=args.decoder_dropout)
    else:
        print("decoder init error")
        exit()
    decoder.cuda()

    ## the coefficients
    tau = np.array([1.0 / args.edge_types] * args.edge_types, dtype = np.float64)
    tau = tau.reshape(args.edge_types, 1)
    print("### Initialize tau as: ", tau)



    ## load the trained model
    # load_folder = "trained_EM_LJ_N100_var0.001/train_7000simulations/exp1/"
    load_folder = args.load_folder

    epoch = -1

    # decoder_file_name = os.path.join(load_folder, "decoder_{}.pt".format(epoch))
    # tau_file_name = os.path.join(load_folder, "tau_{}.pkl".format(epoch))    
    decoder_file_name = os.path.join(load_folder, "best_decoder.pt")
    decoder.load_state_dict(torch.load(decoder_file_name))
    tau_file_name = os.path.join(load_folder, "best_decoder_tau.pkl")
    with open(tau_file_name, "rb") as f:
        tau = pickle.load(f)
    print("load tau as: ", tau)



    ## load data
    train_loader, valid_loader, test_loader = load_LJ_data_test(
    args.input_dir, args.batch_size, args.train_examples, args.valid_examples, args.test_examples, save_min_max_dir="-1", testing = True, min_max_folder = args.load_folder, mode=args.mode)

    # load min_max_dict
    with open(args.load_folder + "min_max_dict.pkl", "rb") as f:
        min_max_dict = pkl.load(f)
        input_var = min_max_dict["input_var"]
        target_var = min_max_dict["target_var"]

    ### relational accuracy
    # test_relation_accuracy_different_time_steps(epoch, decoder)
    # test_relation_accuracy(epoch, decoder)
    accuracy, map_true2pred, predict_edgeType_CE, target_edgeType_CE, count_times_edgeType_CE = test_relation_accuracy_2(epoch, decoder, train_data, valid_data, test_data)
    print("relational accuracy: ", accuracy)
    print("map_true2pred: ", map_true2pred)

    # ### save the relational prediction
    # save_path = load_folder+"check_accuracy/"
    # Path(save_path).mkdir(parents=True, exist_ok=True)

    # result_file_name = "Accuracy_result_Train_{}_Valid_{}_Test_{}.pkl".format(train_data, valid_data, test_data)
    # result_to_save = {
    #     "accuracy": accuracy,
    #     "predict_edgeType_CE": predict_edgeType_CE,
    #     "target_edgeType_CE": target_edgeType_CE,
    #     "count_times_edgeType_CE": count_times_edgeType_CE
    # }
    # with open(save_path + result_file_name, "wb") as f:
    #     pkl.dump(result_to_save, f)
    # print("Saved results into " + save_path + result_file_name)




    ### MAE_acceleration
    MAE_acceleration = cmpt_MAE_acceleration(epoch, decoder, train_data = train_data, valid_data = valid_data, test_data = test_data)
    print("MAE_acceleration: ", MAE_acceleration)

    ### MAE_symm
    MAE_symm = cmpt_MAE_symm(decoder, map_true2pred, train_data = train_data, valid_data = valid_data, test_data = test_data)
    print("MAE_symm: ", MAE_symm)


    ### MAE_ef
    MAE_ef, average_true_pairwise_force = cmpt_MAE_ef(decoder, map_true2pred, train_data = train_data, valid_data = valid_data, test_data = test_data)
    print("MAE_ef: ", MAE_ef)
    print("average_true_pairwise_force: ", average_true_pairwise_force)






# CUDA_VISIBLE_DEVICES=2 python test_LJ.py --load_folder "trained_EM_LJ_N100_var0.001/train_7000simulations/exp1/" --var 0.001 --batch-size 32 --train_data False --valid_data False --test_data True
