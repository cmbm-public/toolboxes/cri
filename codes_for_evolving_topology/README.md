# Evolving-CRI

This folder contains the implementation of Evolving-CRI, which is designed for relational inference for the particle systems with the evolving graph topology, _i.e._, each particle interacts with the different neighbors across different time steps.  



### Usage
```bash
# train Evolving-CRI for interpolation
CUDA_VISIBLE_DEVICES=0 python train_EvolvingCRI.py --save_folder trained_EvolvingCRI_LJ_N100_var0.001_interpolation --train-examples 7000 --valid-examples 1500 --test-examples 1500 --epochs 500 --var 0.001 --batch-size 32 --mode interpolation --seed 123 
# test the trained model
CUDA_VISIBLE_DEVICES=0 python test_EvolvingCRI.py --load_folder "trained_EvolvingCRI_LJ_N100_var0.001_interpolation/train_7000simulations/exp1/" --var 0.001 --batch-size 32 --train_data False --valid_data False --test_data True --mode interpolation > trained_EvolvingCRI_LJ_N100_var0.001_interpolation/train_7000simulations/interpolation_results.txt

##############

# train Evolving-CRI for extrapolation
CUDA_VISIBLE_DEVICES=0 python train_EvolvingCRI.py --save_folder trained_EvolvingCRI_LJ_N100_var0.001_extrapolation --train-examples 7000 --valid-examples 1500 --test-examples 1500 --epochs 500 --var 0.001 --batch-size 32 --mode extrapolation --seed 123 
# test the trained model
CUDA_VISIBLE_DEVICES=0 python test_EvolvingCRI.py --load_folder "trained_EvolvingCRI_LJ_N100_var0.001_extrapolation/train_7000simulations/exp1/" --var 0.001 --batch-size 32 --train_data False --valid_data False --test_data True --mode extrapolation > trained_EvolvingCRI_LJ_N100_var0.001_extrapolation/train_7000simulations/extrapolation_results.txt
```

