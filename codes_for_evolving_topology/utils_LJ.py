import numpy as np
import torch
from torch.utils.data.dataset import TensorDataset
from torch.utils.data import DataLoader
import torch.nn.functional as F
from torch.autograd import Variable
import pickle as pkl

torch.set_default_dtype(torch.float64)

# input_B_AN_D, target_B_S_D, nodeIncomingEdges_B_S_IE_AE, rel_rec_B_E_AN, rel_send_B_E_AN, relations_B_AE, exp_id


def load_LJ_data(data_dir, batch_size=1, train_examples=None, valid_examples=None, test_examples=None, save_min_max_dir="-1", testing = False, mode = None):
    assert mode in ["interpolation", "extrapolation"]

    input_file = data_dir + "LJ_GNN_input_N100_500000_freq50_train7000_val1500_test1500.npz"
    data = np.load(input_file)


    input_T_AN_D = data["input_T_AN_D"]
    target_T_S_D = data["target_T_S_D"]
    selected_particles_T_S_AN = data["selected_particles_T_S_AN"]
    node_incoming_edges_T_S_IE_AE = data["node_incoming_edges_T_S_IE_AE"]#.astype(np.float32)
    rel_rec_T_AE_AN = data["rel_rec_T_AE_AN"]#.astype(np.float32)
    rel_send_T_AE_AN = data["rel_send_T_AE_AN"]#.astype(np.float32)
    edgeID_T_AE = data["edgeID_T_AE"]
    truth_relations_T_AE = data["truth_relations_T_AE"].astype(np.int64)


    print("######## In load_LJ_data ########")
    print("input_T_AN_D shape: ", input_T_AN_D.shape, " , input_T_AN_D dtype: ", input_T_AN_D.dtype)
    print("target_T_S_D shape: ", target_T_S_D.shape, " , target_T_S_D dtype: ", target_T_S_D.dtype)
    print("selected_particles_T_S_AN shape: ", selected_particles_T_S_AN.shape, " , selected_particles_T_S_AN dtype: ", selected_particles_T_S_AN.dtype)
    print("node_incoming_edges_T_S_IE_AE shape: , ", node_incoming_edges_T_S_IE_AE.shape, " , node_incoming_edges_T_S_IE_AE dtype: ", node_incoming_edges_T_S_IE_AE.dtype)
    print("rel_rec_T_AE_AN shape: ", rel_rec_T_AE_AN.shape, " , rel_rec_T_AE_AN dtype: ", rel_rec_T_AE_AN.dtype)
    print("rel_send_T_AE_AN shape: ", rel_send_T_AE_AN.shape, " , rel_send_T_AE_AN dtype: ", rel_send_T_AE_AN.dtype)
    print("edgeID_T_AE shape: ", edgeID_T_AE.shape, ", edgeID_T_AE dtype: ", edgeID_T_AE.dtype)
    print("truth_relations_T_AE shape: ", truth_relations_T_AE.shape, " , truth_relations_T_AE dtype: ", truth_relations_T_AE.dtype)
    

    ######## train, valid, test index  ########
    if mode == "interpolation":
        train_ids = data["train_ids"]
        valid_ids = data["valid_ids"]
        test_ids = data["test_ids"]
    elif mode == "extrapolation":
        print("In load_LJ_data, train_examples: {}, valid_examples: {}, test_examples: {}".format(train_examples, valid_examples, test_examples) )
        train_ids = np.array([i for i in range(7000)])
        valid_ids = np.array([i for i in range(7000, 8500 )])
        test_ids =  np.array([i for i in range(8500, 10000 )])
    else:
        print("Error")
        exit()
    assert train_examples <= len(train_ids)
    assert valid_examples <= len(valid_ids)
    assert test_examples <= len(test_ids)
    train_ids = train_ids[:train_examples]
    valid_ids = valid_ids[:valid_examples]
    test_ids = test_ids[:test_examples]

    ######## train, valid, test data  ########
    input_train_T_AN_D = input_T_AN_D[train_ids]
    acc_train_T_S_D = target_T_S_D[train_ids]
    train_centerNodeID_T_S_AN = selected_particles_T_S_AN[train_ids]
    train_node_incoming_edges_T_S_IE_AE = node_incoming_edges_T_S_IE_AE[train_ids]
    train_rel_rec_T_AE_AN = rel_rec_T_AE_AN[train_ids]
    train_rel_send_T_AE_AN = rel_send_T_AE_AN[train_ids]
    train_edgeID_T_AE = edgeID_T_AE[train_ids]
    train_truth_relations_T_AE = truth_relations_T_AE[train_ids]

    input_valid_T_AN_D = input_T_AN_D[valid_ids]
    acc_valid_T_S_D = target_T_S_D[valid_ids]
    valid_centerNodeID_T_S_AN = selected_particles_T_S_AN[valid_ids]
    valid_node_incoming_edges_T_S_IE_AE = node_incoming_edges_T_S_IE_AE[valid_ids]
    valid_rel_rec_T_AE_AN = rel_rec_T_AE_AN[valid_ids]
    valid_rel_send_T_AE_AN = rel_send_T_AE_AN[valid_ids]
    valid_edgeID_T_AE = edgeID_T_AE[valid_ids]
    valid_truth_relations_T_AE = truth_relations_T_AE[valid_ids]

    input_test_T_AN_D = input_T_AN_D[test_ids]
    acc_test_T_S_D = target_T_S_D[test_ids]
    test_centerNodeID_T_S_AN = selected_particles_T_S_AN[test_ids]
    test_node_incoming_edges_T_S_IE_AE = node_incoming_edges_T_S_IE_AE[test_ids]
    test_rel_rec_T_AE_AN = rel_rec_T_AE_AN[test_ids]
    test_rel_send_T_AE_AN = rel_send_T_AE_AN[test_ids]
    test_edgeID_T_AE = edgeID_T_AE[test_ids]
    test_truth_relations_T_AE = truth_relations_T_AE[test_ids]


    input_var  = np.var(input_train_T_AN_D)
    # input_var = 1.0

    target_var = np.var(acc_train_T_S_D)
    # target_var = 1.0

    min_max_dict = {
        "input_var": input_var,
        "target_var": target_var
    }
    if save_min_max_dir != "-1":
        with open(save_min_max_dir + "min_max_dict.pkl", "wb") as f:
            pkl.dump(min_max_dict, f)
        print("saved min_max_dict into " + save_min_max_dir)

    print("min_max_dict")
    print(min_max_dict)

    ### normalization
    input_train_T_AN_D = input_train_T_AN_D / np.sqrt(input_var)
    acc_train_T_S_D = acc_train_T_S_D / np.sqrt(target_var)

    input_valid_T_AN_D = input_valid_T_AN_D / np.sqrt(input_var)
    acc_valid_T_S_D =  acc_valid_T_S_D / np.sqrt(target_var)

    input_test_T_AN_D = input_test_T_AN_D / np.sqrt(input_var)
    acc_test_T_S_D = acc_test_T_S_D / np.sqrt(target_var)


    train_frames_id = [i for i in range(train_examples)]
    train_frames_id = torch.IntTensor(train_frames_id)
    
    valid_frames_id = [i for i in range(valid_examples)]
    valid_frames_id = torch.IntTensor(valid_frames_id)


    test_frames_id = [i for i in range(test_examples)]
    test_frames_id = torch.IntTensor(test_frames_id)


    feat_train = input_train_T_AN_D
    target_train = acc_train_T_S_D
    centerNodeID_train = train_centerNodeID_T_S_AN
    node_incoming_edge_train = train_node_incoming_edges_T_S_IE_AE
    rel_rec_train = train_rel_rec_T_AE_AN
    rel_send_train = train_rel_send_T_AE_AN
    truth_edge_type_train = train_truth_relations_T_AE

    feat_train = torch.from_numpy(feat_train)#; print("feat_train shape: ", feat_train.shape)
    target_train = torch.from_numpy(target_train)#; print("target_train shape: ", target_train.shape)
    centerNodeID_train = torch.from_numpy(centerNodeID_train)
    node_incoming_edge_train = torch.from_numpy(node_incoming_edge_train)#; print("node_incoming_edge_train shape: ", node_incoming_edge_train.shape)
    rel_rec_train = torch.from_numpy(rel_rec_train)#; print("rel_rec_train shape: ", rel_rec_train.shape)
    rel_send_train = torch.from_numpy(rel_send_train)#; print("rel_send_train shape: ", rel_send_train.shape)
    train_edgeID_T_AE = torch.from_numpy(train_edgeID_T_AE)
    truth_edge_type_train = torch.from_numpy(truth_edge_type_train)#; print("truth_edge_type_train shape: ", truth_edge_type_train.shape)



    feat_valid = input_valid_T_AN_D
    target_valid = acc_valid_T_S_D
    centerNodeID_valid = valid_centerNodeID_T_S_AN
    node_incoming_edge_valid = valid_node_incoming_edges_T_S_IE_AE
    rel_rec_valid = valid_rel_rec_T_AE_AN
    rel_send_valid = valid_rel_send_T_AE_AN
    truth_edge_type_valid = valid_truth_relations_T_AE

    feat_valid = torch.from_numpy(feat_valid)#; print("feat_valid shape: ", feat_valid.shape)
    target_valid = torch.from_numpy(target_valid)#; print("target_valid shape: ", target_valid.shape)
    centerNodeID_valid = torch.from_numpy(centerNodeID_valid)
    node_incoming_edge_valid = torch.from_numpy(node_incoming_edge_valid)#; print("node_incoming_edge_valid shape: ", node_incoming_edge_valid.shape)
    rel_rec_valid = torch.from_numpy(rel_rec_valid)#; print("rel_rec_valid shape: ", rel_rec_valid.shape)
    rel_send_valid = torch.from_numpy(rel_send_valid)#; print("rel_send_valid shape: ", rel_send_valid.shape)
    valid_edgeID_T_AE = torch.from_numpy(valid_edgeID_T_AE)
    truth_edge_type_valid = torch.from_numpy(truth_edge_type_valid)#; print("truth_edge_type_valid shape: ", truth_edge_type_valid.shape)


    feat_test = torch.from_numpy(input_test_T_AN_D)
    target_test = torch.from_numpy(acc_test_T_S_D)
    centerNodeID_test = torch.from_numpy(test_centerNodeID_T_S_AN)
    node_incoming_edge_test = torch.from_numpy(test_node_incoming_edges_T_S_IE_AE)
    rel_rec_test = torch.from_numpy(test_rel_rec_T_AE_AN)
    rel_send_test = torch.from_numpy(test_rel_send_T_AE_AN)
    test_edgeID_T_AE = torch.from_numpy(test_edgeID_T_AE)
    truth_edge_type_test = torch.from_numpy(test_truth_relations_T_AE)



    train_data = TensorDataset(feat_train, target_train, centerNodeID_train, node_incoming_edge_train, rel_rec_train, rel_send_train, train_edgeID_T_AE, truth_edge_type_train, train_frames_id)
    valid_data = TensorDataset(feat_valid, target_valid, centerNodeID_valid, node_incoming_edge_valid, rel_rec_valid, rel_send_valid, valid_edgeID_T_AE, truth_edge_type_valid, valid_frames_id)
    test_data = TensorDataset(feat_test, target_test, centerNodeID_test, node_incoming_edge_test, rel_rec_test, rel_send_test, test_edgeID_T_AE, truth_edge_type_test, test_frames_id)

    train_data_loader = DataLoader(train_data, batch_size=batch_size, num_workers=4, shuffle=True)
    valid_data_loader = DataLoader(valid_data, batch_size=batch_size, num_workers=4, shuffle= False)
    test_data_loader = DataLoader(test_data, batch_size = batch_size, num_workers=4, shuffle= False)
    
    if testing == True:
        return train_data_loader, valid_data_loader, test_data_loader
    else:
        return train_data_loader, valid_data_loader


def load_LJ_data_test(data_dir, batch_size=1, train_examples=None, valid_examples=None, test_examples=None, save_min_max_dir="-1", testing = False, min_max_folder=None, mode = None):
    assert mode in ["interpolation", "extrapolation"]

    input_file = data_dir + "LJ_GNN_input_N100_500000_freq50_train7000_val1500_test1500.npz"
    data = np.load(input_file)


    pairwise_force_file = data_dir + "true_pairwise_force_10000_500_2.npy"
    pairwise_force_T_AE_D = np.load(pairwise_force_file)


    input_T_AN_D = data["input_T_AN_D"]
    target_T_S_D = data["target_T_S_D"]
    selected_particles_T_S_AN = data["selected_particles_T_S_AN"]
    node_incoming_edges_T_S_IE_AE = data["node_incoming_edges_T_S_IE_AE"]#.astype(np.float32)
    rel_rec_T_AE_AN = data["rel_rec_T_AE_AN"]#.astype(np.float32)
    rel_send_T_AE_AN = data["rel_send_T_AE_AN"]#.astype(np.float32)
    edgeID_T_AE = data["edgeID_T_AE"]
    truth_relations_T_AE = data["truth_relations_T_AE"].astype(np.int64)


    print("######## In load_LJ_data ########")
    print("input_T_AN_D shape: ", input_T_AN_D.shape, " , input_T_AN_D dtype: ", input_T_AN_D.dtype)
    print("target_T_S_D shape: ", target_T_S_D.shape, " , target_T_S_D dtype: ", target_T_S_D.dtype)
    print("selected_particles_T_S_AN shape: ", selected_particles_T_S_AN.shape, " , selected_particles_T_S_AN dtype: ", selected_particles_T_S_AN.dtype)
    print("node_incoming_edges_T_S_IE_AE shape: , ", node_incoming_edges_T_S_IE_AE.shape, " , node_incoming_edges_T_S_IE_AE dtype: ", node_incoming_edges_T_S_IE_AE.dtype)
    print("rel_rec_T_AE_AN shape: ", rel_rec_T_AE_AN.shape, " , rel_rec_T_AE_AN dtype: ", rel_rec_T_AE_AN.dtype)
    print("rel_send_T_AE_AN shape: ", rel_send_T_AE_AN.shape, " , rel_send_T_AE_AN dtype: ", rel_send_T_AE_AN.dtype)
    print("edgeID_T_AE shape: ", edgeID_T_AE.shape, ", edgeID_T_AE dtype: ", edgeID_T_AE.dtype)
    print("truth_relations_T_AE shape: ", truth_relations_T_AE.shape, " , truth_relations_T_AE dtype: ", truth_relations_T_AE.dtype)
    

    ######## train, valid, test index  ########
    if mode == "interpolation":
        train_ids = data["train_ids"]
        valid_ids = data["valid_ids"]
        test_ids = data["test_ids"]
    elif mode == "extrapolation":
        print("In load_LJ_data_test, train_examples: {}, valid_examples: {}, test_examples: {}".format(train_examples, valid_examples, test_examples) )
        train_ids = np.array([i for i in range(7000)])
        valid_ids = np.array([i for i in range(7000, 8500 )])
        test_ids =  np.array([i for i in range(8500, 10000 )])
    else:
        print("Error")
        exit()
    
    assert train_examples <= len(train_ids)
    assert valid_examples <= len(valid_ids)
    assert test_examples <= len(test_ids)
    train_ids = train_ids[:train_examples]
    valid_ids = valid_ids[:valid_examples]
    test_ids = test_ids[:test_examples]

    ######## train, valid, test data  ########
    input_train_T_AN_D = input_T_AN_D[train_ids]
    acc_train_T_S_D = target_T_S_D[train_ids]
    train_centerNodeID_T_S_AN = selected_particles_T_S_AN[train_ids]
    train_node_incoming_edges_T_S_IE_AE = node_incoming_edges_T_S_IE_AE[train_ids]
    train_rel_rec_T_AE_AN = rel_rec_T_AE_AN[train_ids]
    train_rel_send_T_AE_AN = rel_send_T_AE_AN[train_ids]
    train_edgeID_T_AE = edgeID_T_AE[train_ids]
    train_truth_relations_T_AE = truth_relations_T_AE[train_ids]

    train_pairwise_force_T_AE_D = pairwise_force_T_AE_D[train_ids]




    input_valid_T_AN_D = input_T_AN_D[valid_ids]
    acc_valid_T_S_D = target_T_S_D[valid_ids]
    valid_centerNodeID_T_S_AN = selected_particles_T_S_AN[valid_ids]
    valid_node_incoming_edges_T_S_IE_AE = node_incoming_edges_T_S_IE_AE[valid_ids]
    valid_rel_rec_T_AE_AN = rel_rec_T_AE_AN[valid_ids]
    valid_rel_send_T_AE_AN = rel_send_T_AE_AN[valid_ids]
    valid_edgeID_T_AE = edgeID_T_AE[valid_ids]
    valid_truth_relations_T_AE = truth_relations_T_AE[valid_ids]

    valid_pairwise_force_T_AE_D = pairwise_force_T_AE_D[valid_ids]





    input_test_T_AN_D = input_T_AN_D[test_ids]
    acc_test_T_S_D = target_T_S_D[test_ids]
    test_centerNodeID_T_S_AN = selected_particles_T_S_AN[test_ids]
    test_node_incoming_edges_T_S_IE_AE = node_incoming_edges_T_S_IE_AE[test_ids]
    test_rel_rec_T_AE_AN = rel_rec_T_AE_AN[test_ids]
    test_rel_send_T_AE_AN = rel_send_T_AE_AN[test_ids]
    test_edgeID_T_AE = edgeID_T_AE[test_ids]
    test_truth_relations_T_AE = truth_relations_T_AE[test_ids]

    test_pairwise_force_T_AE_D = pairwise_force_T_AE_D[test_ids]




    input_var  = np.var(input_train_T_AN_D)
    # input_var = 1.0

    target_var = np.var(acc_train_T_S_D)
    # target_var = 1.0

    min_max_dict = {
        "input_var": input_var,
        "target_var": target_var
    }
    if save_min_max_dir != "-1":
        with open(save_min_max_dir + "min_max_dict.pkl", "wb") as f:
            pkl.dump(min_max_dict, f)
        print("saved min_max_dict into " + save_min_max_dir)
    else:
        assert min_max_folder != None
        with open(min_max_folder + "min_max_dict.pkl", "rb") as f:
            min_max_dict = pkl.load(f)
            input_var = min_max_dict["input_var"]
            target_var = min_max_dict["target_var"]

    print("min_max_dict")
    print(min_max_dict)

    ### normalization
    input_train_T_AN_D = input_train_T_AN_D / np.sqrt(input_var)
    acc_train_T_S_D = acc_train_T_S_D / np.sqrt(target_var)

    input_valid_T_AN_D = input_valid_T_AN_D / np.sqrt(input_var)
    acc_valid_T_S_D =  acc_valid_T_S_D / np.sqrt(target_var)

    input_test_T_AN_D = input_test_T_AN_D / np.sqrt(input_var)
    acc_test_T_S_D = acc_test_T_S_D / np.sqrt(target_var)


    train_frames_id = [i for i in range(train_examples)]
    train_frames_id = torch.IntTensor(train_frames_id)
    
    valid_frames_id = [i for i in range(valid_examples)]
    valid_frames_id = torch.IntTensor(valid_frames_id)


    test_frames_id = [i for i in range(test_examples)]
    test_frames_id = torch.IntTensor(test_frames_id)


    feat_train = input_train_T_AN_D
    target_train = acc_train_T_S_D
    centerNodeID_train = train_centerNodeID_T_S_AN
    node_incoming_edge_train = train_node_incoming_edges_T_S_IE_AE
    rel_rec_train = train_rel_rec_T_AE_AN
    rel_send_train = train_rel_send_T_AE_AN
    truth_edge_type_train = train_truth_relations_T_AE

    feat_train = torch.from_numpy(feat_train)#; print("feat_train shape: ", feat_train.shape)
    target_train = torch.from_numpy(target_train)#; print("target_train shape: ", target_train.shape)
    centerNodeID_train = torch.from_numpy(centerNodeID_train)
    node_incoming_edge_train = torch.from_numpy(node_incoming_edge_train)#; print("node_incoming_edge_train shape: ", node_incoming_edge_train.shape)
    rel_rec_train = torch.from_numpy(rel_rec_train)#; print("rel_rec_train shape: ", rel_rec_train.shape)
    rel_send_train = torch.from_numpy(rel_send_train)#; print("rel_send_train shape: ", rel_send_train.shape)
    train_edgeID_T_AE = torch.from_numpy(train_edgeID_T_AE)
    truth_edge_type_train = torch.from_numpy(truth_edge_type_train)#; print("truth_edge_type_train shape: ", truth_edge_type_train.shape)

    train_pairwise_force_T_AE_D = torch.from_numpy(train_pairwise_force_T_AE_D)



    feat_valid = input_valid_T_AN_D
    target_valid = acc_valid_T_S_D
    centerNodeID_valid = valid_centerNodeID_T_S_AN
    node_incoming_edge_valid = valid_node_incoming_edges_T_S_IE_AE
    rel_rec_valid = valid_rel_rec_T_AE_AN
    rel_send_valid = valid_rel_send_T_AE_AN
    truth_edge_type_valid = valid_truth_relations_T_AE

    feat_valid = torch.from_numpy(feat_valid)#; print("feat_valid shape: ", feat_valid.shape)
    target_valid = torch.from_numpy(target_valid)#; print("target_valid shape: ", target_valid.shape)
    centerNodeID_valid = torch.from_numpy(centerNodeID_valid)
    node_incoming_edge_valid = torch.from_numpy(node_incoming_edge_valid)#; print("node_incoming_edge_valid shape: ", node_incoming_edge_valid.shape)
    rel_rec_valid = torch.from_numpy(rel_rec_valid)#; print("rel_rec_valid shape: ", rel_rec_valid.shape)
    rel_send_valid = torch.from_numpy(rel_send_valid)#; print("rel_send_valid shape: ", rel_send_valid.shape)
    valid_edgeID_T_AE = torch.from_numpy(valid_edgeID_T_AE)
    truth_edge_type_valid = torch.from_numpy(truth_edge_type_valid)#; print("truth_edge_type_valid shape: ", truth_edge_type_valid.shape)

    valid_pairwise_force_T_AE_D = torch.from_numpy(valid_pairwise_force_T_AE_D)




    feat_test = torch.from_numpy(input_test_T_AN_D)
    target_test = torch.from_numpy(acc_test_T_S_D)
    centerNodeID_test = torch.from_numpy(test_centerNodeID_T_S_AN)
    node_incoming_edge_test = torch.from_numpy(test_node_incoming_edges_T_S_IE_AE)
    rel_rec_test = torch.from_numpy(test_rel_rec_T_AE_AN)
    rel_send_test = torch.from_numpy(test_rel_send_T_AE_AN)
    test_edgeID_T_AE = torch.from_numpy(test_edgeID_T_AE)
    truth_edge_type_test = torch.from_numpy(test_truth_relations_T_AE)

    test_pairwise_force_T_AE_D = torch.from_numpy(test_pairwise_force_T_AE_D)



    train_data = TensorDataset(feat_train, target_train, train_pairwise_force_T_AE_D, centerNodeID_train, node_incoming_edge_train, rel_rec_train, rel_send_train, train_edgeID_T_AE, truth_edge_type_train, train_frames_id)
    valid_data = TensorDataset(feat_valid, target_valid, valid_pairwise_force_T_AE_D, centerNodeID_valid, node_incoming_edge_valid, rel_rec_valid, rel_send_valid, valid_edgeID_T_AE, truth_edge_type_valid, valid_frames_id)
    test_data = TensorDataset(feat_test, target_test, test_pairwise_force_T_AE_D, centerNodeID_test, node_incoming_edge_test, rel_rec_test, rel_send_test, test_edgeID_T_AE, truth_edge_type_test, test_frames_id)

    train_data_loader = DataLoader(train_data, batch_size=batch_size, num_workers=4, shuffle=True)
    valid_data_loader = DataLoader(valid_data, batch_size=batch_size, num_workers=4, shuffle= False)
    test_data_loader = DataLoader(test_data, batch_size = batch_size, num_workers=4, shuffle= False)
    
    if testing == True:
        return train_data_loader, valid_data_loader, test_data_loader
    else:
        return train_data_loader, valid_data_loader


